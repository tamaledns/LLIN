<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class ajax_api extends CI_Controller {

    public $page_level;
    public $page_level2 = "";
    public $product_id = 2;

    function __construct()
        {
            parent::__construct();

            $user_type = $this->session->userdata('user_type');

            switch ($user_type) {

                default:
                    $this->page_level = 'app';
                    break;
//                case 1:
//                    $this->page_level = 'admin';
//                    break;
//                case 2:
//                    $this->page_level = 'teller';
//                    break;
//                case 3:
//                    $this->page_level = 'finance';
//                    break;
//                case 4:
//                    $this->page_level = 'advertisement';
//                    break;
//                case 5:
//                    $this->page_level = 'data_entrant';
//                    break;
//                case 6:
//                    $this->page_level = 'customer';
//                    break;
//                case 7:
//                    $this->page_level = 'agent';
//                    break;

            }

//            $this->output->enable_profiler(TRUE);

        }

    ///this the Location Management for the LLIN project

    function get_country($id)
        {


        }

    function get_district($id)
        {


            print_r($this->locations->get_district($id));


        }

    function get_children($id)
        {


            print_r($this->locations->get_children($id));


        }

    function get_location_name($id)
        {

//        57544



            print_r($this->locations->get_location_name($id));


        }

    function get_location_type($id)
        {

//        57544



            print_r($this->locations->get_location_type($id));


        }

    function get_path($id)
        {

//        57544


            $path = $this->locations->get_path($id);

            print_r($path);


        }

    function get_sub_country($id)
        {


        }

    function get_parish($id)
        {

        }

    function get_village($id)
        {


            echo '[0] => Array ( [id] => 3 [name] => Eastern )
            [1] => Array ( [id] => 64 [name] => Mbale )
            [2] => Array ( [id] => 956 [name] => Busiu )
            [3] => Array ( [id] => 6105 [name] => Buwalasi )
            [4] => Array ( [id] => 27141 [name] => Namunyu )<hr/>';

            //   print_r( $this->locations->get_villages_under_selection($id));

            $villages = '';

            foreach ($this->locations->get_villages_under_selection($id) as $v) {
                echo $villages .= $v . ',';
            }


        }


    ///this is the management for the LLLin project


    function get_children_options($id)
        {


            echo '<option value="">Select..</option>';
            foreach ($this->locations->get_children($id) as $sc) {
                ?>

                <option value="<?php echo $sc->id ?>"><?php echo $sc->name ?> </option>

            <?php }

        }


    function get_coverage($id)
        {
            $data['id'] = $id;
            $this->load->view($this->page_level . '/coverage/cov_locations', $data);
        }


    function get_subcategory($id)
        {
            foreach ($this->db->select('id,name,description')->from('sub_category')->where('category_id', $id)->get()->result() as $sc) { ?>
                <option
                    value="<?php echo $sc->id ?>" <?php echo set_select('sub_category', $sc->id); ?>><?php echo $sc->name ?> </option>
            <?php } ?>

            <?php

        }

    function inbox_notification()
        {
            $n = $this->db->where(array('replied' => 'N'))->from('inbox')->count_all_results();

            echo $n;

        }

    function get_city($id)
        {

            foreach ($this->db->select('title')->from('state')->where('country', $id)->order_by('title', 'asc')->get()->result() as $sc) { ?>
                <option
                    value="<?php echo $sc->title ?>" <?php echo set_select('city', $sc->title); ?>><?php echo $sc->title ?> </option>
            <?php } ?>

            <?php

        }


    function dialing_code($id)
        {
            $c = $this->db->select('dialing_code')->from('country')->where('a2_iso', $id)->get()->row();
            if (count($c) == 1) {
                echo $c->dialing_code;
            }

        }


    function get_users($id = 7)
        { ?>
            <!--        <option value="" --><?php //echo set_select('referral', '', true);
            ?><!-->Select Referral...</option>-->

            <?php foreach ($this->db->select('id,first_name,last_name,full_name')->from('users')->where('user_type', $id)->order_by('id', 'desc')->get()->result() as $sc) { ?>

            <option
                value="<?php echo $sc->id ?>" <?php echo set_select('referral', $sc->id); ?>><?php echo $sc->first_name . ' ' . $sc->last_name ?> </option>

        <?php } ?>

            <?php

        }


    function sent_sms($type = null)
        {

            /*
             * Paging
             */

            if (strlen($type) > 0) {
                if ($type == 'unpublished') {

                    $where = array('a.status !=' => 'published');

                }
                elseif ($type == 'published') {
                    $where = array('a.publication_type' => $type / date('Y'));
                }
                else {
                    $where = array();
                }
            }
            // $ss= $_REQUEST['search'];


            $this->db->from('sqlbox_sent_sms a');
            //strlen($type)>0?$this->db->where($where):'';
            $trecords = $this->db->count_all_results();
            $iTotalRecords = $trecords;
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);


            $records = array();
            $records["data"] = array();

            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;


            ///$user=isset($_REQUEST["user"])?$_REQUEST["user"]:'';
            $this->db->select('')->from('sqlbox_sent_sms a');

//            strlen($type)>0?$this->db->where($where):'';
//            $this->db->like('a.title', $_REQUEST['search']['value']);

            //filtering
//            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_search") {
//
//                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
//
////                isset($_REQUEST["customSearchName"]) && strlen($_REQUEST["customSearchName"]) > 0 ? $this->db->like('a.title', $_REQUEST['search']['value']) : '';
//
//
//                isset($_REQUEST["customSectorName"]) && strlen($_REQUEST["customSectorName"]) > 0 ? $this->db->where('a.sector', $_REQUEST["customSectorName"]) : '';
//
//
//                isset($_REQUEST["customPublicationName"]) && strlen($_REQUEST["customPublicationName"]) > 0 ? $this->db->where('a.publication_type', $_REQUEST["customPublicationName"]) : '';
//
//                isset($_REQUEST["customFromName"]) && strlen($_REQUEST["customFromName"]) > 0 ? $this->db->where('a.created_on >=', strtotime($_REQUEST["customFromName"])) : '';
//
//
//                isset($_REQUEST["customToName"]) && strlen($_REQUEST["customToName"]) > 0 ? $this->db->where('a.created_on <=', strtotime($_REQUEST["customToName"].' 24:59:59')) : '';
//
//
////                $records["customActionMessage"] = "Group action successfully has been completed. Well done !".$_REQUEST["customSearchName"]." ".$_REQUEST["customSectorName"].' '.$_REQUEST["customPublicationName"]; // pass custom message(useful for getting status of group actions)
//
//            }
            //end of filtering

            $orders = $this->db->order_by('a.sql_id', 'desc')->limit($iDisplayLength, $iDisplayStart)->get()->result();


            $no = 1;
            foreach ($orders as $sb):

//                if ($sb->status == 'canceled') {
//
//                    $action='published';
//                    $label='Publish';
//                    $status = '<div style="width:100px;" class="btn btn-sm btn-danger">Un Published</div>';
//                }
//                elseif ($sb->status == 'pending') {
//
//                    $action='published';
//                    $label='Publish';
//                    $status = '<div style="width:100px;" class="btn btn-sm btn-info">' . humanize($sb->status) . '</div>';
//                }
//                elseif ($sb->status == 'evaluated') {
//
//                    $action='published';
//                    $label='Publish';
//                    $status = '<div style="width:100px;" class="btn btn-sm btn-warning">' . humanize($sb->status) . '</div>';
//                }
//                elseif ($sb->status == 'published') {
//                    $action='canceled';
//                    $label='Unpublish';
//
//                    $status = '<div style="width:100px;" class="btn btn-sm green-jungle">' . humanize($sb->status) . '</div>';
//                }else{
//
//                    $action='published';
//                    $label='Publish';
//                    $status = '<div style="width:100px;" class="btn btn-sm btn-primary">Pending</div>';
//                }

                $action = 'published';
                $label = 'Publish';
                $status = '<div style="width:100px;" class="btn btn-sm btn-primary">Pending</div>';


                $output = ' <li>' . anchor('home/publications/view_publication/' . $sb->sql_id * date('Y'), '<i class="fa fa-eye"></i> View', 'target="_blank"') . '</li>';


                $output .= $this->session->userdata('user_type') == 1 ? (' <li>' . anchor($this->page_level . '/publications/approve/' . $sb->sql_id * date('Y') . '/' . $action, '<i class="fa fa-arrow-right"></i> ' . $label) . '</li>') : '';
                $output .= $this->session->userdata('user_type') == 1 ? (' <li>' . anchor($this->page_level . '/publications/recommend/' . $sb->sql_id * date('Y'), '<i class="fa fa-plus"></i> Recommendation') . '</li>') : '';

                $output .= '<ul/>';


                $records["data"][] = array(

//                    '<input type="checkbox"  name="id[]" value="' . $sb->id . '">',

                    $no,
                    anchor('home/publications/view_publication/' . $sb->sql_id * date('Y'), character_limiter(str_replace('+', ' ', textfom($sb->msgdata)), 10), 'title="View Publication"', 'target="_blank"'),

                    $sb->sender,
                    $sb->receiver,
                    date('d-m-Y', $sb->time),
                    $sb->smsc_id,
                    $sb->service,


                    strlen($output) > 0 ?
                        ' <div class="btn-group">
                        <a class="btn green-jungle btn-sm" href="javascript:;" data-toggle="dropdown">
                            <i class="fa fa-cogs"></i> Action <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-right">' .
                        $output : ''


                );
                $no++; endforeach;


            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;

            echo json_encode($records);
        }


    function realtime_monitoring()
        {

            /*
             * Paging
             */

            $iTotalRecords = $this->db->count_all_results('ci_sessions');
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);


            $records = array();
            $records["data"] = array();

            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;


            $this->db->select()->from('ci_sessions');
            $query = $this->db->order_by('timestamp', 'desc')->limit($iDisplayLength, $iDisplayStart)->get()->result();


            $no = 1;
            foreach ($query as $row):


                $session_data = $row->data;

                //  print_r($session_data);
                $return_data = array();
                $offset = 0;
                while ($offset < strlen($session_data)) {
                    if (!strstr(substr($session_data, $offset), "|")) {
                        throw new Exception("invalid data, remaining: " . substr($session_data, $offset));
                    }
                    $pos = strpos($session_data, "|", $offset);
                    $num = $pos - $offset;
                    $varname = substr($session_data, $offset, $num);
                    $offset += $num + 1;
                    $data = unserialize(substr($session_data, $offset));
                    $return_data[$varname] = $data;
                    $offset += strlen(serialize($data));
                }

                if (!empty($return_data['username'])) {


                    $records["data"][] = array(

//                    '<input type="checkbox"  name="id[]" value="' . $sb->id . '">',

                        $no,
                        trending_date($row->timestamp),
                        $return_data['username'],
                        $row->ip_address,
                        $return_data['first_name'] . '&nbsp;' . $return_data['last_name'],

                        $return_data['email'],
                        isset($return_data['browser']) ? $return_data['browser'] : '',
                        isset($return_data['platform']) ? $return_data['platform'] : ''


                    );


                }


                $no++; endforeach;

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;

            echo json_encode($records);
        }

    function newsletter($email = null)
        {

            // Provides: <body text='black'>


            if (strlen($email) > 0) {

                $email = str_replace("~", "@", $email);
                $message = "Dear Subscriber,\n\r
Thank you for subscribing to the Government Evaluation Facility newsletter.";
                $admin_message = "Dear GEF Admin,
$email has subscribed to the Government Evaluation Facility newsletter.";
                $admin_email = 'ondouglas@gmail.com';


                $count = $this->db->from('newsletter_emails')->where(array('email' => $email, 'email_sent' => 1))->count_all_results();

                if ($count == 0) {

                    $count2 = $this->db->from('newsletter_emails')->where(array('email' => $email))->count_all_results();

                    $count2 == 0 ? $this->db->insert('newsletter_emails', array('email' => $email, 'created_on' => time())) : '';


                    if ($this->sendHTMLEmail2($email, 'Government Evaluation Facility', $message)) {

                        $this->db->where('email', $email)->update('newsletter_emails', array('email_sent' => 1));
                        echo '<div class="alert alert-success"><i class="fa fa-check"></i> Thank you for subscribing to the GEF Newsletter. You will receive a confirmation email shortly.</div>';
                        $this->sendHTMLEmail2($admin_email, 'Government Evaluation Facility', $admin_message);
                    }
                    else {
                        echo '<div class="alert alert-danger"><i class="fa fa-ban"></i> Opps!!! an an  error occurred while subscribing.</div>';
                    }
                }
                else {


                    echo '<div class="alert alert-info"><i class="fa fa-ban"></i>  Your are already subscribed with us...</div>';
                }

            }
            else {

                echo '<div class="alert alert-danger"><i class="fa fa-ban fa-spinner"></i> Your email address is required.' . $email . '</div>';
            }
        }

    public function getNetwork($mobile)
        {
            $substring = substr($mobile, 0, 5);

            if ($substring == "25678" || $substring == "25677" || $substring == "25639") {
                return "MTN_UGANDA";

            }
            else if ($substring == "25675" || $substring == "25670") {

                return "AIRTEL_UGANDA";

            }
            else if ($substring == "25679") {

                return "ORANGE_UGANDA";

            }
            else if ($substring == "25671") {

                return "UTL_UGANDA";
            }
        }

        function get_form($id){
            $data['id']=$id;
            $this->load->view('app/house_hold_reg/distribution_form',$data);
        }


    function list_forms($type = null)
        {


            isset($type) && $type != 'list_forms' ? ($type != 'new' ? $this->db->where(array('a.status' => $type)) : '') : '';
            $this->db->from('forms a');

            $trecords = $this->db->count_all_results();
            $iTotalRecords = $trecords;
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);


            $records = array();
            $records["data"] = array();

            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;


// this is the part where sorting is made
            $order = $_REQUEST['order'];
            $column = $order[0]['column'];
            $dir = $order[0]['dir'];

            $order_list = array(
                array("id" => $dir),
                array("form_num" => $dir),
                array("c.first_name" => $dir),
                array("danger" => $dir),
                array("danger" => $dir),
                array("danger" => $dir),
                array("c.village" => $dir),
                array("danger" => $dir),
                array("date_created" => $dir),
                array("a.status" => $dir)
            );
            $o_list = $order_list[$column];
//            (key($status)) . '">' . (current($status))

            //this is the end of the sorting


            if (isset($_REQUEST["sub_county"]) && strlen($_REQUEST["sub_county"]) > 0) {

                if (strlen($vill = $_REQUEST["village"]) > 0) {

                    $wher = $this->all_villages($vill);

                }
                elseif (strlen($vill = $_REQUEST["parish"]) > 0) {

                    $wher = $this->all_villages($vill);

                }
                elseif (strlen($vill = $_REQUEST["sub_county"]) > 0) {
                    $wher = $this->all_villages($vill);

                }

                // $this->db->like('a.form_num', $_REQUEST["sub_county"])->or_like('a.form_num', $_REQUEST["sub_county"]);

            }


            $this->db->select('a.id,a.form_num,a.phone,a.date_created,c.first_name,c.last_name,c.village,a.status')->from('forms a')->join('vht c', 'a.phone=c.phone');

            //filtering
            isset($type) && $type != 'list_forms' ? ($type != 'new' ? $this->db->where(array('a.status' => $type)) : '') : '';

            isset($_REQUEST['search']['value']) && strlen($_REQUEST['search']['value']) > 0 ? $this->db->group_start()
                ->like('a.form_num', $_REQUEST['search']['value'])
                ->or_like('c.phone', $_REQUEST['search']['value'])
                ->or_like('c.first_name', $_REQUEST['search']['value'])
                ->or_like('c.last_name', $_REQUEST['search']['value'])->group_end() : ($this->session->user_type == 1 ? '' : $this->db->like('a.form_num', '000000000--------'));


            isset($_REQUEST["date_from"]) && strlen($_REQUEST["date_from"]) > 0 ? $this->db->where(array('a.date_created >=' => $_REQUEST["date_from"], 'a.date_created <=' => $_REQUEST["date_to"] . ' 24:59:59')) : '';


            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_search") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                isset($_REQUEST["customFromName"]) && strlen($_REQUEST["customFromName"]) > 0 ? $this->db->where('a.date_created >=', $_REQUEST["customFromName"]) : '';
                isset($_REQUEST["customToName"]) && strlen($_REQUEST["customToName"]) > 0 ? $this->db->where('a.date_created <=', $_REQUEST["customToName"] . ' 24:59:59') : '';
            }


            if (isset($_REQUEST["sub_county"]) && strlen($_REQUEST["sub_county"]) > 0) {

                $this->db->where($wher);

            }


            //end of filtering

            $this->db->order_by(key($o_list), current($o_list));

            $forms = $this->db->limit($iDisplayLength, $iDisplayStart)->get()->result();


            $no = 1;
            foreach ($forms as $sb):


                if ($sb->status == 'in_field') {

                    $status = '<div style="width:100px;" class="btn btn-sm btn-danger">' . humanize($sb->status) . '</div>';
                }
                elseif ($sb->status == 'in_store') {

                    $status = '<div style="width:100px;" class="btn btn-sm btn-warning">' . humanize($sb->status) . '</div>';
                }
                elseif ($sb->status == 'released') {

                    $status = '<div style="width:100px;" class="btn btn-sm btn-primary">' . humanize($sb->status) . '</div>';
                }
                elseif ($sb->status == 'returned') {

                    $status = '<div style="width:100px;" class="btn btn-sm green-jungle">' . humanize($sb->status) . '</div>';
                }
                else {
                    $status = '<div style="width:100px;" class="btn btn-sm btn-danger">In field</div>';
                }


                $output = ' <li>' . anchor($this->page_level . '/house_hold_reg/view/' . $sb->form_num, '<i class="fa fa-eye"></i> View') . '</li>';
                $output .= ' <li>' . anchor($this->page_level . '/house_hold_reg/new/' . $sb->form_num, '<i class="fa fa-plus"></i> Enter Data') . '</li>';
                $output .= ' <li>' . anchor($this->page_level . '/house_hold_reg/view/' . $sb->form_num . '/compare', '<i class="fa fa-exchange"></i> Compare') . '</li>';
                $output .= '<ul/>';

                //getting path and and location name
                $path = $this->locations->get_path($sb->village);
                //$village_name=$this->locations->get_location_name($sb->village);

                ///getting house holds and the number of people in the house holds

                $form_entered = $this->custom_library->house_holds($sb->form_num) ? $this->custom_library->house_holds($sb->form_num) : array();


                if (count($form_entered) != 0) {

                    $people_in_household = $this->custom_library->people_in_household($form_entered[0]->id) ? $this->custom_library->people_in_household($form_entered[0]->id) : array();


                    $house_holds = $this->db->select_sum('person_vht')->from('reg_detail')->where(array('data_id' => $form_entered[0]->id))->get()->row();

                }
                else {
                    $people_in_household = array();
                }


                $direction = count($form_entered) == 0 ? 'new/' : 'view/';


                $forms_entered = $this->db->where('form_num', $sb->form_num)->from('registration')->count_all_results();


                $records["data"][] = array(

                    '
                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                            <input type="checkbox" class="group-checkable" name="id[]" value="' . $sb->id . '" />  <span></span>
                                                        </label>
                    ',


                    anchor($this->page_level . '/house_hold_reg/' . $direction . $sb->form_num, $sb->form_num . '<br/><b>District:</b>' . $path[1]['name'], 'title="View"', 'target="_blank"'),
                    strlen($sb->first_name) > 0 ? '<b>Name:</b>' . $sb->first_name . ' ' . $sb->last_name . '<br/><b>Phone:</b>' . $sb->phone : 'Not registered',

                    $forms_entered,
                    count($people_in_household),
                    count($people_in_household) > 0 ? $house_holds->person_vht : '',
                    //$house_holds->person_vht,
                    $path[4]['name'],
                    $path[2]['name'],

                    '<span style="white-space: nowrap;">' . trending_date(strtotime($sb->date_created)) . '</span>',

                    $status,

                    strlen($output) > 0 ?
                        ' <div class="btn-group">
                        <a class="btn green-jungle btn-sm" href="javascript:;" data-toggle="dropdown">
                            <i class="fa fa-cogs"></i> Action <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-right">' .
                        $output : ''


                );
                $no++; endforeach;


            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;

            echo json_encode($records);
        }

    function data_tree()
        {
            $parent = $_REQUEST["parent"];

            $data = array();

            $states = array(
                "success",
                "info",
                "danger",
                "warning"
            );

            if ($parent == "#") {
                for ($i = 1; $i < rand(4, 7); $i++) {
                    $data[] = array(
                        "id" => "node_" . time() . rand(1, 100000),
                        "text" => "Node #" . $i,
                        "icon" => "fa fa-folder icon-lg icon-state-" . ($states[rand(0, 3)]),
                        "children" => true,
                        "type" => "root"
                    );
                }
            }
            else {
                if (rand(1, 5) === 3) {
                    $data[] = array(
                        "id" => "node_" . time() . rand(1, 100000),
                        "icon" => "fa fa-file fa-large icon-state-default",
                        "text" => "No childs ",
                        "state" => array("disabled" => true),
                        "children" => false
                    );
                }
                else {
                    for ($i = 1; $i < rand(2, 4); $i++) {
                        $data[] = array(
                            "id" => "node_" . time() . rand(1, 100000),
                            "icon" => (rand(0, 3) == 2 ? "fa fa-file icon-lg" : "fa fa-folder icon-lg") . " icon-state-" . ($states[rand(0, 3)]),
                            "text" => "Node " . time(),
                            "children" => (rand(0, 3) == 2 ? false : true)
                        );
                    }
                }
            }

            header('Content-type: text/json');
            header('Content-type: application/json');
            echo json_encode($data);
        }

    function vht($type = null)
        {


            isset($type) && $type != 'list_forms' ? ($type != 'new' ? $this->db->where(array('a.status' => $type)) : '') : '';
            $this->db->from('vht a');

            $trecords = $this->db->count_all_results();
            $iTotalRecords = $trecords;
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);


            $records = array();
            $records["data"] = array();

            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;


// this is the part where sorting is made
            $order = $_REQUEST['order'];
            $column = $order[0]['column'];
            $dir = $order[0]['dir'];

            $order_list = array(
                array("id" => $dir),
                array("first_name" => $dir),
                array("phone" => $dir),
            );
            $o_list = $order_list[$column];
//            (key($status)) . '">' . (current($status))

            //this is the end of the sorting


            if (isset($_REQUEST["sub_county"]) && strlen($_REQUEST["sub_county"]) > 0) {

                if (strlen($vill = $_REQUEST["village"]) > 0) {

                    $wher = $this->all_villages($vill);

                }
                elseif (strlen($vill = $_REQUEST["parish"]) > 0) {

                    $wher = $this->all_villages($vill);

                }
                elseif (strlen($vill = $_REQUEST["sub_county"]) > 0) {
                    $wher = $this->all_villages($vill);

                }

                // $this->db->like('a.form_num', $_REQUEST["sub_county"])->or_like('a.form_num', $_REQUEST["sub_county"]);

            }


            $this->db->select()->from('vht a');

            //filtering
            isset($type) && $type != 'list_forms' ? ($type != 'new' ? $this->db->where(array('a.status' => $type)) : '') : '';

            isset($_REQUEST['search']['value']) && strlen($_REQUEST['search']['value']) > 0 ? $this->db->group_start()
                ->like('a.first_name', $_REQUEST['search']['value'])
                ->or_like('a.last_name', $_REQUEST['search']['value'])
                ->or_like('a.phone', $_REQUEST['search']['value'])->group_end(): ($this->session->user_type == 1 ? '' : $this->db->like('a.id', '000000000--------'));


            isset($_REQUEST["date_from"]) && strlen($_REQUEST["date_from"]) > 0 ? $this->db->where(array('a.date_created >=' => $_REQUEST["date_from"], 'a.date_created <=' => $_REQUEST["date_to"] . ' 24:59:59')) : '';


            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_search") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                isset($_REQUEST["customFromName"]) && strlen($_REQUEST["customFromName"]) > 0 ? $this->db->where('a.date_created >=', $_REQUEST["customFromName"]) : '';
                isset($_REQUEST["customToName"]) && strlen($_REQUEST["customToName"]) > 0 ? $this->db->where('a.date_created <=', $_REQUEST["customToName"] . ' 24:59:59') : '';
            }


            if (isset($_REQUEST["sub_county"]) && strlen($_REQUEST["sub_county"]) > 0) {

                $this->db->where($wher);

            }


            //end of filtering

            $this->db->order_by(key($o_list), current($o_list));

            $vht = $this->db->limit($iDisplayLength, $iDisplayStart)->get()->result();


            $no = 1;
            foreach ($vht as $sb):

                $output = ' <li>' . anchor($this->page_level . '/vht/edit/' . $sb->id*date('Y'), '<i class="fa fa-edit"></i> Edit') . '</li>';
                $output .= '<ul/>';

                //getting path and and location name
                $path = $this->locations->get_path($sb->village);


                $records["data"][] = array(

                    '
                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                            <input type="checkbox" class="group-checkable" name="id[]" value="' . $sb->id . '" />  <span></span>
                                                        </label>
                    ',

                    anchor($this->page_level . '/vht/edit/'  . $sb->id*date('Y'), strlen($sb->first_name)>0?ucwords($sb->first_name.' '.$sb->last_name):'<span style="color: red;">Registration not Completed</span>'),
                    $sb->phone,

                    $sb->code,
                    $path[1]['name'],
                    $path[3]['name'],
                    $path[4]['name'],

                    strlen($output) > 0 ?
                        ' <div class="btn-group">
                        <a class="btn green-jungle btn-sm" href="javascript:;" data-toggle="dropdown">
                            <i class="fa fa-cogs"></i> Action <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-right">' .
                        $output : ''

                );
                $no++; endforeach;


            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;

            echo json_encode($records);
        }

    function all_villages($id, $village = 'village')
        {
            $villages = '(';

            $village_array = $this->locations->get_villages_under_selection($id);
            $nov = count($village_array);
            $n1 = 1;
            foreach ($village_array as $v) {
                $villages .= " $village = $v ";
                $villages .= $nov == $n1 ? '' : ' OR ';
                $n1++;

            }
            $villages .= ')';
            return $villages;

        }

    function variance_reports($type = null)
        {


            $this->db->from('forms a');
            $trecords = $this->db->count_all_results();

            $iTotalRecords = $trecords;
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);


            $records = array();
            $records["data"] = array();

            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;


            $this->db->select('a.form_num,b.village')->from('forms a')
                ->join('vht b', 'a.phone=b.phone')//->join('registration c','a.form_num=c.form_num')
            ;

            //filtering

            isset($_REQUEST['search']['value']) && strlen($_REQUEST['search']['value']) > 0 ? $this->db->group_start()->like('a.form_num', $_REQUEST['search']['value'])->or_like('b.village', $_REQUEST['search']['value'])->group_end() : '';

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_search") {

                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)

                isset($_REQUEST["customFromName"]) && strlen($_REQUEST["customFromName"]) > 0 ? $this->db->where('a.date_created >=', $_REQUEST["customFromName"]) : '';


                isset($_REQUEST["customToName"]) && strlen($_REQUEST["customToName"]) > 0 ? $this->db->where('a.date_created <=', $_REQUEST["customToName"] . ' 24:59:59') : '';


//                $records["customActionMessage"] = "Group action successfully has been completed. Well done !".$_REQUEST["customSearchName"]." ".$_REQUEST["customSectorName"].' '.$_REQUEST["customPublicationName"]; // pass custom message(useful for getting status of group actions)

            }

            //end of filtering

            $forms = $this->db->order_by('a.id', 'desc')->limit($iDisplayLength, $iDisplayStart)->get()->result();


            $no = 1;
            foreach ($forms as $sb):


                //these are form actions
                $output = ' <li>' . anchor($this->page_level . '/house_hold_reg/view/' . $sb->form_num, '<i class="fa fa-eye"></i> View') . '</li>';

//                $output .= ' <li>' . anchor($this->page_level . '/house_hold_reg/new/' . $sb->form_num, '<i class="fa fa-plus"></i> Enter Data') . '</li>';

                $output .= ' <li>' . anchor($this->page_level . '/house_hold_reg/view/' . $sb->form_num . '/compare', '<i class="fa fa-exchange"></i> Compare') . '</li>';

                $output .= '<ul/>';


                $path = $this->locations->get_path($sb->village);

                $form_variations = $type == 'entry_variance' ? $this->custom_library->entry_variations($sb->form_num) : $this->custom_library->collection_variations($sb->form_num);

                $resolved = $form_variations['variance'] == $form_variations['resolved'] ? '<span class="label label-success" >' . $form_variations['resolved'] . '</span>' : '<span class="label label-warning" >' . $form_variations['resolved'] . '</span>';


                if ($form_variations['records'] > 0) {

                    $records["data"][] = array(

//                    '<input type="checkbox"  name="id[]" value="' . $sb->id . '">',

                        $no,
                        anchor($this->page_level . '/house_hold_reg/view/' . $sb->form_num . '/compare', $sb->form_num . '<br/><b>District:</b>' . $path[1]['name'], 'title="View"', 'target="_blank"'),
                        $path[4]['name'],

                        $form_variations['variance'],
                        $resolved,
                        $form_variations['entrant'],
                        $form_variations['validation'],

                        strlen($output) > 0 ?
                            ' <div class="btn-group">
                        <a class="btn green-jungle btn-sm" href="javascript:;" data-toggle="dropdown">
                            <i class="fa fa-cogs"></i> Action <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-right">' .
                            $output : ''


                    );
                    $no++;
                }
            endforeach;


            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;

            echo json_encode($records);
        }


    function distribution_list($village_id)
        {


            $vil = $this->locations->get_children($village_id);
            //print_r($vil);

            ?>


            <div class="table-scrollable">


                <table class="table table-condensed table-hover">
                    <thead>
                    <tr>
                        <th style="width: 5px;"> #</th>
                        <th> Village</th>
                        <th> #HouseHolds</th>
                        <th> #Nets</th>
                        <th> #Bails</th>
                        <th> #Extra</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php $vno = 1;
                    foreach ($vil as $v) { ?>


                        <?php $for_det = $this->db->select('a.id,person_vht')->from('registration a')->join('reg_detail b', 'b.data_id=a.id')->where('a.village', $v->id)->get()->result();
                        //print_r($for_det);
                        ?>
                        <tr>
                            <td> <?php echo $vno;
                                $vno++; ?>.
                            </td>
                            <td> <?php echo $v->name//.$v->id ?> </td>

                            <td>

                                <?php

                                $pop = 0;
                                $hh_net = 0;
                                foreach ($for_det as $dd) {

                                    //adding the total house holds
                                    $pop = $pop + $dd->person_vht;


                                    $original_num = bcdiv(($dd->person_vht / 2), 1, 0);
                                    $modulus_num = ($dd->person_vht % 2) == 0 ? 0 : 1;
                                    $nets = $original_num + $modulus_num;

                                    //computing the the nets
                                    $hh_net = $hh_net + $nets;

                                }

                                echo $pop;

                                //echo count($for_det);


                                ?>   </td>

                            <td>

                                <?php
                                //computing the the nets
                                echo $hh_net;

                                ?>


                            </td>

                            <td>  <?php
                                //these are the house holds $hh_net
                                //echo  $bb=round((50/40),0, PHP_ROUND_HALF_DOWN);

                                //50/40

                                $mod_bails = ($hh_net % 40);
                                $modulus_bails = $mod_bails == 0 ? 0 : 1;
                                echo bcdiv($hh_net, 40, 0) + $modulus_bails;


                                ?> </td>
                            <td>
                                <?php echo $mod_bails > 0 ? (40 - $mod_bails) : 0; ?>
                            </td>
                        </tr>
                    <?php } ?>

                    </tbody>
                </table>


            </div>


        <?php }


    function inventory($type = null)
        {


            $this->db->select('distinct(request_code)')->from('inventory a');


            if (isset($type)) {

                $type == 'pending' || $type == 'approved' ? $this->db->where(array('a.status' => $type)) : '';
            }


            $rqst = $this->db->get()->result();
//            strlen($type)>0?$this->db->where($where):'';
            $trecords = count($rqst);
            $iTotalRecords = $trecords;
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);


            $records = array();
            $records["data"] = array();

            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;


            $this->db->select('a.id,a.form_num,a.approved_by,a.approved_on,count(a.request_code) as forms,a.request_code,a.request_type,a.created_on,a.status as request_status,d.first_name,d.last_name')
                ->from('inventory a')
                ->join('users d', 'a.requested_by=d.id', 'left');
            if (isset($type)) {

                $type == 'pending' || $type == 'approved' ? $this->db->where(array('a.status' => $type)) : '';
                //$type=='release'||$type=='store'||$type=='returned' ? $this->db->where(array('request_type' => $type)) : '';
            }

            //filtering

            isset($_REQUEST['search']['value']) && strlen($_REQUEST['search']['value']) > 0 ? $this->db->group_start()
                ->like('d.last_name', $_REQUEST['search']['value'])
                ->or_like('d.first_name', $_REQUEST['search']['value'])
                ->or_like('a.request_code', $_REQUEST['search']['value'])->group_end() : '';

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_search") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                isset($_REQUEST["customFromName"]) && strlen($_REQUEST["customFromName"]) > 0 ? $this->db->where('a.created_on >=', strtotime($_REQUEST["customFromName"])) : '';
                isset($_REQUEST["customToName"]) && strlen($_REQUEST["customToName"]) > 0 ? $this->db->where('a.created_on <=', strtotime($_REQUEST["customToName"] . ' 24:59:59')) : '';
            }

            //end of filtering

            $forms = $this->db->order_by('a.id', 'desc')->group_by('request_code')->limit($iDisplayLength, $iDisplayStart)->get()->result();


            $no = 1;
            foreach ($forms as $sb):


//this is the form status
                if ($sb->request_status == 'pending') {

                    $request_status = '<div style="width:100px;" class="btn btn-sm btn-info">' . humanize($sb->request_status) . '</div>';
                }
                elseif ($sb->request_status == 'approved') {

                    $request_status = '<div style="width:100px;" class="btn btn-sm green-jungle">' . humanize($sb->request_status) . '</div>';

                }
                elseif ($sb->request_status == 'rejected') {

                    $request_status = '<div style="width:100px;" class="btn btn-sm btn-danger">' . humanize($sb->request_status) . '</div>';

                }
                else {
                    $request_status = '<div style="width:100px;" class="btn btn-sm btn-info">Pending</div>';
                }

                //this is the end of the form status


                $output = $this->custom_library->role_exist('View Inventory Details') ? ' <li>' . anchor($this->page_level . '/inventory/view/' . $sb->request_code, '<i class="fa fa-eye"></i> View') . '</li>' : '';

                if ($sb->request_status == 'pending') {
                    $output .= ' <li onclick="return confirm(\'are sure you want to do this ?\')">' . anchor($this->page_level . '/inventory/approve/' . $sb->request_code, '<i class="fa fa-plus"></i> Approve') . '</li>';
                    $output .= ' <li  onclick="return confirm(\'are sure you want to do this ?\')">' . anchor($this->page_level . '/inventory/reject/' . $sb->request_code, '<i class="fa fa-check"></i> Reject') . '</li>';
                }

                $output .= '<ul/>';


                $records["data"][] = array(

                    '
                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                            <input type="checkbox" class="group-checkable" name="id[]" value="' . $sb->id . '" />  <span></span>
                                                        </label>
                    ',

                    anchor($this->page_level . '/inventory/view/' . $sb->request_code, $sb->request_code, 'title="Click to view request"', 'target="_blank"'),
                    '<span style="white-space: nowrap;">' . trending_date($sb->created_on) . '</span>',
                    strlen($sb->first_name) > 0 ? $sb->first_name . ' ' . $sb->last_name : 'Not registered',

                    $sb->forms,
                    humanize($sb->request_type),

//                    $status,
                    $request_status,
                    $sb->approved_by != 0 ? $this->custom_library->get_user_full_name($sb->approved_by) : '',
                    $sb->approved_on != 0 ? trending_date($sb->approved_on) : '',

                    strlen($output) > 0 ?
                        ' <div class="btn-group">
                        <a class="btn green-jungle btn-sm" href="javascript:;" data-toggle="dropdown">
                            <i class="fa fa-cogs"></i> Action <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-right">' .
                        $output : ''


                );
                $no++; endforeach;


            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;

            echo json_encode($records);
        }


    function new_request($type = null)
        {


            $this->db->from('forms a');
//            strlen($type)>0?$this->db->where($where):'';
            $trecords = $this->db->count_all_results();
            $iTotalRecords = $trecords;
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);


            $records = array();
            $records["data"] = array();

            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;


            $this->db->select('a.id,a.form_num,b.phone,b.name,b.village,a.status')->from('forms a')->join('vht b', 'a.phone=b.phone');

            //filtering

            isset($_REQUEST['search']['value']) && strlen($_REQUEST['search']['value']) > 0 ? $this->db->group_start()
                ->like('a.form_num', $_REQUEST['search']['value'])
                ->or_like('b.phone', $_REQUEST['search']['value'])
                ->or_like('b.name', $_REQUEST['search']['value'])->group_end() : '';

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_search") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                isset($_REQUEST["customFromName"]) && strlen($_REQUEST["customFromName"]) > 0 ? $this->db->where('a.created_on >=', strtotime($_REQUEST["customFromName"])) : '';
                isset($_REQUEST["customToName"]) && strlen($_REQUEST["customToName"]) > 0 ? $this->db->where('a.created_on <=', strtotime($_REQUEST["customToName"] . ' 24:59:59')) : '';
            }

            //end of filtering

            $forms = $this->db->order_by('a.id', 'desc')->limit($iDisplayLength, $iDisplayStart)->get()->result();


            $no = 1;
            foreach ($forms as $sb):

                if ($sb->status == 'in_field') {

                    $status = '<div style="width:100px;" class="btn btn-sm btn-danger">' . humanize($sb->status) . '</div>';
                }
                elseif ($sb->status == 'in_store') {

                    $status = '<div style="width:100px;" class="btn btn-sm btn-warning">' . humanize($sb->status) . '</div>';
                }
                elseif ($sb->status == 'released') {

                    $status = '<div style="width:100px;" class="btn btn-sm btn-primary">' . humanize($sb->status) . '</div>';
                }
                elseif ($sb->status == 'returned') {

                    $status = '<div style="width:100px;" class="btn btn-sm green-jungle">' . humanize($sb->status) . '</div>';
                }
                else {
                    $status = '<div style="width:100px;" class="btn btn-sm btn-danger">In field</div>';
                }


                $output = ' <li>' . anchor($this->page_level . '/house_hold_reg/view/' . $sb->form_num, '<i class="fa fa-eye"></i> View') . '</li>';
                $output .= ' <li>' . anchor($this->page_level . '/house_hold_reg/new/' . $sb->form_num, '<i class="fa fa-plus"></i> Enter Data') . '</li>';
                $output .= ' <li>' . anchor($this->page_level . '/house_hold_reg/validate/' . $sb->form_num, '<i class="fa fa-check"></i> Validate') . '</li>';
                $output .= '<ul/>';

                //getting path and and location name
                $path = $this->locations->get_path($sb->village);
                //$village_name=$this->locations->get_location_name($sb->village);

                ///getting house holds and the number of people in the house holds
                $form_entered = $this->custom_library->house_holds($sb->form_num) ? $this->custom_library->house_holds($sb->form_num) : array();


                $records["data"][] = array(

                    '
                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                            <input type="checkbox" class="group-checkable" name="id[]" value="' . $sb->id . '" />  <span></span>
                                                        </label>
                    ',

                    anchor($this->page_level . '/house_hold_reg/view/' . $sb->form_num, $sb->form_num . '<br/><b>District:</b>' . $path[1]['name'], 'title="View"', 'target="_blank"'),
                    strlen($sb->name) > 0 ? '<b>Name:</b>' . $sb->name . '<br/><b>Phone:</b>' . $sb->phone : 'Not registered',

                    $path[1]['name'],
                    $path[2]['name'],
                    $path[4]['name'],


                    $status,

                    strlen($output) > 0 ?
                        ' <div class="btn-group">
                        <a class="btn green-jungle btn-sm" href="javascript:;" data-toggle="dropdown">
                            <i class="fa fa-cogs"></i> Action <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-right">' .
                        $output : ''


                );
                $no++; endforeach;


            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;

            echo json_encode($records);
        }


    function households($type = null)
        {


            if (isset($_REQUEST["sub_county"]) && strlen($_REQUEST["sub_county"]) > 0) {

                if (strlen($vill = $_REQUEST["village"]) > 0) {

                    $wher = $this->all_villages($vill, 'd.village');

                }
                elseif (strlen($vill = $_REQUEST["parish"]) > 0) {

                    $wher = $this->all_villages($vill, 'd.village');

                }
                elseif (strlen($vill = $_REQUEST["sub_county"]) > 0) {
                    $wher = $this->all_villages($vill, 'd.village');

                }
            }


            $this->db->where('d.confirm', 1);
            isset($wher) ? $this->db->where($wher) : '';
            $this->db->from('reg_detail a')->join('registration d', 'a.data_id=d.id');
            $rqst = $this->db->count_all_results();

            $trecords = $rqst;
            $iTotalRecords = $trecords;
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);


            $records = array();
            $records["data"] = array();

            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;


            $this->db->select('a.*,d.form_num,d.village')->from('reg_detail a')
                ->join('registration d', 'a.data_id=d.id')
                ->join('forms c', 'c.form_num=d.form_num')
                ->where('d.confirm', 1);

            //filtering

            isset($_REQUEST['search']['value']) && strlen($_REQUEST['search']['value']) > 0 ? $this->db->group_start()

                ->like('a.first_name', $_REQUEST['search']['value'])
                ->or_like('a.last_name', $_REQUEST['search']['value'])
                ->or_like('a.national_id', $_REQUEST['search']['value'])
                ->or_like('a.tel', $_REQUEST['search']['value'])->group_end() : '';

            isset($wher) ? $this->db->where($wher) : '';
            //end of filtering

            $forms = $this->db->order_by('village', 'asc')->limit($iDisplayLength, $iDisplayStart)->get()->result();


            $no = 1;
            foreach ($forms as $sb):


//this the status if the nets have been picked
                if ($sb->picked == 0) {

                    $net_picked = '<div class="btn btn-sm  btn-info">' . $sb->no_picked . '</div>';
                }
                elseif ($sb->picked == 1) {

                    $net_picked = '<div  class="btn btn-sm  green-jungle">' . $sb->no_picked . '</div>';

                }
                else {
                    $net_picked = '<div class="btn btn-sm btn-info">' . $sb->no_picked . '</div>';
                }

                //this is the end for the status


                $output = ' <li>' . anchor($this->page_level . '/house_hold_reg/view/' . $sb->form_num, '<i class="fa fa-eye"></i> View') . '</li>';
//                $output .= ' <li>' . anchor($this->page_level . '/house_hold_reg/new/' . $sb->form_num, '<i class="fa fa-plus"></i> Enter Data') . '</li>';
//                $output .= ' <li>' . anchor($this->page_level . '/house_hold_reg/validate/' . $sb->form_num, '<i class="fa fa-check"></i> Validate') . '</li>';
                $output .= '<ul/>';


                $final_pop=$sb->person_final!=0?$sb->person_final:$sb->person_vht;
                $hp= nets_agg_only($final_pop);

                $path = $this->locations->get_path($sb->village);

                $records["data"][] = array(

                    '
                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">  <input type="checkbox" class="group-checkable" name="id[]" value="' . $sb->id . '" />  <span></span> </label>
                    ',
                    anchor($this->page_level . '/house_hold_reg/view/' . $sb->form_num, (strlen($sb->last_name) ? $sb->last_name .' '. $sb->first_name : 'No Name'), 'title="Click to view request"', 'target="_blank"'),
                    $sb->national_id,
                    $sb->chalk_id,
                    $sb->tel,
                    $path[4]['name'],
                    $path[2]['name'],
                    $hp['population'],
                    $hp['house_hold_nets'],
                    $net_picked,
                    strlen($output) > 0 ?
                        ' <div class="btn-group">
                        <a class="btn green-jungle btn-sm" href="javascript:;" data-toggle="dropdown">
                            <i class="fa fa-cogs"></i> Action <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-right">' .
                        $output : ''




                );
                $no++; endforeach;


            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;

            echo json_encode($records);
        }




    function distribution_gap($type = null)
        {


            if (isset($_REQUEST["sub_county"]) && strlen($_REQUEST["sub_county"]) > 0) {

                if (strlen($vill = $_REQUEST["village"]) > 0) {

                    $wher = $this->all_villages($vill, 'd.village');

                }
                elseif (strlen($vill = $_REQUEST["parish"]) > 0) {

                    $wher = $this->all_villages($vill, 'd.village');

                }
                elseif (strlen($vill = $_REQUEST["sub_county"]) > 0) {
                    $wher = $this->all_villages($vill, 'd.village');

                }
            }


            $this->db->where('d.confirm', 1);
            isset($wher) ? $this->db->where($wher) : '';
            $this->db->from('distribution_gap a')->join('registration d', 'a.data_id=d.id');
            $rqst = $this->db->count_all_results();

            $trecords = $rqst;
            $iTotalRecords = $trecords;
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);


            $records = array();
            $records["data"] = array();

            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;


            $this->db->select('a.*,d.form_num,d.village')->from('distribution_gap a')
                ->join('registration d', 'a.data_id=d.id')
                ->join('forms c', 'c.form_num=d.form_num')
                ->where('d.confirm', 1);

            //filtering

            isset($_REQUEST['search']['value']) && strlen($_REQUEST['search']['value']) > 0 ? $this->db->group_start()

                ->like('a.first_name', $_REQUEST['search']['value'])
                ->or_like('a.last_name', $_REQUEST['search']['value'])
                ->or_like('a.national_id', $_REQUEST['search']['value'])
                ->or_like('a.tel', $_REQUEST['search']['value'])->group_end() : '';

            isset($wher) ? $this->db->where($wher) : '';
            //end of filtering

            $forms = $this->db->order_by('village', 'asc')->limit($iDisplayLength, $iDisplayStart)->get()->result();


            $no = 1;
            foreach ($forms as $sb):

                $final_pop=$sb->person_final!=0?$sb->person_final:$sb->person_vht;
                $hp= nets_agg_only($final_pop);



//this the status if the nets have been picked
                if ($sb->picked == 0) {

                    $net_picked = '<div class="btn btn-sm  btn-info">' . $sb->no_picked . '</div>';
                }
                elseif ($sb->picked == 1) {

                    $net_picked = '<div  class="btn btn-sm  green-jungle">' . $sb->no_picked . '</div>';

                }
                else {
                    $net_picked = '<div class="btn btn-sm btn-info">' . $sb->no_picked . '</div>';
                }

                //this is the end for the status


                //this is the part for the distribution gap


                if (($sb->no_picked !=0) && ($sb->no_picked != $nets=$hp['house_hold_nets'])) {

                    $gap = '<div class="btn btn-sm  btn-danger">' . ($nets-$sb->no_picked) . '</div>';
                }
                elseif (($sb->no_picked !=0) && ($sb->no_picked == $hp['house_hold_nets'])) {

                    $gap='';
                   // $net_picked = '<div  class="btn btn-sm  green-jungle">' . $sb->no_picked . '</div>';

                }
                else {

                    $gap='';
                   // $net_picked = '<div class="btn btn-sm btn-info">' . $sb->no_picked . '</div>';
                }

                //this the end of the part for distribution gap


                $output = ' <li>' . anchor($this->page_level . '/house_hold_reg/view/' . $sb->form_num, '<i class="fa fa-eye"></i> View') . '</li>';
//                $output .= ' <li>' . anchor($this->page_level . '/house_hold_reg/new/' . $sb->form_num, '<i class="fa fa-plus"></i> Enter Data') . '</li>';
//                $output .= ' <li>' . anchor($this->page_level . '/house_hold_reg/validate/' . $sb->form_num, '<i class="fa fa-check"></i> Validate') . '</li>';
                $output .= '<ul/>';


                $path = $this->locations->get_path($sb->village);

                $records["data"][] = array(

                    '
                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">  <input type="checkbox" class="group-checkable" name="id[]" value="' . $sb->id . '" />  <span></span> </label>
                    ',
                    anchor($this->page_level . '/house_hold_reg/view/' . $sb->form_num, (strlen($sb->last_name) ? $sb->last_name .' '. $sb->first_name : 'No Name'), 'title="Click to view request"', 'target="_blank"'),
                    $sb->national_id,
                    $sb->chalk_id,
                    $sb->tel,
                    $path[4]['name'],
                    $path[2]['name'],
                    $hp['population'],
                    $hp['house_hold_nets'],
                    $net_picked,
                    $gap,
                    strlen($output) > 0 ?
                        ' <div class="btn-group">
                        <a class="btn green-jungle btn-sm" href="javascript:;" data-toggle="dropdown">
                            <i class="fa fa-cogs"></i> Action <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-right">' .
                        $output : ''




                );
                $no++; endforeach;


            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;

            echo json_encode($records);
        }


    function village_report($type = null)
        {


            if (isset($_REQUEST["sub_county"]) && strlen($_REQUEST["sub_county"]) > 0) {

                if (strlen($vill = $_REQUEST["village"]) > 0) {

                    $wher = $this->all_villages($vill, 'a.village');

                }
                elseif (strlen($vill = $_REQUEST["parish"]) > 0) {

                    $wher = $this->all_villages($vill, 'a.village');

                }
                elseif (strlen($vill = $_REQUEST["sub_county"]) > 0) {
                    $wher = $this->all_villages($vill, 'a.village');

                }
            }


            $this->db->from('registration a')->where('confirm', 1);
            isset($wher) ? $this->db->where($wher) : '';
            $results = $this->db->group_by('a.village')->get()->result();
            $iTotalRecords = count($results);
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);

            $records = array();
            $records["data"] = array();

            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;

            $status_list = array(
                array("primary" => "Pending"),
                array("success" => "Completed"),
                array("danger" => "On Hold"),
                array("warning" => "Fraud")
            );


            $this->db->select('distinct(a.village) as village,b.name')->from('registration a')->join('locations_view b', 'a.village=b.id')->where('confirm', 1);

            isset($_REQUEST['search']['value']) && strlen($_REQUEST['search']['value']) > 0 ? $this->db->group_start()->like('b.name', $_REQUEST['search']['value'])->group_end() : '';

            isset($wher) ? $this->db->where($wher) : '';

            $form_serials = $this->db->order_by('a.id', 'desc')->group_by('a.village')->limit($iDisplayLength, $iDisplayStart)->get()->result();


            $no = 1;
            foreach ($form_serials as $q) {

                $for_det = $this->db->select('b.person_vht,b.person_final')
                    ->from('registration a')
                    ->join('reg_detail b', 'b.data_id=a.id')
                    ->where(array('a.village' => $q->village, 'confirm' => 1))->get()->result();


                $np = nets_popn_agg($for_det);
                $hp= nets_agg_only( $np['population']);

                $path = $this->locations->get_path($q->village);
                $records["data"][] = array(
                    ' <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                            <input type="checkbox" class="group-checkable" name="id[]" value="' . $q->village . '" />  <span></span>
                                                        </label>',
                    $path[1]['name'],
                    $path[2]['name'],
                    $path[3]['name'],
                    $q->name,
                    count($for_det),
                    $hp['population'],
                    $hp['house_hold_nets'],

                );
                $no++;
            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;

            echo json_encode($records);
        }


    function distribution_point($type = null)
        {


            $result = $this->db->from('distribution_points')->count_all_results();
            $iTotalRecords = $result;
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);

            $records = array();
            $records["data"] = array();

            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;

            $status_list = array(
                array("primary" => "Pending"),
                array("success" => "Completed"),
                array("danger" => "On Hold"),
                array("warning" => "Fraud")
            );


            if (isset($_REQUEST["sub_county"]) && strlen($_REQUEST["sub_county"]) > 0) {

                if (strlen($vill = $_REQUEST["village"]) > 0) {

                    $wher = $this->all_villages($vill, 'a.village');

                }
                elseif (strlen($vill = $_REQUEST["parish"]) > 0) {

                    $wher = $this->all_villages($vill, 'a.village');

                }
                elseif (strlen($vill = $_REQUEST["sub_county"]) > 0) {
                    $wher = $this->all_villages($vill, 'a.village');

                }
            }

            $this->db->select()
                ->from('distribution_points a');
            isset($_REQUEST['search']['value']) && strlen($_REQUEST['search']['value']) > 0 ? $this->db->group_start()
                ->like('a.point_name', $_REQUEST['search']['value'])->group_end() : '';



            $d_points = $this->db->order_by('a.id', 'desc')->limit($iDisplayLength, $iDisplayStart)->get()->result();


            $no = 1;
            foreach ($d_points as $q) {


                $np = $this->custom_library->getting_distribution_points($q->id);
                $hp= nets_agg_only( $np['population']);
                $path=$this->locations->get_path($q->subcounty);

                $records["data"][] = array(
                    ' <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                            <input type="checkbox" class="group-checkable" name="id[]" value="' . $q->id . '" />  <span></span>
                                                        </label>',


                    $path[1]['name'],
                    $path[2]['name'],
                    $q->point_name,
                    $np['village_no'],
                    $np['house_holds'],
                    $hp['population'],
                    $hp['house_hold_nets'],
                    $hp['bails'],
                    $hp['extra_bails'],


                );
                $no++;
            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;

            echo json_encode($records);
        }


    function parish_report($type = null)
        {

            $this->db->where('confirm', 1);
            $result = $this->db->from('registration a')->join('villages b', 'a.village=b.village')->group_by('b.parish')->get()->result();
            $iTotalRecords = count($result);
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);

            $records = array();
            $records["data"] = array();

            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;

            $status_list = array(
                array("primary" => "Pending"),
                array("success" => "Completed"),
                array("danger" => "On Hold"),
                array("warning" => "Fraud")
            );


            if (isset($_REQUEST["sub_county"]) && strlen($_REQUEST["sub_county"]) > 0) {

                if (strlen($vill = $_REQUEST["village"]) > 0) {

                    $wher = $this->all_villages($vill, 'a.village');

                }
                elseif (strlen($vill = $_REQUEST["parish"]) > 0) {

                    $wher = $this->all_villages($vill, 'a.village');

                }
                elseif (strlen($vill = $_REQUEST["sub_county"]) > 0) {
                    $wher = $this->all_villages($vill, 'a.village');

                }
            }

            $this->db->select('a.village,b.name,b.parish')
                ->from('registration a')
                ->join('villages b', 'a.village=b.village')
                ->where('confirm', 1);

            isset($_REQUEST['search']['value']) && strlen($_REQUEST['search']['value']) > 0 ? $this->db->group_start()
                ->like('b.name', $_REQUEST['search']['value']) ->group_end(): '';

            if (isset($_REQUEST["sub_county"]) && strlen($_REQUEST["sub_county"]) > 0) {

                $this->db->where($wher);

            }
            $parish = $this->db->order_by('a.id', 'desc')->group_by('parish')->limit($iDisplayLength, $iDisplayStart)->get()->result();


            $no = 1;
            foreach ($parish as $q) {


                $np = $this->custom_library->getting_villages_in_parish($q->parish);
                $hp= nets_agg_only( $np['population']);


                $path = $this->locations->get_path($q->parish);


                $records["data"][] = array(
                    ' <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                            <input type="checkbox" class="group-checkable" name="id[]" value="' . $q->village . '" />  <span></span>
                                                        </label>',
                    $path[1]['name'],
                    $path[2]['name'],
                    $path[3]['name'],
                    $np['village_no'],
                    $np['house_holds'],
                    $hp['population'],
                    $hp['house_hold_nets'],
                    $hp['bails'],
                    $hp['extra_bails'],


                );
                $no++;
            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;

            echo json_encode($records);
        }


    function sub_county_report($type = null)
        {

            $this->db->where('confirm', 1);
            $result = $this->db->from('registration a')->join('villages b', 'a.village=b.village')->group_by('b.sub_county')->get()->result();
            $iTotalRecords = count($result);
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);

            $records = array();
            $records["data"] = array();

            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;

            $status_list = array(
                array("primary" => "Pending"),
                array("success" => "Completed"),
                array("danger" => "On Hold"),
                array("warning" => "Fraud")
            );


            if (isset($_REQUEST["sub_county"]) && strlen($_REQUEST["sub_county"]) > 0) {

                if (strlen($vill = $_REQUEST["village"]) > 0) {

                    $wher = $this->all_villages($vill, 'a.village');

                }
                elseif (strlen($vill = $_REQUEST["parish"]) > 0) {

                    $wher = $this->all_villages($vill, 'a.village');

                }
                elseif (strlen($vill = $_REQUEST["sub_county"]) > 0) {
                    $wher = $this->all_villages($vill, 'a.village');

                }
            }


            $this->db->select('a.village,b.name,b.sub_county')
                ->from('registration a')
                ->join('villages b', 'a.village=b.village')
                ->where('confirm', 1);

            isset($_REQUEST['search']['value']) && strlen($_REQUEST['search']['value']) > 0 ? $this->db->group_start()
                ->like('b.name', $_REQUEST['search']['value'])->group_end() : '';

            if (isset($_REQUEST["sub_county"]) && strlen($_REQUEST["sub_county"]) > 0) {

                $this->db->where($wher);

            }

            $sub_county = $this->db->order_by('a.id', 'desc')->group_by('b.sub_county')->limit($iDisplayLength, $iDisplayStart)->get()->result();


            $no = 1;
            foreach ($sub_county as $q) {
                $np = $this->custom_library->getting_parish_in_sub_county($q->sub_county);
                $hp= nets_agg_only( $np['population']);

                $path = $this->locations->get_path($q->sub_county);

                $records["data"][] = array(
                    ' <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                            <input type="checkbox" class="group-checkable" name="id[]" value="' . $q->village . '" />  <span></span>
                                                        </label>',
                    $path[1]['name'],
                    $path[2]['name'],
                    $np['parish_no'],
                    $np['village_no'],
                    $np['house_holds'],
                    $hp['population'],
                    $hp['house_hold_nets'],
                    $hp['bails'],
                    $hp['extra_bails']


                );
                $no++;
            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;

            echo json_encode($records);
        }


    function district_report($type = null)
        {

            $this->db->where('confirm', 1);
            $result = $this->db->from('registration a')->join('villages b', 'a.village=b.village')->group_by('b.district')->get()->result();
            $iTotalRecords = count($result);
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);

            $records = array();
            $records["data"] = array();

            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;

            $status_list = array(
                array("primary" => "Pending"),
                array("success" => "Completed"),
                array("danger" => "On Hold"),
                array("warning" => "Fraud")
            );


            if (isset($_REQUEST["sub_county"]) && strlen($_REQUEST["sub_county"]) > 0) {

                if (strlen($vill = $_REQUEST["village"]) > 0) {

                    $wher = $this->all_villages($vill, 'a.village');

                }
                elseif (strlen($vill = $_REQUEST["parish"]) > 0) {

                    $wher = $this->all_villages($vill, 'a.village');

                }
                elseif (strlen($vill = $_REQUEST["sub_county"]) > 0) {
                    $wher = $this->all_villages($vill, 'a.village');

                }
            }


            $this->db->select('a.village,b.name,a.district')
                ->from('registration a')
                ->join('villages b', 'a.village=b.village')
                ->where('confirm', 1);

            isset($_REQUEST['search']['value']) && strlen($_REQUEST['search']['value']) > 0 ? $this->db->group_start()
                ->like('b.name', $_REQUEST['search']['value']) ->group_end(): '';

            if (isset($_REQUEST["sub_county"]) && strlen($_REQUEST["sub_county"]) > 0) {

                $this->db->where($wher);

            }

            $district = $this->db->order_by('a.id', 'desc')->group_by('b.district')->limit($iDisplayLength, $iDisplayStart)->get()->result();


            $no = 1;
            foreach ($district as $q) {
                $np = $this->custom_library->getting_sub_county_in_district($q->district);
                $hp= nets_agg_only( $np['population']);
                $path = $this->locations->get_path($q->district);

                $records["data"][] = array(
                    ' <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                            <input type="checkbox" class="group-checkable" name="id[]" value="' . $q->village . '" />  <span></span>
                                                        </label>',
                    $path[1]['name'],

                    $np['sub_county_no'],
                    $np['parish_no'],
                    $np['village_no'],
                    $np['house_holds'],
                    $hp['population'],
                    $hp['house_hold_nets'],
                    $hp['bails'],
                    $hp['extra_bails'],



                );
                $no++;
            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;

            echo json_encode($records);
        }


    function current_villages()
        {


            foreach ($this->db->select()->from('villages')->get()->result() as $v) {
                $path = $this->locations->get_path($v->village);


                $values = array(
                    'parish' => $path[3]['id'], 'sub_county' => $path[2]['id'], 'district' => $path[1]['id'], 'region' => $path[0]['id']
                );
                $this->db->where('id', $v->id)->where('parish', 0)->update('villages', $values);
            }
        }


    function get_vht_details($phone)
        {

            $vht = $this->db->select()->from('vht')->where('phone', phone($phone))->get()->row();

            if (count($vht) == 1) {


                ?>

                <div class="col-md-5 bold">
                    <?php

                    echo 'Phone : ' . $vht->phone;
                    echo '<br/>';
                    echo 'Name : ' . ($vht->first_name . ' ' . $vht->last_name);
                    echo '<br/>';
                    $path = $this->locations->get_path($vht->village);
                    echo 'District : ' . $path[1]['name'];
                    echo '<br/>';
                    echo 'Village : ' . $path[2]['name'] . ' - ' . $path[4]['name'];


                    ?>
                </div>

                <div class="col-md-4 pull-right">

                    <label class="control-label">Scan Barcode</label>
                    <input id="barcode" name="barcode" type="number" maxlength="6" minlength="6"
                           class="form-control input-xs" value=""/>
                    <?php echo form_error('barcode', '<label class="text-danger">', '</label>'); ?>
                    <label class="text-danger barcode"></label>


                </div>


                <!--        <script src="--><?php //echo base_url() ?><!--assets/global/plugins/jquery.min.js" type="text/javascript"></script>-->

                <script>

                    $('#barcode').keyup(function () {

                        var barcode = $('#barcode').val();
                        var phone = $('.phone').val();

                        if (barcode.length == 6) {

                            $.ajax({
                                type: 'GET',
                                url: '<?php echo base_url("index.php/ajax_api/add_form")?>/' + barcode + '/' + phone,
                                beforeSend: function () {
                                    $(".progress").html('<div style="text-align: center; "><i  class="fa fa-lg fa-spinner fa-pulse"></i></div>');
                                },
                                success: function (d) {

                                    $(".progress").html('')
                                    $(".serials").append(d);
                                    $('#barcode').val('');


                                }


                            });
                        }
                    });


                </script>


                <?php

            }
            else {
                $data = array(
                    'alert' => 'danger',
                    'message' => 'VHT user cannot be found  !!! Try Again',
                    'hide' => 1,
                    'width' => 'col-md-8'
                );
                $this->load->view('alert', $data);
            }
        }


    function add_form($form_num, $phone)
        {
            $f = $this->db->where('form_num', $form_num)->from('forms')->count_all_results();
            if ($f == 0) {

                echo $form_num . '<i class="fa fa-check text-success"></i>,';

                $values = array(
                    'form_num' => $form_num,
                    'phone' => phone($phone),
                    'status' => 'in_store',
                    'created_by' => $this->session->id,
                    'created_on' => time()
                );
                $this->db->insert('forms', $values);
            }
            else {
                echo $form_num . '<i class="fa fa-remove text-danger"></i>,';
            }
        }


    function coverage($type = null)
        {


            if (isset($type)) {
                $page_level2 = $this->uri->segment(2);

                $this->load->view("$this->page_level/$page_level2/$type");
            }
            else {
                $data = array(
                    'alert' => 'info',
                    'message' => 'Please select a category to view Coverage',
                    'hide' => 1
                );
                $this->load->view('alert', $data);
            }

        }


}


?>