<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class dashboard extends CI_Controller {

    public $username = "";
    public $password = "";
    public $page_level = "";
    public $page_level2 = "";

    function __construct()
        {
            parent::__construct();
            $this->page_level = $this->uri->slash_segment(1);
            $this->page_level2 = $this->uri->slash_segment(2);

        }

    public function index($page = null)
        {

            echo 'This is the first Page at ' . date('H:i:s');


        }


    function dashboard($type)
        {
            switch ($type) {
                default:
                    0;

                case 'percentage_distribution':

                    $nd= ($this->nets_picked()/$this->nets_allocation())*100;
                    echo round($nd,2);
                    break;

                case 'households':
                    echo $this->$type();
                    break;

                case 'head_count':
                    echo $this->$type();
                    break;

                case 'nets_allocation':
                    echo $this->$type();
                    break;

                case 'nets_picked':
                    echo $this->$type();
                    break;
            }
        }

    //these are the number of households
    function households()
        {
            $hh =$this->model->total_house_holds();

            return $hh;
            //return 2000;
        }

    //this is a method which helps in counting of the number of headcounts in the households
    function head_count()
        {
            $hc = $this->model->total_head_count();

            return $hc->person_vht;

        }


    //this is a method which helps in counting of the number of headcounts in the households
    function nets_allocation()
        {


            $for_det = $this->db->select('a.id,person_vht,person_final')->from('registration a')->join('reg_detail b', 'b.data_id=a.id')->where(array('a.confirm'=>1))->get()->result();



            $q=nets_popn_agg($for_det);

            return $q['house_hold_nets'];

        }


    //this is a method which helps in counting of the number of headcounts in the households
    function nets_picked()
        {
            $nets_picked = $this->db->select_sum('no_picked')->from('reg_detail')->where(array('picked' => 1))->get()->row();


            return $nets_picked = isset($nets_picked->no_picked) ? $nets_picked->no_picked : 0;
        }


    function top_household()
        {

            $th = $this->db->select()->from('reg_detail a')->join('registration b','b.id=a.data_id')->where('b.confirm',1)->order_by('person_vht', 'desc')->limit(6)->get()->result();

            foreach ($th as $r) {
                echo '<tr>

                            <td><a class="primary-link" href="javascript:;"> ' . $r->last_name.' '.$r->first_name  . ' </a></td>

                            <td>
                                    <span class="label label-sm label-warning pull-right">  ' . $r->person_vht . ' </span>
                            </td>
                        </tr>
';
            }
        }


    function top_villages()
        {

//            SELECT sum(person_vht) FROM llin2.registration a join reg_detail b on a.id=b.data_id where a.confirm=1 group by village;

            $qr = $this->db->select('sum(b.person_vht)as no_village,a.village')->from('registration a')->join('reg_detail b', 'a.id=b.data_id')->where(array('a.confirm'=>1))->order_by('sum(b.person_vht)','desc')->group_by('a.village')->limit(6)->get()->result();

            foreach ($qr as $q) {
                echo '<tr>

                            <td><a class="primary-link" href="javascript:;"> ' . $this->locations->get_location_name($q->village) . '</a> </td>

                            <td>
                                    <span class="label label-sm label-warning pull-right">  ' . $q->no_village . ' </span>
                            </td>
                        </tr>
                        
';
            }
        }


    function top_distributions()
        {
            echo '
                        <tr>
                            <td><a class="primary-link" href="javascript:;"> Point 1</a> </td>

                            <td>
                                    <span class="label label-sm label-warning pull-right">  33 </span>
                            </td>
                        </tr>
                      
                        <tr>
                            <td><a class="primary-link" href="javascript:;"> Point 2</a> </td>

                            <td>
                                    <span class="label label-sm label-warning pull-right">  33 </span>
                            </td>
                        </tr>
                      
                        <tr>
                            <td><a class="primary-link" href="javascript:;"> Point 3</a> </td>

                            <td>
                                    <span class="label label-sm label-warning pull-right">  33 </span>
                            </td>
                        </tr>
                      
';
        }



    function pie_chart()
        {
            //this chart represents the the forms in the field and in the store

            $in_store = $this->db->where(array('status' => 'in_store'))->from('forms')->count_all_results();
            $in_field = $this->db->where(array('status' => 'in_field'))->from('forms')->count_all_results();


            ?>

            <script type="text/javascript">
                $(function () {
                    $('#3d_pie').highcharts({
                        chart: {
                            type: 'pie',
                            options3d: {
                                enabled: true,
                                alpha: 45
                            }
                        },
                        title: {
                            text: 'Field'
                        },
                        subtitle: {
                            text: ''
                        },
                        plotOptions: {
                            pie: {
                                dataLabels: {
                                    enabled: false
                                },
                                cursor: 'pointer',
                                allowPointSelect: true,
                                innerSize: 100,
                                showInLegend: true,
                                depth: 45

                            }
                        },
                        series: [{
                            name: 'Total Amount',
                            data: [


                                ['In Field :<?php echo $in_field ?>', <?php echo $in_field ?>],


                                ['In Store : <?php echo $in_store ?>', <?php echo $in_store ?>]


                            ]
                        }]
                    });
                });
            </script>

        <?php }

    function pie_chart2()
        {

            //this chart represents the the forms in the field and in the store

            $in_store = $this->db->where(array('status' => 'in_store'))->from('forms')->count_all_results();
            $released = $this->db->where(array('status' => 'released'))->from('forms')->count_all_results();
            ?>
            <script type="text/javascript">
                $(function () {
                    $('#3d_pie2').highcharts({
                        chart: {
                            type: 'pie',
                            options3d: {
                                enabled: true,
                                alpha: 45
                            }
                        },
                        title: {
                            text: 'Inventory'
                        },
                        subtitle: {
                            text: ''
                        },
                        plotOptions: {
                            pie: {
                                dataLabels: {
                                    enabled: false
                                },
                                cursor: 'pointer',
                                allowPointSelect: true,
                                innerSize: 100,
                                showInLegend: true,
                                depth: 45

                            }
                        },
                        series: [{
                            name: 'Total Amount',
                            data: [

                                ['Released :<?php echo $released ?>', <?php echo $released ?>],


                                ['In Store : <?php echo $in_store ?>', <?php echo $in_store ?>]


                            ]
                        }]
                    });
                });
            </script>

        <?php }

    function pie_chart3()
        {
            //this picks the allocated nets VS picked

            $nets_picked = $this->db->select_sum('no_picked')->from('reg_detail')->where(array('picked' => 1))->get()->row();


            echo $nets_picked = isset($nets_picked->no_picked) ? $nets_picked->no_picked : 0;

            $for_det = $this->db->select('a.id,person_vht,person_final')->from('registration a')->join('reg_detail b', 'b.data_id=a.id')->where(array('a.confirm'=>1))->get()->result();
           $q=nets_popn_agg($for_det);

            // $pop;

            //echo count($for_det);


            ?>
            <script type="text/javascript">
                $(function () {
                    $('#3d_pie3').highcharts({
                        chart: {
                            type: 'pie',
                            options3d: {
                                enabled: true,
                                alpha: 45
                            }
                        },
                        title: {
                            text: 'Distribution'
                        },
                        subtitle: {
                            text: ''
                        },
                        plotOptions: {
                            pie: {
                                dataLabels: {
                                    enabled: false
                                },
                                cursor: 'pointer',
                                allowPointSelect: true,
                                innerSize: 100,
                                showInLegend: true,
                                depth: 45

                            }
                        },
                        series: [{
                            name: 'Total Amount',
                            data: [


                                ['Nets Allocated : <?php echo $q['house_hold_nets'] ?>', <?php echo $q['house_hold_nets'] ?>],


                                ['Nets Picked : <?php echo $nets_picked ?>',  <?php echo $nets_picked ?>],


                            ]
                        }]
                    });
                });
            </script>


        <?php }

    function pie_chart4()
        {

            //$qry1="SELECT DISTINCT(form_num)from registration";
            $qry = "SELECT count(form_num) as nums FROM `forms` WHERE `form_num` NOT IN(SELECT DISTINCT(form_num)from registration)";
            $qr = $this->db->query($qry)->row();

            $not_captured = $qr->nums;


            ///this is another part for the captured

            $captured = $this->db->select('distinct(form_num)')->from('registration')->get()->result();

            $captured = count($captured);


            ?>

            <script type="text/javascript">
                $(function () {
                    $('#3d_pie4').highcharts({
                        chart: {
                            type: 'pie',
                            options3d: {
                                enabled: true,
                                alpha: 45
                            }
                        },
                        title: {
                            text: 'Captured'
                        },
                        subtitle: {
                            text: ''
                        },
                        plotOptions: {
                            pie: {
                                dataLabels: {
                                    enabled: false
                                },
                                cursor: 'pointer',
                                allowPointSelect: true,
                                innerSize: 100,
                                showInLegend: true,
                                depth: 45

                            }
                        },
                        series: [{
                            name: 'Total Amount',
                            data: [


                                ['Captured :<?php echo $captured ?>', <?php echo $captured ?>],


                                ['Not Captured : <?php echo $not_captured ?>',<?php echo $not_captured ?>],


                            ]
                        }]
                    });
                });
            </script>

        <?php }


    function variance_report()
        {

            $this->load->library('table');


            $gc=$this->get_current_forms();
            $ev=$gc['entry_v'];
            $cv=$gc['collection_v'];
            $eforms=$gc['entry_forms'];
            $cforms=$gc['collection_forms'];

            $forms_worked_on=$gc['forms_worked_on'];
            $forms_resolved=$gc['forms_resolved'];
            $households=$gc['households'];


            //this is the row for the variances
            $title = array('data' => '<a class="primary-link" href="javascript:;">Variances</a>', 'class' => 'primary-l');
            $total = array('data' => $ev+$cv, 'class' => 'bold theme-font');
            $this->table->add_row($title, $ev,$cv, $total);

            //this is the row for the households
            $title = array('data' => '<a class="primary-link" href="javascript:;">Households</a>', 'class' => 'primary-l');
            $total = array('data' => $this->households(), 'class' => 'bold theme-font');
            $this->table->add_row($title, $households, '', $households);

            //this is the row for the Forms
            $title = array('data' => '<a class="primary-link" href="javascript:;">Forms</a>', 'class' => 'primary-l');
            $total = array('data' => $eforms+$cforms, 'class' => 'bold theme-font');
            $this->table->add_row($title,  $cforms, $eforms, $total);

            //this is the row for the Forms
            $title = array('data' => '<a class="primary-link" href="javascript:;">Resolved</a>', 'class' => 'primary-l');
            $total = array('data' => $this->resolved_variances(), 'class' => 'bold theme-font');
            $this->table->add_row($title, $forms_worked_on, $forms_resolved, $forms_worked_on+ $forms_resolved);


            echo $this->table->generate();

        }




        function get_current_forms(){
            $forms=$this->db->select('distinct(form_num)')->from('registration')->where('confirm',1)->get()->result();
          //  print_r($forms);


            $entry_v=0;
            $collection_v=0;

            $entry_forms=0;
            $collection_forms=0;

            $form_worked_on=0;
            $forms_resolved=0;

            $households=0;

            foreach ($forms as $r){
                $ev=$this->custom_library->entry_variations($r->form_num);
                $cv=$this->custom_library->collection_variations($r->form_num);

                $entry_v=($entry_v+$ev['variance']);
                $collection_v=($collection_v+$cv['variance']);

                $entry_forms=$entry_forms+($ev['variance']>0?1:0);
                $collection_forms=$collection_forms+($cv['variance']>0?1:0);

                $form_worked_on=$form_worked_on+($ev['forms_worked_on']>0?1:0);

                $forms_resolved=$forms_resolved+($ev['forms_resolved']>0?1:0);
                $households=$households+($ev['households']>0?1:0);

            }

            return array(
                'entry_v'=>$entry_v,
                'collection_v'=>$collection_v,
                'entry_forms'=>$entry_forms,
                'collection_forms'=>$collection_forms,
                'forms_worked_on'=>$form_worked_on,
                'forms_resolved'=>$forms_resolved,
                'households'=>$households,
            );
        }


//        $form_variations = $type == 'entry_variance' ? $this->custom_library->entry_variations($sb->form_num) : $this->custom_library->collection_variations($sb->form_num);

    function resolved_variances(){

      return $result=$this->db->where(array('confirm'=>1,'entry_times'=>3))->from('registration')->count_all_results();

    }
    function test_collection_v($form_num){
        $f=$this->custom_library->entry_variations($form_num);
        print_r($f);
    }


    function forms($form_type=null){
     if(isset($form_type)){
            return $result=$this->db->where(array('confirm'=>1))->from('registration')->count_all_results();
        }else{ return $result=$this->db->from('forms')->count_all_results();}

    }


    function data_entrants()
        {

            $this->load->library('table');


            $qry = $this->db->select('created_by, count(created_by) as nos')
                ->from('registration')
                ->group_by('created_by')
                ->order_by('count(created_by)', 'desc')
                ->limit(6)
                ->get()->result();


            foreach ($qry as $r):

                $title = array('data' => '<a class="primary-link" href="javascript:;">' . $this->custom_library->get_user_full_name($r->created_by) . '</a>', 'class' => 'primary-l');
                $total = array('data' => $r->nos, 'class' => 'pull-right bold theme-font');
                $this->table->add_row($title, $total);

            endforeach;


            echo $this->table->generate();

        }



    function form_entries()
        {

            $this->load->library('table');


            $qry = $this->db->select('created_on,form_num,created_by')
                ->from('registration')
                ->order_by('created_on', 'desc')
                ->limit(6)
                ->get()->result();


            foreach ($qry as $r):

                $title = array('data' => '<a class="primary-link" href="javascript:;">' . $r->form_num . '</a>', 'class' => 'primary-l');
                $date =array('data'=>trending_date($r->created_on),'style'=>'white-space:nowrap;');

                $this->table->add_row($title,$this->custom_library->get_user_full_name($r->created_by),$date);

            endforeach;


            echo $this->table->generate();

        }


    function recent_nets_picked()
        {

            $this->load->library('table');

            $qry = $this->db->select('last_name,first_name,picked_on,no_picked')
                ->from('reg_detail')
                ->where(array('picked' => 1))
                ->order_by('picked_on', 'desc')
                ->limit(6)
                ->get()->result();

            $no_results = array('data' => 'No records','colspan'=>3,'style'=>'text-align:center;');

            count($qry)==0?$this->table->add_row($no_results):'';

            foreach ($qry as $r):

                $head = array('data' => '<a class="primary-link" href="javascript:;">' . $r->last_name.' '.$r->first_name . '</a>');
                $date =array('data'=>trending_date($r->picked_on),'style'=>'white-space:nowrap;');

                $this->table->add_row($head, $r->no_picked, $date);

            endforeach;


            echo $this->table->generate();

        }



    function inventory()
        {

            $this->load->library('table');


            $qry =  $this->db->select('a.id,count(a.request_code) as forms,a.request_code,a.request_type,a.created_on,a.status as request_status,requested_by')
                ->from('inventory a')
                ->order_by('a.id', 'desc')->group_by('request_code')
                ->limit(6)
                ->get()->result();
            foreach ($qry as $r):


                //this is the form status
                if ($r->request_status == 'pending') {

                    $request_status = '<div style="width:80px;" class="btn btn-xs btn-info">' . humanize($r->request_status) . '</div>';
                }
                elseif ($r->request_status == 'approved') {

                    $request_status = '<div style="width:80px;" class="btn btn-xs green-jungle">' . humanize($r->request_status) . '</div>';

                }
                elseif ($r->request_status == 'rejected') {

                    $request_status = '<div style="width:80px;" class="btn btn-xs btn-danger">' . humanize($r->request_status) . '</div>';

                }
                else {
                    $request_status = '<div style="width:80px;" class="btn btn-xs btn-info">Pending</div>';
                }


                $title = array('data' => '<a class="primary-link" href="javascript:;">' . $r->request_code . '</a>', 'class' => 'primary-l');
                $date =array('data'=>trending_date($r->created_on),'style'=>'white-space:nowrap;');

                $this->table->add_row($title,$request_status, $date);

            endforeach;


            echo $this->table->generate();

        }



    public function logout()
        {
            $this->session->sess_destroy();
            redirect($this->page_level . 'login', 'refresh');
        }

    //this is the error page
    public function error()
        {
            $this->load->view('404');
        }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */