<?php
function microPayApproved(){
    if(!empty($_POST['txn_id']) and !empty($_POST['indicator'])){
        $trid = trim($_POST['txn_id']);
        $reference = trim($_POST['indicator']);
        $phone_no = $this->clean($_POST['phone']);
        $q = $this->select('micropay','status,updated_on,updated_by',"WHERE indicator='$reference'",null,null);
        if($this->numrows($q) == 1){
            $pay_status = $this->sqlResult($q,'status');
            if($pay_status !== 'APPROVED'){
                $url = 'https://mb.micropay.co.ug:4443/MicropayMobile/MicropayThirdpartyIntegrationsService';
                $content = 'MIXAKIDSQUERY_PAYMENT_STATUS'.$reference;
                $pkeyid = openssl_pkey_get_private("file:///var/www/vhosts/mixakids.com/httpdocs/certificates/mixakids.pem");
                openssl_sign($content, $signature, $pkeyid);
                $signature = base64_encode($signature);
                $data = array(
                    'PEER_USERNAME'=>'MIXAKIDS',
                    'ACTION'=>'QUERY_PAYMENT_STATUS',
                    'PROGRESS_INDICATOR'=>$reference,
                    'REQUEST_SIGNATURE'=>$signature
                );
                $data_string = json_encode($data);
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        'Content-Length: ' . strlen($data_string))
                );
                $result = curl_exec($ch);
                $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                $content_type = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
                $curlerr = curl_error($ch);
                curl_close($ch);
                if($http_status == 200){
                    $d = json_decode($result);
                    $return_code = $d->returnCode;
                    if($return_code == 0){
                        $obj = $d->returnObject;
                        $amount = $obj->amount;
                        $phone = $obj->requested_party;
                        $status = $obj->status;
                        if($status == 'APPROVED'){
                            $user = $this->userid();
                            $t = time();
                            $amount = $this->convertToUSD($amount,'UGX',$t);
                            $currentAmount = $this->userWallet($user,'current_amount');
                            $newBalance = $currentAmount + $amount;

                            $t_type = 'Deposit-MobileMoney#MicroPay#'.$phone;
                            $this->insert('user_statements','user,trans_type,amount,amount_before,amount_after,created_on,created_by',"'$user','$t_type','$amount','$currentAmount','$newBalance','$t','$user'");
                            $this->update('transactions',"status='completed'",$trid,'transation_id');
                            $this->update('micropay',"status='$status',updated_by='$user',updated_on='$t'",$reference,'indicator');
                            $this->update('wallet',"current_amount='$newBalance',updated_on='$t',updated_by='$user'",$user,'user');
                            $a = $this->convertPrice('USD',$amount,time());
                            $b = $this->convertPrice('USD',$newBalance,time());
                            echo $this->displayMessage('Your wallet is successfully updated by '.$a['price']['name'].' <b>'.number_format($a['price']['value'],2).'</b>. Your new balance is '.$b['price']['name'].' <b>'.number_format($b['price']['value'],2).'</b><br><a href="wallet?topup">You can topup again</a>','success');
                            echo '<h4>These are some books that you can rent or download</h4>';
                            $sort = "";
                            $this->showBooks($sort);
                        } else if($status == 'EXPIRED'){
                            echo $this->displayMessage('This transaction has expired!!. Micropay transactions expire if they take too long to be approved on the MicroPay App. Please initiate a new transaction and approve it. <a href="wallet?topup"><h4>Topup to be able to read and download these books</h4></a>','danger');
                            $sort = "";
                            $this->showBooks($sort);
                        } else if($status == 'DECLINED'){
                            echo $this->displayMessage('<h4>This payment is cancelled by you</h4>
								<p>You declined this transaction from your micropay account. Please, <a href="topup">Try Again</a> here to topup now. You may also be interested in the books below</p>','danger');
                            //echo '<a href="wallet?topup"><h4>Topup to be able to read and download these books</h4></a>';
                            $sort = "";
                            $this->showBooks($sort);
                        } else {
                            $amount = $obj->amount;
                            echo '<form action="?topup&micropay" method="post">
									<input type="hidden" name="phone" value="'.$phone_no.'" />
									<input type="hidden" name="amount" value="'.$amount.'" />
									<input type="hidden" name="txn_id" value="'.$trid.'" />
									<input type="hidden" name="method" value="micropay" />
									<input type="hidden" name="indicator" value="'.$reference.'" />
									'.$this->displayMessage('<h4>This payment is not approved</h4>
									<p><i class="fa fa-info"></i> Go to your MicroPay App, approve payment, then click <b>Confirm</b> below</p>','info').'
									<p><input type="submit" style="margin-left:20px !important;" name="topup" value="Confirm" class="btn btn-primary bg-color-blueDark dep-btn" /></p>
									<p><a href="wallet?topup"><small>Back to topup</small></a></p>
								</form>';
                        }
                    } else if($return_code == 404){
                        echo $this->displayMessage('The number you provided is not registered with MicroPay. Please use a number that is MicroPay registered or select another method<br><a href="wallet?topup"><small>Back to topup</small></a>','info');
                    } else {
                        echo $this->displayMessage('MicroPay Payment failed. Error message returned is '.$d->returnCode.' -> '.$d->returnMessage.'.<br><a href="wallet?topup"><small>Back to topup</small></a>','info');
                    }
                }
            } else {
                $updated = $this->sqlResult($q,'updated_on');
                $by = $this->sqlResult($q,'updated_by');
                echo $this->displayMessage('<h4>This payment is already approved</h4>
					<p>This transaction was already approved, and is recorded in your wallet</p>
					<p>Transaction was approved on '.date('d M, Y H:i',$updated).' by '.$this->userinfo($by,'full_name').'</p>
					<p><a href="wallet?topup"><small>Click here to topup more funds</small></a></p>','info');
                echo '<h4>These are some books that you can rent or download</h4>';
                $sort = "";
                $this->showBooks($sort);
            }
        }
    }
}
function pesapal_1(){
    include_once('OAuth.php');
    //pesapal params
    $token = $params = NULL;
    $consumer_key = 'NlwkEGZS5iwmAdQjra5Pi+N6x4x1xwGY';
    $consumer_secret = 'zuZr16dsfRywBcGPXz1pFTGCVXw=';
    $signature_method = new OAuthSignatureMethod_HMAC_SHA1();
    $iframelink = 'https://www.pesapal.com/API/PostPesapalDirectOrderV4';

    $amount = $this->clean($_POST['amount']);
    $desc = "MixaKids.Com Account Topup";
    $type = 'MERCHANT';
    $txn_id = $this->clean($_POST['txn_id']);
    $reference = $this->rand_string(10);
    $name = $this->myinfo('full_name');
    $first_name = substr($name,0,strpos($name,' '));
    $last_name = substr($name,strpos($name,' ')+1);
    $user = $this->userid();
    $t = time();
    $phonenumber = $this->clean($_POST['phone']);
    $email = $this->clean($_POST['email']);
    if($_SESSION['currency'] == 'KES' or $_SESSION['currency'] == 'KSH'){

    } else {
        $currency = $_SESSION['currency'];
        $amount = $this->convertToKES($amount,$currency,time());
        unset($_SESSION['currency']);
        $_SESSION['currency'] = 'KES';
    }
    $this->insert('pesapal','user,phone,email,amount,pp_merchant_ref,txn_id,created_on,created_by',"'$user','$phonenumber','$email','$amount','$reference','$txn_id','$t','$user'") or die(mysql_error());
    $amount = number_format($amount, 2);//format amount to 2 decimal places

    $callback_url = 'http://'.$_SERVER['HTTP_HOST'].'/wallet?topup&pesapal'; //redirect url, the page that will handle the response from pesapal.
    $post_xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?><PesapalDirectOrderInfo xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" Amount=\"".$amount."\" Description=\"".$desc."\" Type=\"".$type."\" Reference=\"".$reference."\" FirstName=\"".$first_name."\" LastName=\"".$last_name."\" Email=\"".(!empty($phonenumber) ? (strlen($phonenumber) >= 9 ? '' : $email) : $email)."\" PhoneNumber=\"".(!empty($email) ? ($this->check_email($email) ? '' : $phonenumber) : $phonenumber)."\" xmlns=\"http://www.pesapal.com\" />";
    $post_xml = htmlentities($post_xml);

    $consumer = new OAuthConsumer($consumer_key, $consumer_secret);

    //post transaction to pesapal
    $iframe_src = OAuthRequest::from_consumer_and_token($consumer, $token, "GET", $iframelink, $params);
    $iframe_src->set_parameter("oauth_callback", $callback_url);
    $iframe_src->set_parameter("pesapal_request_data", $post_xml);
    $iframe_src->sign_request($signature_method, $consumer, $token);

    //display pesapal - iframe and pass iframe_src
    ?>
    <iframe src="<?php echo $iframe_src;?>" width="100%" height="700px"  scrolling="no" frameBorder="0">
        <p>Browser unable to load iFrame</p>
    </iframe>
    <?php
}
function pesapal_2(){
    include_once('OAuth.php');
    $consumer_key = 'NlwkEGZS5iwmAdQjra5Pi+N6x4x1xwGY';
    $consumer_secret = 'zuZr16dsfRywBcGPXz1pFTGCVXw=';
    $statusrequestAPI = 'https://www.pesapal.com/api/querypaymentstatus';

    $pesapalNotification= 'CHANGE';
    $pesapalTrackingId = $_GET['pesapal_transaction_tracking_id'];
    $pesapal_merchant_reference = $_GET['pesapal_merchant_reference'];
    $signature_method = new OAuthSignatureMethod_HMAC_SHA1();
    if($pesapalNotification=="CHANGE" && $pesapalTrackingId!='')
    {
        $token = $params = NULL;
        $consumer = new OAuthConsumer($consumer_key, $consumer_secret);

        //get transaction status
        $request_status = OAuthRequest::from_consumer_and_token($consumer, $token, "GET", $statusrequestAPI, $params);
        $request_status->set_parameter("pesapal_merchant_reference", $pesapal_merchant_reference);
        $request_status->set_parameter("pesapal_transaction_tracking_id",$pesapalTrackingId);
        $request_status->sign_request($signature_method, $consumer, $token);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $request_status);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        if(defined('CURL_PROXY_REQUIRED')) if (CURL_PROXY_REQUIRED == 'True')
        {
            $proxy_tunnel_flag = (defined('CURL_PROXY_TUNNEL_FLAG') && strtoupper(CURL_PROXY_TUNNEL_FLAG) == 'FALSE') ? false : true;
            curl_setopt ($ch, CURLOPT_HTTPPROXYTUNNEL, $proxy_tunnel_flag);
            curl_setopt ($ch, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);
            curl_setopt ($ch, CURLOPT_PROXY, CURL_PROXY_SERVER_DETAILS);
        }

        $response = curl_exec($ch);

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $raw_header  = substr($response, 0, $header_size - 4);
        $headerArray = explode("\r\n\r\n", $raw_header);
        $header      = $headerArray[count($headerArray) - 1];

        $elements = preg_split("/=/",substr($response, $header_size));
        $status = $elements[1];

        curl_close ($ch);
        $user = $this->userid();
        $t = time();
        $q = $this->select('pesapal','id,amount,status,updated_on,updated_by,phone',"WHERE pp_merchant_ref='$pesapal_merchant_reference' AND user='$user'",null,null);
        if($this->numrows($q) == 1){
            $id = $this->sqlResult($q,'id');
            $this->update('pesapal',"pp_tracking_id='$pesapalTrackingId'",$id,'id');
            $amount = $this->sqlResult($q,'amount');
            $cur_status = $this->sqlResult($q,'status');
            $phone_number = $this->sqlResult($q,'phone');
            if($cur_status != 'COMPLETED'){
                if($status == 'COMPLETED'){
                    $amount = $this->convertToUSD($amount,'KES',$t);
                    $currentAmount = $this->userWallet($user,'current_amount');
                    $newBalance = $currentAmount + $amount;

                    $t_type = 'Deposit-PESAPAL#'.$phone_number;
                    $this->insert('user_statements','user,trans_type,amount,amount_before,amount_after,created_on,created_by',"'$user','$t_type','$amount','$currentAmount','$newBalance','$t','$user'");
                    $this->update('transactions',"status='completed'",$pesapal_merchant_reference,'transation_id');
                    $this->update('pesapal',"status='$status',pp_tracking_id='$pesapalTrackingId',updated_on='$t',updated_by='$user'",$id,'id');
                    $this->update('wallet',"current_amount='$newBalance',updated_on='$t',updated_by='$user'",$user,'user');
                    $a = $this->convertPrice('USD',$amount,time());
                    $b = $this->convertPrice('USD',$newBalance,time());
                    echo $this->displayMessage('Your wallet is successfully updated by '.$a['price']['name'].' <b>'.number_format($a['price']['value'],2).'</b>. Your new balance is '.$b['price']['name'].' <b>'.number_format($b['price']['value'],2).'</b><br><a href="wallet?topup" class="btn btn-info">You can topup again</a>','success');
                    echo '<h4>These are some books that you can rent or download</h4>';
                    $sort = "";
                    $this->showBooks($sort);
                } else {
                    echo $this->displayMessage('
					<h4>PENDING APPROVAL</h4>
					<p>This transaction is not yet approved by your provider. Please <a href="">click here</a> to try again.</p>
					<p><a href="" class="btn btn-primary" style="margin-right:20px">Check status</a><a href="wallet?topup" class="btn btn-default">Cancel</a></p>
					','info');
                }
                $this->update('pesapal',"status='$status',pp_tracking_id='$pesapalTrackingId',updated_on='$t',updated_by='$user'",$id,'id');
            } else {
                $updated = $this->sqlResult($q,'updated_on');
                $by = $this->sqlResult($q,'updated_by');
                echo $this->displayMessage('<h4>This payment is already approved</h4>
				<p>This transaction was already approved, and is recorded in your wallet</p>
				<p>Transaction was approved on '.date('d M, Y H:i',$updated).' by '.$this->userinfo($by,'full_name').'</p>
				<p><a href="wallet?topup"><small>Click here to topup more funds</small></a></p>','info');
                echo '<h4>These are some books that you can rent or download</h4>';
                $sort = "";
                $this->showBooks($sort);
            }
        } else {
            echo $this->displayMessage('<h4>Transaction not found</h4>
				<p>This transaction is not found in system. Contact administrator for more details</p>
				<p><a href="wallet?topup"><small>Click here to topup more funds</small></a></p>','info');
        }
    }
}
function yoGetTransID(){
    $user = $this->userid();
    $t = time();
    $phone_number = $this->clean($_POST['phone']);
    $reference = $this->clean($_POST['txn_id']);
    $amount = $this->clean($_POST['amount']);
    if($_SESSION['currency'] != 'UGX'){
        if($_SESSION['currency'] == 'KES' OR $_SESSION['currency'] == 'KSH'){
            $from_currency = $_SESSION['currency'];
        } else {
            $from_currency = 'USD';
        }
        $amount = $this->changeCurrency($amount,$from_currency,'UGX',time());
    }
    if(substr($phone_number,0,1) == '0'){
        if(strlen($phone_number) == 10){
            $phone_number = '256'.substr($phone_number,1);
        }
    }
    if(strlen($phone_number) == 9 and substr($phone_number,0,1) !== 0){
        $phone_number = '256'.$phone_number;
    }
    if(substr($phone_number,0,1) == '+'){
        $phone_number = substr($phone_number,1);
    }
    $name = $this->myinfo('full_name');
    $trid = $reference;
    $email = $this->userinfo($user,'email');
    $narrative = 'MixaKids Topup';
    $success_message = 'Topup successfull';
    $theData = '<?xml version="1.0" encoding="UTF-8"?>
			<AutoCreate>
				<Request>
					<APIUsername>100781460953</APIUsername>
					<APIPassword>8XzN-XD4F-Kf8C-vfk7-6sd5-nk3K-xfFb-4lik</APIPassword>
					<Method>acdepositfunds</Method>
					<NonBlocking>TRUE</NonBlocking>
					<Amount>'.$amount.'</Amount>
					<Account>'.$phone_number.'</Account>
					<AccountProviderCode>'.$email.'</AccountProviderCode>
					<Narrative>'.$narrative.'</Narrative>
					<ExternalReference>'.$reference.'</ExternalReference>
					<ProviderReferenceText>'.$success_message.'</ProviderReferenceText>
				</Request>
			</AutoCreate>';
    $url = 'https://paymentsapi1.yo.co.ug/ybs/task.php';

    define('XML_PAYLOAD', $theData);
    define('XML_POST_URL', $url);

    $stream_options = array(
        'http' => array(
            'method'  => 'POST',
            'header'  => 'Content-Type: text/xml' . "\r\n".'Content-transfer-encoding: text'."\r\n",
            'content' =>  XML_PAYLOAD));
    $host = 'paymentsapi1.yo.co.ug';
    $port = 80;

    $context  = stream_context_create($stream_options);
    $response = file_get_contents(XML_POST_URL, false, $context);

    $xml = simplexml_load_string($response);

    $status = $xml->Response->Status; //RETURNED STATUS , WE NEED SUCCEEDED OR PENDING
    $status_code = $xml->Response->StatusCode; //RETURNED STATUS OF THE TRANSACTION 0->SUCCESS, 1->PENDING, -9999->ERROR
    if($status_code == 1){
        if($status == 'OK'){
            $ref = $xml->Response->TransactionReference;
            //Display form here with instructions
            //record it as a pending Yo! transaction
            //submission will include status and check results
            $this->insert('yo_payments','user,phone,amount,indicator,status_code,txn_id,created_on,created_by',"'$user','$phone_number','$amount','$ref','$status_code','$reference','$t','$user'") or die(mysql_error());
            $nw = strtolower($_POST['nw']);
            echo '<form action="?topup&mm" method="post">
					<input type="hidden" name="phone" value="'.$phone_number.'" />
					<input type="hidden" name="trans_reference" value="'.$ref.'" />
					<input type="hidden" name="amount" value="'.$amount.'" />
					<input type="hidden" name="txn_id" value="'.trim($_POST['txn_id']).'" />
					<input type="hidden" name="method" value="MM" />
					'.$this->displayMessage('<h4>Payment Summary</h4>
					<p>You are about to deposit <b>UGX '.number_format($amount,2).'</b> to your wallet</p>
					<p>Phone Number : <strong>'.$phone_number.'</strong></p>
					<p>Amount : <strong>UGX '.number_format($amount,2).'</strong></p>
					<p>A popup will come on your screen for you to approve the payment. Follow the instructions to approve, then click \'Confirm\' below to topup your wallet</p>
					<p>If the popup does not appear, follow these instructions</p>
					<h5 class="margin-top-20">MTN MOBILE MONEY PAYMENT INSTRUCTIONS:</h5>
					<p>INSTRUCTIONS:</p>
					<ol type="1">
						<li>On your phone: Dial *165# to access MTN Mobile Money Menu</li>
						<li>Select 00 (NEXT) and press OK</li>
						<li>Select 11 (MY ACCOUNT) and press OK</li>
						<li>Select 1 (MY APPROVALS)</li>
						<li>Enter your PIN to confirm</li>
						<li>Select 1 (Transaction with the amount '.number_format($amount).'/)</li>
						<li>Select 1 (Yes) to complete approval</li>
						<li>Once you have approved the transaction, click \'Confirm\' button below to topup</li>
					</ol>','info').'
					<p><input type="submit" style="margin-right:20px !important;" name="topup" value="Confirm" class="btn btn-primary bg-color-blueDark dep-btn" /> &nbsp;&nbsp;&nbsp;<a href="wallet?topup" class="btn btn-default"><small>Cancel</small></a></p>
				</form>';
        } else {

        }
    } else {
        //check other status
        $this->yoPaymentStatusCode($status,$status_code,$xml,$amount,$phone_number,$reference);
    }
}
function yoApproveTransaction(){
    if(isset($_POST['trans_reference']) and isset($_POST['amount'])){
        $ref = $this->clean($_POST['trans_reference']);
        $amount = $this->clean($_POST['amount']);
        $reference = $this->clean($_POST['txn_id']);
        $phone_number = $this->clean($_POST['phone']);
        $q = $this->select('yo_payments','status,updated_on,updated_by',"WHERE indicator='$ref'",null,null);
        if($this->numrows($q) == 1){
            $pay_status = $this->sqlResult($q,'status');
            if($pay_status !== 'APPROVED'){
                $theData='<?xml version="1.0" encoding="UTF-8"?>
							<AutoCreate>
								<Request>
									<APIUsername>100781460953</APIUsername>
									<APIPassword>8XzN-XD4F-Kf8C-vfk7-6sd5-nk3K-xfFb-4lik</APIPassword>
									<Method>actransactioncheckstatus</Method>
									<TransactionReference>'.$ref.'</TransactionReference>
									<DepositTransactionType>PULL</DepositTransactionType>
								</Request>
							</AutoCreate>';

                $url = 'https://paymentsapi1.yo.co.ug/ybs/task.php';

                define('XML_PAYLOAD2', $theData);
                define('XML_POST_URL2', $url);

                $stream_options = array(
                    'http' => array(
                        'method'  => 'POST',
                        'header'  => 'Content-Type: text/xml' . "\r\n".'Content-transfer-encoding: text'."\r\n",
                        'content' =>  XML_PAYLOAD2));
                $host = 'paymentsapi1.yo.co.ug';
                $port = 80;
                $fp = @fsockopen($host, $port, $errno, $errstr);
                if ($fp){
                    $context  = stream_context_create($stream_options);
                    $response = file_get_contents(XML_POST_URL2, false, $context);

                    $load_xml = simplexml_load_string($response);
                    $status = $load_xml->Response->Status; //RETURNED STATUS , WE NEED SUCCEEDED OR PENDING
                    $status_code = $load_xml->Response->StatusCode; //RETURNED STATUS OF THE TRANSACTION 0->SUCCESS, 1->PENDING, -9999->ERROR
                    if($status_code == 0){
                        $amount = $load_xml->Response->Amount;
                        $init_date = $load_xml->Response->TransactionInitiationDate;
                        $complete_date = $load_xml->Response->TransactionCompletionDate;
                        $user = $this->userid();
                        $t = time();
                        $amount = $this->convertToUSD($amount,'UGX',$t);
                        $currentAmount = $this->userWallet($user,'current_amount');
                        $newBalance = $currentAmount + $amount;

                        $ct = strtotime($complete_date);
                        $it = strtotime($init_date);
                        $new_status = 'APPROVED';
                        $t_type = 'Deposit-MobileMoney#'.$this->phoneNetwork($phone_number).'#'.$phone_number;
                        $this->insert('user_statements','user,trans_type,amount,amount_before,amount_after,created_on,created_by',"'$user','$t_type','$amount','$currentAmount','$newBalance','$t','$user'");
                        $this->update('transactions',"status='completed'",$reference,'transation_id');
                        $this->update('yo_payments',"status='$new_status',updated_by='$user',updated_on='$ct',created_on='$it'",$ref,'indicator');
                        $this->update('wallet',"current_amount='$newBalance',updated_on='$t',updated_by='$user'",$user,'user');
                        $a = $this->convertPrice('USD',$amount,time());
                        $b = $this->convertPrice('USD',$newBalance,time());
                        echo $this->displayMessage('Your wallet is successfully updated by '.$a['price']['name'].' <b>'.number_format($a['price']['value'],2).'</b>. Your new balance is '.$b['price']['name'].' <b>'.number_format($b['price']['value'],2).'</b><br><a href="wallet?topup">You can topup again</a>','success');
                        echo '<h4>These are some books that you can rent or download</h4>';
                        $sort = "";
                        $this->showBooks($sort);
                    } else if($status_code == 1){
                        //Not yet approved, still pending. Show submit instructions
                        echo '<form action="?topup&mm" method="post">
							<input type="hidden" name="phone" value="'.$phone_number.'" />
							<input type="hidden" name="trans_reference" value="'.$ref.'" />
							<input type="hidden" name="amount" value="'.$amount.'" />
							<input type="hidden" name="txn_id" value="'.trim($_POST['txn_id']).'" />
							<input type="hidden" name="method" value="MM" />
							'.$this->displayMessage('<h4>This Transaction is still pending</h4>
							<p>Phone Number : <strong>'.$phone_number.'</strong></p>
							<p>Amount : <strong> UGX '.number_format($amount,2).'</strong></p>
							<p>A popup will come on your screen for you to approve the payment. Follow the instructions to approve, then click \'Confirm\' below to topup your wallet</p>
							<p>If the popup does not appear, follow these instructions</p>
							<h5 class="margin-top-20">MTN MOBILE MONEY PAYMENT INSTRUCTIONS:</h5>
							<p>INSTRUCTIONS:</p>
							<ol type="1">
								<li>On your phone: Dial *165# to access MTN Mobile Money Menu</li>
								<li>Select 00 (NEXT) and press OK</li>
								<li>Select 11 (MY ACCOUNT) and press OK</li>
								<li>Select 1 (MY APPROVALS)</li>
								<li>Enter your PIN to approve</li>
								<li>Select 1 (Transaction with the amount '.number_format($amount).'/)</li>
								<li>Select 1 (Yes) to complete approval</li>
								<li>Once you have approved the transaction, click \'Confirm\' button below to topup</li>
							</ol>','info').'
							<p><input type="submit" style="margin-right:20px !important;" name="topup" value="Confirm" class="btn btn-primary bg-color-blueDark dep-btn" /> &nbsp;&nbsp;&nbsp;<a href="wallet?topup" class="btn btn-default"><small>Cancel</small></a></p>
						</form>';
                    } else {
                        $this->yoPaymentStatusCode($status,$status_code,$load_xml,$amount,$phone_number,$reference);
                    }
                } else {
                    echo $this->displayMessage('Connection error : '.$errno.' => '.$errstr.'<br />','info');
                }
            } else {
                $updated = $this->sqlResult($q,'updated_on');
                $by = $this->sqlResult($q,'updated_by');
                echo $this->displayMessage('<h4>This payment is already approved</h4>
					<p>This transaction was already approved, and is recorded in your wallet</p>
					<p>Transaction was approved on '.date('d M, Y H:i',$updated).' by '.$this->userinfo($by,'full_name').'</p>
					<p><a href="wallet?topup"><small>Click here to topup more funds</small></a></p>','info');
                echo '<h4>These are some books that you can rent or download</h4>';
                $sort = "";
                $this->showBooks($sort);
            }
        } else {
            echo $this->displayMessage('This transaction was not found. Please contact administrator for more information','info');
        }
    }
}
function microPayDeposit(){
    $user = $this->userid();
    $phone = $this->clean($_POST['phone']);
    $reference = $this->clean($_POST['txn_id']);
    $txn_id = $reference;
    $url = 'https://mb.micropay.co.ug:4443/MicropayMobile/MicropayThirdpartyIntegrationsService';
    $or_amount = $this->clean($_POST['amount']);
    if($_SESSION['currency'] != 'UGX'){
        if($_SESSION['currency'] == 'KES' OR $_SESSION['currency'] == 'KSH'){
            $from_currency = 'KES';
        } else {
            $from_currency = 'USD';
        }
        $amount = $this->changeCurrency($or_amount,$from_currency,'UGX',time());
    } else {
        $amount = $or_amount;
    }
    $reason = $this->userinfo($user,'username').' account topup on MixaKids.com';
    $content = 'MIXAKIDSSEND_PAYMENT_REQUEST'.$phone.''.$amount.''.$reason;
    $pkeyid = openssl_pkey_get_private("file:///var/www/vhosts/mixakids.com/httpdocs/certificates/mixakids.pem");
    openssl_sign($content, $signature, $pkeyid);
    $signature = base64_encode($signature);
    $data = array(
        'PEER_USERNAME'=>'MIXAKIDS',
        'ACTION'=>'SEND_PAYMENT_REQUEST',
        'REQUESTED_PARTY'=>$phone,
        'AMOUNT'=>$amount,
        'REASON'=>$reason,
        'REQUEST_SIGNATURE'=>$signature
    );

    $data_string = json_encode($data);
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
    );
    $result = curl_exec($ch);
    $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    $content_type = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
    $curlerr = curl_error($ch);
    curl_close($ch);
    if($http_status == 200){
        $d = json_decode($result);
        $return_code = $d->returnCode;
        if($return_code == 0){
            $successCode = $d->progressIndicator;
            $date = time();
            $this->insert('micropay','user,phone,amount,indicator,txn_id,created_on,created_by',"'$user','$phone','$amount','$successCode','$txn_id','$date','$user'") or die(mysql_error());
            echo '<form action="?topup&micropay" method="post">
					<input type="hidden" name="phone" value="'.$phone.'" />
					<input type="hidden" name="amount" value="'.$amount.'" />
					<input type="hidden" name="txn_id" value="'.$txn_id.'" />
					<input type="hidden" name="method" value="micropay" />
					<input type="hidden" name="indicator" value="'.$successCode.'" />
					'.$this->displayMessage('<h4>Payment Summary</h4>
					<p>You are about to deposit <b>UGX '.number_format($amount,2).'</b> to your wallet</p>
					<p>Phone Number : <b>'.$phone.'</b></p>
					<p>Amount : <b>UGX '.number_format($amount,2).'</b></p>
					<p><i class="fa fa-info"></i> Go to your MicroPay App, approve payment, then click <b>Confirm</b> below</p>','info').'
					<p><input type="submit" style="margin-left:20px !important;" name="topup" value="Confirm" class="btn btn-primary bg-color-blueDark dep-btn" /></p>
					<p><a href="wallet?topup"><small>Back to topup</small></a></p>
				</form>';
        } else if($return_code == 404){
            echo $this->displayMessage('The number you provided is not registered with MicroPay. Please use a number that is MicroPay registered or select another method<br><a href="wallet?topup"><small>Back to topup</small></a>','info');
        } else {
            echo $this->displayMessage('MicroPay Payment failed. Error message returned is '.$d->returnCode.' -> '.$d->returnMessage.'.<br><a href="wallet?topup"><small>Back to topup</small></a>','info');
        }
    } else if($http_status == 404){
        echo $this->displayMessage('Wrong MicroPay URL. Admin has been notified<br><a href="wallet?topup"><small>Back to topup</small></a>','info');
    } else {
        echo $this->displayMessage('Internal error has occured.<br><a href="wallet?topup"><small>Back to topup</small></a>','info');
    }
}

?>