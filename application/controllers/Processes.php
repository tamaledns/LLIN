<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class processes extends CI_Controller {

    public $username = "";
    public $password = "";
    public $page_level = "";
    public $page_level2 = "";
    public $footer_news = "";


    function __construct()
        {
            parent::__construct();
            $this->page_level = $this->uri->slash_segment(1);
            $this->page_level2 = $this->uri->slash_segment(2);

        }

    public function index($page = null)
        {
            $data['view'] = 'home';
            $data['sublink'] = '';
            $data['page'] = $page;
            $this->login();
//            $this->home();
//
            $this->load->library('Custom_library');

            $this->custom_library->site_visits();


        }


    function update_messages()
        {
            $last_record = $this->db->select('created_on,message_time')->from('kannel_msgs')->order_by('id', 'desc')->get()->row();

            if (count($last_record) == 1) {

//$last_day= strtotime('friday this week');

                $begin_time = $last_record->created_on;
                $end_time = time();

                $begin = new DateTime(date('Y-m-d H:i:s', $begin_time));
                $end = new DateTime(date('Y-m-d  H:i:s', $end_time));
//$ldate=$last_day

                $interval = DateInterval::createFromDateString('1 seconds');
                $period = new DatePeriod($begin, $interval, $end);


                foreach ($period as $p) {

                    $time = date('Y-m-d H:i:s', strtotime($p->format("y-m-d H:i:s")));
                    $this->update_group($time);

                }

            }
            else {

                echo 'An error has occurred';

            }

//print_r($period);
        }


    function get_path(){

//        $path = '/var/log/kannel/';//live
//        $file = $path . 'access.log';//live

        $path=$system='/var/www/html/reports/uploads/logs_upload/';

//        $path = '/uploads/logs_upload/';//local
         $file = $path . 'access-Rwanda.log';//local

        echo $file;

        $searchfor = '2016-03-29 11:50:18';
        $contents = file_get_contents($file);
        // escape special characters in the query
        $pattern = preg_quote($searchfor, '/');
        // finalise the regular expression, matching the whole line
        $pattern = "/^.*$pattern.*\$/m";
        if (preg_match_all($pattern, $contents, $matches)) {

            var_dump($matches);
        }

    }


    function update_group($time)
        {


            //$path = 'uploads/logs_upload/access-Rwanda.log';//local
            // $file = $path . 'access-Rwanda.log';//local


            $path = 'var/log/kannel/';//live
            $file = $path . 'access.log';//live

            $searchfor = $time;

            // the following line prevents the browser from parsing this as HTML.
            //header('Content-Type: text/plain');

            // get the file contents, assuming the file to be readable (and exist)
            $contents = file_get_contents($file);
            // escape special characters in the query
            $pattern = preg_quote($searchfor, '/');
            // finalise the regular expression, matching the whole line
            $pattern = "/^.*$pattern.*\$/m";
            // search, and store all matching occurences in $matches
            if (preg_match_all($pattern, $contents, $matches)) {
                //    echo "Found matches:\n";
                //      ($line=str_replace(']',' ',$matches[0][0]));


                //this is the point where all arrays are collected
                foreach ($matches[0] as $d) {


                    $line = str_replace(']', ' ', $d);

                    //removing the leading "]" from the data
                    $ex = explode('[', $line);
                    //print_r($ex);

                    //date time
                    $date_string = explode(' ', $ex[0]);
                    $date = $date_string[0];
                    $time = $date_string[1];
                    $status = $date_string[2];

                    //other attributes

                    $smc = explode(':', $ex[1]);
                    $svc = explode(':', $ex[2]);
                    $to = explode(':', $ex[8]);
                    $msg = explode(':', $ex[10]);


                    //id, message_time, status, smsc, svc, msg_to, message, kannel_msd_id, created_on, id, id
                    $values = array(
                        'message_time' => strtotime(date($date . ' ' . $time)),
                        'status' => $status,
                        'smsc' => $smc[1],
                        'svc' => $svc[1],
                        'receiver' => $to[1],
                        'message' => $msg[2],
                        'kannel_msd_id' => $msg[1],
                        'created_on' => time()
                    );

                    if ($this->db->insert('kannel_msgs', $values)) {
                        echo '=>Database has been updated successfully for' . $time;
                    }


                }

            }
            else {
                echo "=>No matches found for " . $time;
            }

        }
    function email_templates(){
        $this->load->view('email_templates/email');
    }


    function sendHTMLEmail2($to, $subject, $message)
        {


            $mailto = $to;
            // $file="thanks.htm";
            $pcount = 0;
            $gcount = 0;
            $subject = $subject;
            $b = time();
            $pstr = $message;//$this->email_template($message);
            $gstr = $message;//$this->email_template($message);
            $from = "noreply@deltaits.net";


            $headers = sprintf("From: GEF<noreply@opm.co.ug>\r\n");
            $headers .= sprintf("MIME-Version: 1.0\r\n");
            $headers .= sprintf("Content-type: text/html; charset=utf-8\r\n");

            while (list($key, $val) = each($_POST)) {
                $pstr = $pstr . "$key : $val \n ";
                ++$pcount;

            }
            while (list($key, $val) = each($_GET)) {
                $gstr = $gstr . "$key : $val \n ";
                ++$gcount;

            }
            if ($pcount > $gcount) {
                $message_body = $message;//$pstr;
                // $message_body = $pstr;
                mail($mailto, $subject, $message_body, "From:" . $from);
                //$send = @mail($to, $subject, $body, $headers);

                $this->db->insert('outbox', array('to_user' => $mailto, 'subject' => $subject, 'm_type' => 'Email', 'message' => $message_body, 'created_on' => time(),));
                //  include("$file");
                return true;
            }
            else {
                $message_body = $gstr;

                if (!mail($mailto, $subject, $message_body, "From:" . $from)) {
//                die ("Not sent");
                    return false;
                }
                else {
                    // include("$file");
                    // print $b;
                    return true;
                }
            }

        }


    function email_template($body)
        {
            $logo = 'http://citiexpress.net/Ariane/wp-content/uploads/2016/05/Coat_of_arms_of_the_Republic_of_Uganda.svg.png';
            $html = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
				<html style="-webkit-text-size-adjust: none;-ms-text-size-adjust: none;">
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
				<title>Government Evaluation Facility | Email</title>
				<style type="text/css">
				html { -webkit-text-size-adjust:none; -ms-text-size-adjust: none;}
				@media only screen and (max-device-width: 680px), only screen and (max-width: 680px) {
					*[class="table_width_100"] {
						width: 96% !important;
					}
					*[class="border-right_mob"] {
						border-right: 1px solid #dddddd;
					}
					*[class="mob_100"] {
						width: 100% !important;
					}
					*[class="mob_center"] {
						text-align: center !important;
						padding: 0 !important;
					}
					*[class="mob_center_bl"] {
						float: none !important;
						display: block !important;
						margin: 0px auto;
					}
					.iage_footer a {
						text-decoration: none;
						color: #929ca8;
					}
					img.mob_display_none {
						width: 0px !important;
						height: 0px !important;
						display: none !important;
					}
					img.mob_width_50 {
						width: 40% !important;
						height: auto !important;
					}
					img.mob_width_80 {
						width: 80% !important;
						height: auto !important;
					}
					img.mob_width_80_center {
						width: 80% !important;
						height: auto !important;
						margin: 0px auto;
					}
					.img_margin_bottom {
						font-size: 0;
						height: 25px;
						line-height: 25px;
					}
				}
				.table_width_100 {
					width: 680px;
				}
				</style>
				</head>

				<body style="padding: 0px; margin: 0px;">
				<div id="mailsub" class="notification" align="center">

				<table width="100%" border="0" cellspacing="0" cellpadding="0" style="min-width: 320px;"><tr><td align="center" bgcolor="#eff3f8">


				<!--[if gte mso 10]>
				<table width="680" border="0" cellspacing="0" cellpadding="0">
				<tr><td>
				<![endif]-->

				<table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%" style="max-width: 680px;min-width: 300px;width: 680px;">
					<tr><td>
					<!-- padding --><div style="height: 80px; line-height: 80px; font-size: 10px;">&nbsp;</div>
					</td></tr>
					<!--header -->
					<tr><td align="center" bgcolor="#ffffff">
						<!-- padding --><div style="height: 10px; line-height: 10px; font-size: 10px;">&nbsp;</div>
						<table width="90%" border="0" cellspacing="0" cellpadding="0">
							<tr><td align="left"><!--

								Item --><div class="mob_center_bl" style="float: left; display: inline-block; width: 115px;">
									<table class="mob_center" width="180" border="0" cellspacing="0" cellpadding="0" align="left" style="border-collapse: collapse;">
										<tr><td align="left" valign="middle">
											<!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;">&nbsp;</div>
											<table width="180" border="0" cellspacing="0" cellpadding="0">
												<tr><td align="left" valign="top" class="mob_center">
													<a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 13px;">
													<font face="Arial, Helvetica, sans-seri; font-size: 13px;" size="3" color="#596167">
													<img src="' . $logo . '" alt="MixaKids" border="0" style="display: block;height:100px"></font></a>
												</td></tr>
											</table>
										</td></tr>
									</table></div><!-- Item END--><!--[if gte mso 10]>
									</td>
									<td align="right">
								<![endif]--><!--

								Item --><div class="mob_center_bl" style="float: right; display: inline-block; width: 88px;">
									<table width="88" border="0" cellspacing="0" cellpadding="0" align="right" style="border-collapse: collapse;">
										<tr><td align="right" valign="middle">
											<!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;">&nbsp;</div>
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr><td align="right">
													<!--social -->
													<div class="mob_center_bl" style="width: 88px;">
													<table border="0" cellspacing="0" cellpadding="0">
														<tr><td width="30" align="center" style="line-height: 19px;">
															<a href="https://www.facebook.com/opmuganda/" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
															<font face="Arial, Helvetica, sans-serif" size="2" color="#596167">
															<img src="http://artloglab.com/metromail3/images/facebook.gif" width="10" height="19" alt="Follow us on Facebook" border="0" style="display: block;"></font></a>
														</td><td width="39" align="center" style="line-height: 19px;">
															<a href="https://www.twitter.com/mixakids" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
															<font face="Arial, Helvetica, sans-serif" size="2" color="#596167">
															<img src="http://artloglab.com/metromail3/images/twitter.gif" width="19" height="16" alt="Follow us on Twitter" border="0" style="display: block;"></font></a>
														</td><!--<td width="29" align="right" style="line-height: 19px;">
															<a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
															<font face="Arial, Helvetica, sans-serif" size="2" color="#596167">
															<img src="http://artloglab.com/metromail3/images/dribbble.gif" width="19" height="19" alt="Dribbble" border="0" style="display: block;"></font></a>
														</td>--></tr>
													</table>
													</div>
													<!--social END-->
												</td></tr>
											</table>
										</td></tr>
									</table></div><!-- Item END--></td>
							</tr>
						</table>
						<!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;">&nbsp;</div>
					</td></tr>
					<!--header END-->

					<!--content 1 -->
					<tr><td align="center" bgcolor="#f8f8f8">
						<table width="90%" border="0" cellspacing="0" cellpadding="0">
							<tr><td align="left">
								<!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;">&nbsp;</div>
								<font face="Arial, Helvetica, sans-serif" size="4" color="#333" style="font-size: 15px;">
									<span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #333;">
										' . $body . '
									</span>
								</font>
								<!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;">&nbsp;</div>
							</td></tr>
						</table>
					</td></tr>
					<!--content 1 END-->
					<tr><td align="center" bgcolor="#ffffff" style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #eff2f4;">
						<table width="90%" border="0" cellspacing="0" cellpadding="0">
							<tr><td align="left">
						<!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;">&nbsp;</div>
						<div style="line-height: 22px;">
							<font face="Arial, Helvetica, sans-serif" size="5" color="#6b6b6b" style="font-size: 20px;">
								<span style="font-family: Arial, Helvetica, sans-serif; font-size: 20px; color: #6b6b6b;">
									Thank you for Subscribing with <strong>Governmnent evaluation Facility</strong>
								</span>
							</font>
						</div>
					<!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;">&nbsp;</div>
							</td></tr>
						</table>
					</td></tr>

					<!--footer -->
					<tr><td align="center" bgcolor="#ffffff">
						<!-- padding --><div style="height: 15px; line-height: 15px; font-size: 10px;">&nbsp;</div>
						<table width="90%" border="0" cellspacing="0" cellpadding="0">
							<tr><td align="left"><!--

								Item --><div class="mob_center_bl" style="float: left; display: inline-block; width: 115px;">
									<table class="mob_center" width="180" border="0" cellspacing="0" cellpadding="0" align="left" style="border-collapse: collapse;">
										<tr><td align="left" valign="middle">
											<!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;">&nbsp;</div>
											<table width="115" border="0" cellspacing="0" cellpadding="0">
												<tr><td align="left" valign="top" class="mob_center">
													<a href="http://www.opm.go.ug" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 13px;">
													<font face="Arial, Helvetica, sans-seri; font-size: 13px;" size="3" color="#596167">
													<img src="src="' . $logo . '"" alt="Government Evaluation Facility" border="0" style="display: block;width:100%" /><br>
													Support Team</font></a>
												</td></tr>
											</table>
										</td></tr>
									</table></div><!-- Item END--><!--[if gte mso 10]>
									</td>
									<td align="right">
								<![endif]--><!--

								Item --><div class="mob_center_bl" style="float: right; display: inline-block; width: 150px;">
									<table class="mob_center" width="150" border="0" cellspacing="0" cellpadding="0" align="right" style="border-collapse: collapse;">
										<tr><td align="right" valign="middle">
											<!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;">&nbsp;</div>
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr><td align="right">
													<!--social -->
													<div class="mob_center_bl">
													<table border="0" cellspacing="0" cellpadding="0">
														<tr><td align="center" style="line-height: 19px; padding-right: 20px;">
															<a href="http://opm.go.ug/" target="_blank" style="color: #9c9c9c; font-family: Arial, Helvetica, sans-serif; font-size: 12px; text-decoration: none;">
															<font face="Arial, Helvetica, sans-serif" size="2" color="#9c9c9c">
															FAQ</font></a>
														</td><td align="center" style="line-height: 19px; padding-right: 20px;">
															<a href="http://opm.go.ug/" target="_blank" style="color: #9c9c9c; font-family: Arial, Helvetica, sans-serif; font-size: 12px; text-decoration: none;">
															<font face="Arial, Helvetica, sans-serif" size="2" color="#9c9c9c">
															Blog</font></a>
														</td><td align="right" style="line-height: 19px;">
															<a href="http://opm.go.ug/" target="_blank" style="color: #9c9c9c; font-family: Arial, Helvetica, sans-serif; font-size: 12px; text-decoration: none;">
															<font face="Arial, Helvetica, sans-serif" size="2" color="#9c9c9c">
															Contact</font></a>
														</td></tr>
													</table>
													</div>
													<!--social END-->
												</td></tr>
											</table>
										</td></tr>
									</table></div><!-- Item END--></td>
							</tr>
						</table>
						<!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;">&nbsp;</div>
					</td></tr>
					<!--footer END-->
					<tr>
						<td>
							<p>&nbsp;</p>
							<p style="background:#e9e9e9;color:#333"><small>This email was sent automatically by <a href="http://opm.go.ug/">opm.go.ug/</a>. Please, do not reply</small></p>
						</td>
					</tr>
					<tr><td>
					<!-- padding --><div style="height: 80px; line-height: 80px; font-size: 10px;">&nbsp;</div>
					</td></tr>
				</table>
				<!--[if gte mso 10]>
				</td></tr>
				</table>
				<![endif]-->

				</td></tr>
				</table>

				</div>
				</body>
		</html>';
            return $html;
        }


    function sendHTMLEmail($to, $subject, $message)
        {
            $headers = sprintf("From: Government Evaluation Facility<noreply@opm.go.ug>\r\n");
            $headers .= sprintf("MIME-Version: 1.0\r\n");
            $headers .= "Reply-To: " . $to . "\r\n";
            $headers .= "Bcc: tamaledns@gmail.com\r\n";
            $headers .= sprintf("Content-type: text/html; charset=utf-8\r\n");
            $send = @mail($to, $subject, $message, $headers);
            if ($send) {
                return true;
            }
            else {
                return false;
            }
        }




    function sendMail($to, $subject, $message,$attachment=null){

        $this->load->library('email');
        $this->load->helper('email');

        $config['protocol'] = 'smtp';
        //$config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = TRUE;
        $config['useragent'] = 'smsone SMS reporting';
        $config['smtp_host'] = 'www.smsone.co.ug';
        $config['smtp_user'] = 'no-reply@smsone.co.ug';
        $config['smtp_pass'] = 'svuN82^6';
        $this->email->initialize($config);

        if(valid_email($to)) {

            $this->email->clear(TRUE);
            $this->email->from('no-reply@smsone.co.ug', 'smsone SMS reporting');
            $this->email->to($to);
            //$this->email->cc('another@another-example.com');
            //$this->email->bcc('them@their-example.com');

            $this->email->subject($subject);
//            $this->email->message($message);
            $data['message']=$message;
            isset($attachment)?$this->email->attach($attachment):'';
            $this->email->message($this->load->view('email_templates/email', $data, true));
            $this->email->set_mailtype('html');




            if($this->email->send())
            {
                $this->db->insert('outbox', array('to_user' => $to, 'subject' => $subject, 'm_type' => 'Email', 'message' => $message, 'created_on' => time(),));

                echo 'message sent';
                // return true;
            }else{
                echo 'message failed';
//                false;
            }

        }else{
            echo 'Message Failed firsttime';
            //return false;
        }
    }


    function daily_reports()
        {

            $r = $this->db->select('email,first_name')->from('users')->where(array('user_type' => 1, 'sub_type' => 1))->get()->result();
            if (count($r) > 0) {
                if ($attachment=$this->exporting_excel_reports('2016-09-01', '2016-09-30')) {


                    foreach($r as $em){

                        if (strlen($em->email) > 0) {

                            $message="Dear $em->first_name,<br/> Please find the attacment for the today's Report";

                            $this->sendMail($em->email,'Daily Sms Report',$message,$attachment);
                        }

                    }

                }
                else {
                    echo 'failed to create an attachment';
                }
            }
            else {
                echo 'No emails found';
            }
        }



    //tamaledns@gmail.com/testing Codeigniter/ Message sending

    function sd(){

        $this->sendMail('tamaledns@gmail.com','testing Maiil Reporting','Message sending');
        $this->sendMail('tamalednns@yahoo.com','testing Maiil Reporting','Message sending');
    }





    function exporting_excel_reports($begin_date = null, $end_date = null)
        {


            //this is where the period is determined for the report to export

            $begin_time = isset($begin_date) ? strtotime($begin_date) : strtotime(date('Y-m-d 00:00:00'));
            $end_time = isset($end_date) ? strtotime($end_date) : time();

//    $begin = new DateTime(date('Y-m-d H:i:s', $begin_time));
//    $end = new DateTime(date('Y-m-d  H:i:s', $end_time));
//
//    $interval = DateInterval::createFromDateString('1 seconds');
//    $period = new DatePeriod($begin, $interval, $end);


            // this is the end of the field


//this is the part where the export is initialised
            $this->load->view('php-excel-reader/Classes/PHPExcel');
            $objPHPexcel = PHPExcel_IOFactory::load('excel_templates/template.xlsx');
            $objWorksheet = $objPHPexcel->getActiveSheet(0);
///Exports depends on the level


            //THIS IS WHERE AM CREATING THE HEADER FOR THE REPORTS
            $networks = Networks('UG');
            $objWorksheet->getCell('A1')->setValue('ACCOUNTS');
            $letter = 'B';
            foreach ($networks as $u) {
                $title = strtoupper(humanize($u['title']));

                $objWorksheet->getCell($letter . 1)->setValue($title);
                $letter++;
            }
            $objWorksheet->getCell($letter . 1)->setValue('TOTAL');
            //this is the end of creating header for creating template


            $id = '';
            $no = 2;


            foreach ($this->db->select('id,sender')->from('sender_accounts')->get()->result() as $r) {

                $sender = strlen($r->sender) > 0 ? strtoupper($r->sender) : 'Not Defined';


                $objWorksheet->getCell('A' . $no)->setValue($sender);

                $letter = 'B';
                $sub_total = 0;
                foreach ($networks as $u) {

                    $code = strtoupper(humanize($u['code']));

                    $receiver = $u['code'];
                    $sqql = "SELECT COUNT(*) AS `numrows` FROM `sqlbox_sent_sms` `a` WHERE `sender` = '$r->sender'  AND `time` >= $begin_time AND `time` <= $end_time AND `receiver` LIKE '%$receiver%'  ESCAPE '!'";

                    $sm = $this->db->query($sqql)->row();

                    $smses = $sm->numrows;

                    $objWorksheet->getCell($letter . $no)->setValue($smses);
                    $letter++;

                    $sub_total = $sub_total + $smses;
                }
                $objWorksheet->getCell($letter . $no)->setValue($sub_total);


                $no++;
            }

//this is the part for the export
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPexcel, 'Excel5');

            $path = 'downloads/';
            if (!is_dir($path)) //create the folder if it's not already exists
            {
                mkdir($path, 0777, TRUE);
            }
            $objWriter->save($dir = $path . 'sms_report' . date('d-m-Y', time()) . '_' . time() . '.xls');
//redirect(base_url($dir));

            return $dir;


        }


function delete()
        {
            $this->db->where('id', 1);
            $this->db->delete('users');
        }


    public function request_code()
        {

            if (strlen($this->session->userdata('id')) > 0) {

                $verification_code = $this->GetSMSCode(5);


                $this->db->where('mobile = "' . $this->session->userdata('mobile') . '" and status = 0');
                $this->db->update('verification_codes', array('status' => 2));
                $this->db->insert('verification_codes',
                    array('mobile' => $this->session->userdata('mobile'),
                        'code' => $verification_code,
                        'created_at' => date('Y-m-d H:i:s')));

                $message = 'ePay Verification Code : ' . $verification_code;


                $values = array(
                    'created' => date('Y-m-d H:i:s'),
                    'sender' => 'ePay',
                    'receiver' => $this->session->userdata('mobile'),
                    'message' => $message,
                    'type' => 'VERIFICATION_CODE',
                    'mobile' => $this->session->userdata('mobile'),
                    'transactionid' => 0);

                $this->db->insert('outbox_messages', $values);


                $this->compticketmodel->SendSMS('ePay', $message, $this->session->userdata('mobile'));

                redirect('home', 'refresh');

            }
        }


    public function isloggedin()
        {
            if (strlen($this->session->userdata('id')) > 0) {
                return true;

            }
            else {
                return false;

            }

        }


    //checking wether the account exists
    public function account_exists($username, $password)
        {
            $status = 0;
            return $results = $this->db->select()->from('users')->where(array('username' => $username, 'password' => $this->hashValue($password)))->get()->row();

        }


    /**
     * @param $trans_type accountModification_accountCreation_accountDeletion_AccountChange
     * @param string $target
     * @param string $desc
     */
    function add_logs($user = null, $trans_type, $target = '', $desc = '')
        {
            $this->load->library('user_agent');
            $user = strlen($user) > 0 ? $user : $this->session->id;

            $values = array(
                'transaction_type' => $trans_type,
                'target' => character_limiter($target, 100),
                'details' => $desc,

                'created_by' => $user,
                'created_on' => time(),
                'platform' => $this->agent->platform(),
                'browser' => $this->agent->browser() . '-' . $this->agent->version(),
                'agent_string' => $this->agent->agent_string(),
                'ip' => $this->input->ip_address(),
                'agent_referral' => $this->agent->is_referral() ? $this->agent->referrer() : '',
            );
            $this->db->insert('logs', $values);
        }


    function SendSMS($sender, $destination, $message, $type = '')
        {
            $email = 'denis@timesolut.com';
            $password = 'shawnluvu';
            $url = 'http://caltonmobile.com/calton/api.php?';
            $parameters = 'username=[EMAIL]&password=[PASSWORD]&contacts=[DESTINATION]&message=[MESSAGE]&sender=[SENDERID]';
            $parameters = str_replace('[EMAIL]', $email, $parameters);
            $parameters = str_replace('[PASSWORD]', urlencode($password), $parameters);
            $parameters = str_replace('[DESTINATION]', $destination, $parameters);
            $parameters = str_replace('[MESSAGE]', urlencode($message), $parameters);
            $parameters = str_replace('[SENDERID]', urlencode($sender), $parameters);
            $post_url = $url . $parameters;
            $response = file($post_url);
            $this->db->insert('outbox_messages', array(
                'created' => time(),
                'sender' => 'Virtual Wallet',
                'receiver' => $destination,
                'message' => $message,
                'type' => $type,
                'mobile' => $sender
            ));
            return $response[0];
        }

    //function sending an email


    function test_ip()
        {
            $ip = $this->input->ip_address();

            //$ip='154.73.12.119';

            $this->load->library('Custom_library');

            $r = $this->custom_library->site_visits();

            //  print_r($r);

            echo($r->statusCode);


        }


    // This is the function getting code
    public function GetCode($length)
        {

            $codes = array();
            $chars = "01a2B3c4D5EF6G7H8I9jK";
            srand((double)microtime() * 1000000);
            $i = 0;
            $code = '';
            $serial = '';

            $i = 0;

            while ($i < $length) {
                $num = rand() % 10;
                $tmp = substr($chars, $num, 1);
                $serial = $serial . $tmp;
                $i++;
            }

            return $serial;

        }

    //this is the error page
    public function error()
        {
            $this->load->view('404');
        }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */