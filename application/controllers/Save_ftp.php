<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class save_ftp extends CI_Controller
{


    function __construct()
    {
        parent::__construct();
        $this->load->library('excel');

    }

    function index()
    {
        echo phpinfo();
    }

    function start_download()
    {
        $last_download = $this->db->select()->from('ftp_downloads')->order_by('id', 'desc')->get()->row();

        if (count($last_download) == 0) {
            $first_date = strtotime(date('2016-06-01'));
            $last_date = time();
        } else {
            $first_date = $last_download->created_on;
            $last_date = time();
        }


        $this->export_excel('save_ftp', $first_date, $last_date);

    }

    function export_excel($type = null, $first_date = null, $last_date = null, $id = null)
    {
        //this is the part where the export is initialised

        $objPHPexcel = PHPExcel_IOFactory::load('excel_templates/template.xlsx');
        $objWorksheet = $objPHPexcel->getActiveSheet(0);
        ///Exports depends on the level
        $records = 0;

        switch ($type) {

            default:
                echo 'default';
                break;

            case 'save_ftp':

                //echo $type;
                $l='A';
                $objWorksheet->getCell($l.'1')->setValue('#FormNo');$l++;
                $objWorksheet->getCell($l.'1')->setValue('HH First Name');$l++;
                $objWorksheet->getCell($l.'1')->setValue('HH Last Name');$l++;
                $objWorksheet->getCell($l.'1')->setValue('Telephone');$l++;
                $objWorksheet->getCell($l.'1')->setValue('ID number');$l++;
                $objWorksheet->getCell($l.'1')->setValue('Chalk ID');$l++;
                $objWorksheet->getCell($l.'1')->setValue('HH Popn VHT');$l++;
                $objWorksheet->getCell($l.'1')->setValue('HH Popn SC');$l++;
                $objWorksheet->getCell($l.'1')->setValue('HH Popn Final');$l++;
                $objWorksheet->getCell($l.'1')->setValue('Nets allocated');$l++;
                $objWorksheet->getCell($l.'1')->setValue('Nets distributed');$l++;
                $objWorksheet->getCell($l.'1')->setValue('village');$l++;
                $objWorksheet->getCell($l.'1')->setValue('parish');$l++;
                $objWorksheet->getCell($l.'1')->setValue('Subcounty');$l++;
                $objWorksheet->getCell($l.'1')->setValue('District');$l++;
                $objWorksheet->getCell($l.'1')->setValue('Distribution Point');$l++;
                $objWorksheet->getCell($l.'1')->setValue('Date of the sheet');$l++;
                $objWorksheet->getCell($l.'1')->setValue('VHT First Name');$l++;
                $objWorksheet->getCell($l.'1')->setValue('VHT Last Name');$l++;
                $objWorksheet->getCell($l.'1')->setValue('HH Signature');$l++;
                $objWorksheet->getCell($l.'1')->setValue('Date of Signature');$l++;
                $objWorksheet->getCell($l.'1')->setValue('LC1 Chairperson');$l++;
                $objWorksheet->getCell($l.'1')->setValue('Entry#');$l++;
                $objWorksheet->getCell($l.'1')->setValue('Final');$l++;
//
//                $where = "name='Joe' AND status='boss' OR status='active'";
//                $this->db->where($where);
//

                $where = ("((a.created_on > $first_date AND a.created_on <= $last_date) OR (a.updated_on > $first_date AND a.updated_on <= $last_date)) AND (b.entry_times = 1 OR (b.entry_times = 2 OR b.entry_times = 3)) ");

                $qry = $this->db->select('a.*,b.village,b.collection_date,b.lc1,c.first_name as vht_first_name,c.last_name as vht_last_name,b.form_num,b.entry_times,confirm')
                    ->from('reg_detail a')
                    ->join('registration b', 'a.data_id=b.id')
                    ->join('vht c', 'b.vhtphone=c.phone', 'left')
                    ->where($where)
                    //->order_by('a.id', 'desc')
                    ->get()->result();

                $no = 2;
                $records = 0;

                foreach ($qry as $c):


                    //this gets the path of the specified village
                    $path = $this->locations->get_path($c->village);

                    //get the distribution point

                    $dp = $this->db->select('point_name')->from('distribution_points')->where(array('subcounty' => $path[3]['id']))->get()->row();

                    $distribution_point = count($dp) == 1 ? $dp->point_name : '';


                    //THIS GETS THE AGGREGATED NETS
                    $final_pop=$c->person_final!=0?$c->person_final:$c->person_vht;
                    $hp = nets_agg_only($final_pop);
//
                    $le='A';
                    $objWorksheet->getCell($le . $no)->setValue('#'.$c->form_num);$le++;
                    $objWorksheet->getCell($le . $no)->setValue($c->first_name); $le++;
                    $objWorksheet->getCell($le . $no)->setValue($c->last_name);$le++;
                    $objWorksheet->getCell($le . $no)->setValue(phone($c->tel));$le++;
                    $objWorksheet->getCell($le . $no)->setValue($c->national_id);$le++;
                    $objWorksheet->getCell($le . $no)->setValue($c->chalk_id);$le++;
                    $objWorksheet->getCell($le . $no)->setValue($c->person_vht);$le++;
                    $objWorksheet->getCell($le . $no)->setValue($c->person_sc);$le++;
                    $objWorksheet->getCell($le . $no)->setValue($c->person_final);$le++;
                    $objWorksheet->getCell($le . $no)->setValue($hp['house_hold_nets']);$le++;
                    $objWorksheet->getCell($le . $no)->setValue($c->no_picked);$le++;
                    $objWorksheet->getCell($le . $no)->setValue($path[4]['name']);$le++;
                    $objWorksheet->getCell($le . $no)->setValue($path[3]['name']);$le++;
                    $objWorksheet->getCell($le . $no)->setValue($path[2]['name']);$le++;
                    $objWorksheet->getCell($le . $no)->setValue($path[1]['name']);$le++;
                    $objWorksheet->getCell($le . $no)->setValue($distribution_point);$le++;
                    $objWorksheet->getCell($le . $no)->setValue($c->collection_date != 0 ? date('Y-m-d H:i:s', $c->collection_date) : '');$le++;
                    $objWorksheet->getCell($le . $no)->setValue($c->vht_first_name);$le++;
                    $objWorksheet->getCell($le . $no)->setValue($c->vht_last_name);$le++;
                    $objWorksheet->getCell($le . $no)->setValue($c->signed);$le++;
                    $objWorksheet->getCell($le . $no)->setValue($c->picked_on != 0 ? date('Y-m-d H:i:s', $c->picked_on) : '');$le++;
                    $objWorksheet->getCell($le . $no)->setValue($c->lc1);$le++;
                    $objWorksheet->getCell($le . $no)->setValue($c->entry_times);$le++;
                    $objWorksheet->getCell($le . $no)->setValue($c->confirm==1?'Yes':'');$le++;


                    $no++;
                    $records++;
                endforeach;

                break;


        }


        $objWriter = PHPExcel_IOFactory::createWriter($objPHPexcel, 'CSV');

        $path = 'amf_downloads/';
        if (!is_dir($path)) //create the folder if it's not already exists
        {
            mkdir($path, 0777, TRUE);
        }
        $file_name = $first_date . ' to ' . $last_date . '.csv';

        if ($records > 0) {
            $objWriter->save($dir = $path . $file_name);

            // force_download($dir, NULL);

            $values = array(
                'file_name' => $file_name,
                'file_path' => $dir,
                'records' => $records,
                'first_date' => $first_date,
                'last_date' => $last_date,
                'created_on' => time()
            );


            //redirect(base_url($dir));
        } else {
            $values = array(
                'file_name' => $file_name,
                'file_path' => '',
                'records' => $records,
                'first_date' => $first_date,
                'last_date' => $last_date,
                'created_on' => time()
            );
        }
        $this->db->insert('ftp_downloads', $values);

        echo " $records records have been exported";


    }

}

?>


