<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class excel_export extends CI_Controller
{


    function __construct()
    {
        parent::__construct();

        $this->isloggedin() == true ? '' :$this->invalid();
        $this->load->library('excel');

        $this->page_level = $this->uri->segment(1);
        $this->page_level2 = $this->uri->segment(2);
        $this->page_level3 = $this->location_title= $this->uri->segment(3);

    }


    function invalid(){

        echo "You can't access this report Contact administrator";
        exit;
    }

    public function isloggedin()
    {
        return $this->session->userdata('user_type') == 1 || $this->session->userdata('user_type') == 2 || $this->session->userdata('user_type') == 3 || $this->session->userdata('user_type') == 4 || $this->session->userdata('user_type') == 5|| $this->session->userdata('user_type') == 6 ? true : false;

    }


    function index()
    {
        echo phpinfo();
    }

    function users()
    {

        //this is the part where the export is initialised

        $objPHPexcel = PHPExcel_IOFactory::load('excel_templates/template.xlsx');
        $objWorksheet = $objPHPexcel->getActiveSheet(0);
        ///Exports depends on the level


        $header = array('Name', 'Phone', 'Location', 'username', 'Email',);

        $L='A';
        $hno=1;
        foreach ($header  as $h){
            $objWorksheet->getCell($L.$hno)->setValue($h);
            $L++;
        }
// Data

        $no = 2;
        $id = $this->db->select()->from('users')->get()->result();
        foreach ($id as $d) {


            $objWorksheet->getCell('A' . $no)->setValue(humanize($d->first_name . ' ' . $d->last_name));
            $objWorksheet->getCell('B' . $no)->setValue(phone($d->phone));
            $objWorksheet->getCell('C' . $no)->setValue($d->city);
            $objWorksheet->getCell('D' . $no)->setValue($d->username);
            $objWorksheet->getCell('E' . $no)->setValue($d->email);

            $no++;
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPexcel, 'Excel5');

        $path = 'downloads/';
        if (!is_dir($path)) //create the folder if it's not already exists
        {
            mkdir($path, 0777, TRUE);
        }
        $file_name =  'LLIN ' . $this->page_level2 . ' export ' . time() . '.xls';

        $objWriter->save($dir = $path . $file_name);

        force_download($dir, NULL);
    }

    function vht()
    {


        //this is the part where the export is initialised

        $objPHPexcel = PHPExcel_IOFactory::load('excel_templates/template.xlsx');
        $objWorksheet = $objPHPexcel->getActiveSheet(0);
        ///Exports depends on the level
        $header = array('Name', 'Phone', 'Code', 'District', 'Parish', 'Village');

        $L='A';
        $hno=1;
        foreach ($header  as $h){
            $objWorksheet->getCell($L.$hno)->setValue($h);
            $L++;
        }


// Data

        $no = 2;
        $id = $this->db->select()->from('vht')->get()->result();


        foreach ($id as $d) {

            $path = $this->locations->get_path($d->village);


            $le='A';
            $objWorksheet->getCell($le . $no)->setValue(humanize($d->first_name . ' ' . $d->last_name));$le++;
            $objWorksheet->getCell($le . $no)->setValue(phone($d->phone));$le++;
            $objWorksheet->getCell($le . $no)->setValue($d->code);$le++;
            $objWorksheet->getCell($le . $no)->setValue($path[4]['name']);$le++;
            $objWorksheet->getCell($le . $no)->setValue($path[3]['name']);$le++;
            $objWorksheet->getCell($le . $no)->setValue($path[1]['name']);$le++;


            $no++;
        }




        $objWriter = PHPExcel_IOFactory::createWriter($objPHPexcel, 'Excel5');

        $path = 'downloads/';
        if (!is_dir($path)) //create the folder if it's not already exists
        {
            mkdir($path, 0777, TRUE);
        }
        $file_name =  'LLIN ' . $this->page_level2 . ' export ' . time() . '.xls';

        $objWriter->save($dir = $path . $file_name);

        force_download($dir, NULL);
    }

    function village($vill = null)
    {


        //this is the part where the export is initialised

        $objPHPexcel = PHPExcel_IOFactory::load('excel_templates/template.xlsx');
        $objWorksheet = $objPHPexcel->getActiveSheet(0);
        ///Exports depends on the level
        $header = array( 'Sub County', 'Parish', 'Village', 'Households', 'Popn', 'Nets');

        $L='A';
        $hno=1;
        foreach ($header  as $h){
            $objWorksheet->getCell($L.$hno)->setValue($h);
            $L++;
        }


        $no = 1;


        if (isset($vill)) {

            $wher = $this->all_villages($vill, 'a.village');

        }

        $this->db->select('distinct(a.village) as village,b.name')->from('registration a')->join('locations_view b', 'a.village=b.id')->where('confirm', 1);

        isset($wher) ? $this->db->where($wher) : '';

        $qry = $this->db->order_by('a.id', 'desc')->group_by('a.village')->get()->result();

        $no=2;
        foreach ($qry as $q) {


            $for_det = $this->db->select('b.person_vht,b.person_final')
                ->from('registration a')
                ->join('reg_detail b', 'b.data_id=a.id')
                ->where(array('a.village' => $q->village, 'confirm' => 1))->get()->result();


            $np = nets_popn_agg($for_det);

            $path = $this->locations->get_path($q->village);


            $le='A';
            $objWorksheet->getCell($le . $no)->setValue($path[2]['name']);$le++;
            $objWorksheet->getCell($le . $no)->setValue($path[3]['name']);$le++;
            $objWorksheet->getCell($le . $no)->setValue($q->name);$le++;
            $objWorksheet->getCell($le . $no)->setValue(count($for_det));$le++;
            $objWorksheet->getCell($le . $no)->setValue($np['population']);$le++;
            $objWorksheet->getCell($le . $no)->setValue($np['house_hold_nets']);$le++;

            $no++;
        }


        $objWriter = PHPExcel_IOFactory::createWriter($objPHPexcel, 'Excel5');

        $path = 'downloads/';
        if (!is_dir($path)) //create the folder if it's not already exists
        {
            mkdir($path, 0777, TRUE);
        }
        $file_name =  'LLIN ' . $this->page_level2 . ' export ' . time() . '.xls';

        $objWriter->save($dir = $path . $file_name);

        force_download($dir, NULL);


    }

    function all_villages($id, $village = 'village')
    {
        $villages = '(';

        $village_array = $this->locations->get_villages_under_selection($id);
        $nov = count($village_array);
        $n1 = 1;
        foreach ($village_array as $v) {
            $villages .= " $village = $v ";
            $villages .= $nov == $n1 ? '' : ' OR ';
            $n1++;

        }
        $villages .= ')';
        return $villages;

    }

    function households($vill = null)
    {

        //this is the part where the export is initialised

        $objPHPexcel = PHPExcel_IOFactory::load('excel_templates/template.xlsx');
        $objWorksheet = $objPHPexcel->getActiveSheet(0);
        ///Exports depends on the level


        $header = array('Household', 'ID Number', 'Chalk ID', 'Phone', 'Village', 'Subcounty', 'Popn', 'Nets', 'Picked');

        $L='A';
        $hno=1;
        foreach ($header  as $h){
            $objWorksheet->getCell($L.$hno)->setValue($h);
            $L++;
        }
// Data


        $no = 2;


        if (isset($vill)) {

            $wher = $this->all_villages($vill, 'd.village');

        }


        $this->db->select('a.*,d.form_num,d.village')->from('reg_detail a')
            ->join('registration d', 'a.data_id=d.id')
            ->join('forms c', 'c.form_num=d.form_num')
            ->where('d.confirm', 1);

        isset($wher) ? $this->db->where($wher) : '';

        $qry = $this->db->order_by('d.village', 'asc')->get()->result();

        foreach ($qry as $sb) {

            $final_pop=$sb->person_final!=0?$sb->person_final:$sb->person_vht;
            $nets=nets_agg_only($final_pop);


            $path = $this->locations->get_path($sb->village);
            //this is the end for the status


            $le='A';
            $objWorksheet->getCell($le . $no)->setValue((strlen($sb->last_name) ? $sb->last_name.' '. $sb->first_name : 'No Name'));$le++;
            $objWorksheet->getCell($le . $no)->setValue($sb->national_id);$le++;
            $objWorksheet->getCell($le . $no)->setValue($sb->chalk_id);$le++;
            $objWorksheet->getCell($le . $no)->setValue(phone($sb->tel));$le++;
            $objWorksheet->getCell($le . $no)->setValue($path[4]['name']);$le++;
            $objWorksheet->getCell($le . $no)->setValue($path[2]['name']);$le++;
            $objWorksheet->getCell($le . $no)->setValue($sb->person_vht);$le++;
            $objWorksheet->getCell($le . $no)->setValue($nets);$le++;
            $objWorksheet->getCell($le . $no)->setValue($sb->no_picked);$le++;


            $no++;
        }


        $objWriter = PHPExcel_IOFactory::createWriter($objPHPexcel, 'Excel5');

        $path = 'downloads/';
        if (!is_dir($path)) //create the folder if it's not already exists
        {
            mkdir($path, 0777, TRUE);
        }
        $file_name =  'LLIN ' . $this->page_level2 . ' export ' . time() . '.xls';

        $objWriter->save($dir = $path . $file_name);

        force_download($dir, NULL);


    }

    function distribution_report($vill = null)
    {

        //this is the part where the export is initialised

        $objPHPexcel = PHPExcel_IOFactory::load('excel_templates/template.xlsx');
        $objWorksheet = $objPHPexcel->getActiveSheet(0);
        ///Exports depends on the level


        $header = array('Household', 'ID Number', 'Chalk ID', 'Phone', 'Village', 'Subcounty', '#HH Popn', 'Allocated', 'Received', 'Signature');

        $L='A';
        $hno=1;
        foreach ($header  as $h){
            $objWorksheet->getCell($L.$hno)->setValue($h);
            $L++;
        }

        $no = 2;

        $this->db->select('a.*,d.form_num,d.village')->from('reg_detail a')
            ->join('registration d', 'a.data_id=d.id')
            ->join('forms c', 'c.form_num=d.form_num')
            ->where('d.confirm', 1);

        isset($wher) ? $this->db->where($wher) : '';

        $qry = $this->db->order_by('a.last_name', 'asc')->get()->result();

        $h=14;
        foreach ($qry as $sb) {

            $final_pop=$sb->person_final!=0?$sb->person_final:$sb->person_vht;
            $nets=nets_agg_only($final_pop);

            $path = $this->locations->get_path($sb->village);
            //this is the end for the status

            $le='A';
            $objWorksheet->getCell($le . $no)->setValue((strlen($sb->last_name) ? $sb->last_name.' '. $sb->first_name  : 'No Name'));$le++;
            $objWorksheet->getCell($le . $no)->setValue($sb->national_id);$le++;
            $objWorksheet->getCell($le . $no)->setValue($sb->chalk_id);$le++;
            $objWorksheet->getCell($le . $no)->setValue(phone($sb->tel));$le++;
            $objWorksheet->getCell($le . $no)->setValue($path[4]['name']);$le++;
            $objWorksheet->getCell($le . $no)->setValue($path[2]['name']);$le++;
            $objWorksheet->getCell($le . $no)->setValue($sb->person_vht);$le++;
            $objWorksheet->getCell($le . $no)->setValue($nets);$le++;
            $objWorksheet->getCell($le . $no)->setValue('');$le++;
            $objWorksheet->getCell($le . $no)->setValue('');$le++;


            $no++;
        }


        $objWriter = PHPExcel_IOFactory::createWriter($objPHPexcel, 'Excel5');

        $path = 'downloads/';
        if (!is_dir($path)) //create the folder if it's not already exists
        {
            mkdir($path, 0777, TRUE);
        }
        $file_name =  'LLIN ' . $this->page_level2 . ' export ' . time() . '.xls';

        $objWriter->save($dir = $path . $file_name);

        force_download($dir, NULL);


    }

    function list_inventory($vill = null)
    {


        //this is the part where the export is initialised

        $objPHPexcel = PHPExcel_IOFactory::load('excel_templates/template.xlsx');
        $objWorksheet = $objPHPexcel->getActiveSheet(0);
        ///Exports depends on the level


        $header = array('Request ID', 'Date', 'Requester', 'Forms', 'Reqst type', 'Reqst Status', 'Approver', 'Approval Date');

        $L='A';
        $hno=1;
        foreach ($header  as $h){
            $objWorksheet->getCell($L.$hno)->setValue($h);
            $L++;
        }

        $no = 2;


        if (isset($vill)) {

            $wher = $this->all_villages($vill, 'd.village');

        }

        $this->db->select('a.id,a.form_num,a.approved_by,a.approved_on,count(a.request_code) as forms,a.request_code,a.request_type,a.created_on,a.status as request_status,d.first_name,d.last_name')
            ->from('inventory a')
            ->join('users d', 'a.requested_by=d.id', 'left');

        isset($wher) ? $this->db->where($wher) : '';

        $qry = $this->db->order_by('a.id', 'desc')->group_by('request_code')->get()->result();

        foreach ($qry as $sb) {

            //this is the end for the status


            $le='A';
            $objWorksheet->getCell($le . $no)->setValue($sb->request_code);$le++;
            $objWorksheet->getCell($le . $no)->setValue(trending_date($sb->created_on));$le++;
            $objWorksheet->getCell($le . $no)->setValue((strlen($sb->first_name) > 0 ? $sb->first_name . ' ' . $sb->last_name : 'Not registered'));$le++;
            $objWorksheet->getCell($le . $no)->setValue($sb->forms);$le++;
            $objWorksheet->getCell($le . $no)->setValue( humanize($sb->request_type));$le++;
            $objWorksheet->getCell($le . $no)->setValue( $sb->request_status);$le++;
            $objWorksheet->getCell($le . $no)->setValue(($sb->approved_by != 0 ? $this->custom_library->get_user_full_name($sb->approved_by) : ''));$le++;
            $objWorksheet->getCell($le . $no)->setValue( ($sb->approved_on != 0 ? trending_date($sb->approved_on) : ''));$le++;

            $no++;
        }


        $objWriter = PHPExcel_IOFactory::createWriter($objPHPexcel, 'Excel5');

        $path = 'downloads/';
        if (!is_dir($path)) //create the folder if it's not already exists
        {
            mkdir($path, 0777, TRUE);
        }
        $file_name =  'LLIN ' . $this->page_level2 . ' export ' . time() . '.xls';

        $objWriter->save($dir = $path . $file_name);

        force_download($dir, NULL);


    }

    function variance_reports($type = null)
    {





        //this is the beginning of the report

        if (isset($vill)) {

            $wher = $this->all_villages($vill, 'd.village');

        }


        //this is the part where the export is initialised

        $objPHPexcel = PHPExcel_IOFactory::load('excel_templates/template.xlsx');
        $objWorksheet = $objPHPexcel->getActiveSheet(0);
        ///Exports depends on the level


        $header = array('Form#','District', 'Village', 'Variations', 'Resolved', 'Entrant', 'Validator');

        $L='A';
        $hno=1;
        foreach ($header  as $h){
            $objWorksheet->getCell($L.$hno)->setValue($h);
            $L++;
        }

        $no = 2;


        $this->db->select('a.form_num,b.village')->from('forms a')
            ->join('vht b', 'a.phone=b.phone')//->join('registration c','a.form_num=c.form_num')
        ;

        $qry = $this->db->order_by('a.id', 'desc')->get()->result();

        foreach ($qry as $sb) {

            //this is the end for the status


            $path = $this->locations->get_path($sb->village);

            $form_variations = $type == 'entry_variance' ? $this->custom_library->entry_variations($sb->form_num) : $this->custom_library->collection_variations($sb->form_num);


            if($form_variations['variance']!='N/A'||$form_variations['variance']!=0){
                $le='A';
                $objWorksheet->getCell($le . $no)->setValue($sb->form_num)->getStyle($le . $no)->getNumberFormat()->setFormatCode('000000');$le++;
                $objWorksheet->getCell($le . $no)->setValue($path[1]['name']);$le++;
                $objWorksheet->getCell($le . $no)->setValue($path[4]['name']);$le++;
                $objWorksheet->getCell($le . $no)->setValue($form_variations['variance']);$le++;
                $objWorksheet->getCell($le . $no)->setValue($form_variations['resolved']);$le++;
                $objWorksheet->getCell($le . $no)->setValue($form_variations['entrant']);$le++;
                $objWorksheet->getCell($le . $no)->setValue($form_variations['validation']);$le++;

                $no++;
            }
        }


        $objWriter = PHPExcel_IOFactory::createWriter($objPHPexcel, 'Excel5');

        $path = 'downloads/';
        if (!is_dir($path)) //create the folder if it's not already exists
        {
            mkdir($path, 0777, TRUE);
        }
        $file_name =  'LLIN ' . $this->page_level2 . ' export ' . time() . '.xls';

        $objWriter->save($dir = $path . $file_name);

        force_download($dir, NULL);


    }

    function list_forms($vill = null)
    {




        //this is the beginning of the report

        if (isset($vill)) {

            $wher = $this->all_villages($vill, 'd.village');

        }


        //this is the part where the export is initialised

        $objPHPexcel = PHPExcel_IOFactory::load('excel_templates/template.xlsx');
        $objWorksheet = $objPHPexcel->getActiveSheet(0);
        ///Exports depends on the level


        $header = array('Form#', 'VHT', 'Entered', '#HH', '#Pop',  'Village', 'Subcounty','District', 'Registered', 'Status');

        $L='A';
        $hno=1;
        foreach ($header  as $h){
            $objWorksheet->getCell($L.$hno)->setValue($h);
            $L++;
        }

        $no = 2;


        if (isset($vill)) {

            $wher = $this->all_villages($vill, 'c.village');

        }


        $this->db->select('a.id,a.form_num,a.phone,a.date_created,c.first_name,c.last_name,c.village,a.status')->from('forms a')->join('vht c', 'a.phone=c.phone');


        isset($wher) ? $this->db->where($wher) : '';

        $qry = $this->db->order_by('a.id', 'asc')->get()->result();

        foreach ($qry as $sb) {



            $path = $this->locations->get_path($sb->village);
            //this is the end for the status

            $form_entered = $this->custom_library->house_holds($sb->form_num) ? $this->custom_library->house_holds($sb->form_num) : array();


            if (count($form_entered) != 0) {

                $people_in_household = $this->custom_library->people_in_household($form_entered[0]->id) ? $this->custom_library->people_in_household($form_entered[0]->id) : array();


                $house_holds = $this->db->select_sum('person_vht')->from('reg_detail')->where(array('data_id' => $form_entered[0]->id))->get()->row();

            }
            else {
                $people_in_household = array();
            }

            $forms_entered=$this->db->where('form_num',$sb->form_num)->from('registration')->count_all_results();


            $le='A';
            $objWorksheet->getCell($le . $no)->setValue($sb->form_num)->getStyle($le . $no)->getNumberFormat()->setFormatCode('000000');$le++;
            $objWorksheet->getCell($le . $no)->setValue($sb->first_name . ' ' . $sb->last_name);$le++;
            $objWorksheet->getCell($le . $no)->setValue($forms_entered);$le++;
            $objWorksheet->getCell($le . $no)->setValue(count($people_in_household));$le++;
            $objWorksheet->getCell($le . $no)->setValue(count($people_in_household) > 0 ? $house_holds->person_vht : '');$le++;
            $objWorksheet->getCell($le . $no)->setValue($path[4]['name']);$le++;
            $objWorksheet->getCell($le . $no)->setValue($path[2]['name']);$le++;
            $objWorksheet->getCell($le . $no)->setValue($path[1]['name']);$le++;
            $objWorksheet->getCell($le . $no)->setValue(trending_date(strtotime($sb->date_created)));$le++;
            $objWorksheet->getCell($le . $no)->setValue(humanize($sb->status));$le++;

            $no++;
        }


        $objWriter = PHPExcel_IOFactory::createWriter($objPHPexcel, 'Excel5');

        $path = 'downloads/';
        if (!is_dir($path)) //create the folder if it's not already exists
        {
            mkdir($path, 0777, TRUE);
        }
        $file_name =  'LLIN ' . $this->page_level2 . ' export ' . time() . '.xls';

        $objWriter->save($dir = $path . $file_name);

        force_download($dir, NULL);


    }

    function parish_report($vill = null)
    {





        //this is the beginning of the report
        $this->fpdf->AddPage('L');
        if (isset($vill)) {

            $wher = $this->all_villages($vill, 'd.village');

        }


        //this is the part where the export is initialised

        $objPHPexcel = PHPExcel_IOFactory::load('excel_templates/template.xlsx');
        $objWorksheet = $objPHPexcel->getActiveSheet(0);
        ///Exports depends on the level


        $header = array('Sub County', 'Parish', 'Village', 'Households', 'Popn', 'Nets', 'Bales', 'Extra');

        $L='A';
        $hno=1;
        foreach ($header  as $h){
            $objWorksheet->getCell($L.$hno)->setValue($h);
            $L++;
        }

        $no = 2;


        if (isset($vill)) {

            $wher = $this->all_villages($vill, 'a.village');

        }

        $this->db->select('a.village,b.name,b.parish')->from('registration a')->join('villages b', 'a.village=b.village')->where('confirm', 1);

        isset($wher) ? $this->db->where($wher) : '';

        $parish = $this->db->order_by('a.id', 'desc')->group_by('parish')->get()->result();

        foreach ($parish as $q) {


            $np = $this->custom_library->getting_villages_in_parish($q->parish);


            $path = $this->locations->get_path($q->village);


            $le='A';
            $objWorksheet->getCell($le . $no)->setValue($path[2]['name']);$le++;
            $objWorksheet->getCell($le . $no)->setValue($path[3]['name']);$le++;
            $objWorksheet->getCell($le . $no)->setValue($np['village_no']);$le++;
            $objWorksheet->getCell($le . $no)->setValue($np['house_holds']);$le++;
            $objWorksheet->getCell($le . $no)->setValue($np['population']);$le++;
            $objWorksheet->getCell($le . $no)->setValue($np['bails']);$le++;
            $objWorksheet->getCell($le . $no)->setValue($np['extra_bails']);$le++;

            $no++;
        }


        $objWriter = PHPExcel_IOFactory::createWriter($objPHPexcel, 'Excel5');

        $path = 'downloads/';
        if (!is_dir($path)) //create the folder if it's not already exists
        {
            mkdir($path, 0777, TRUE);
        }
        $file_name =  'LLIN ' . $this->page_level2 . ' export ' . time() . '.xls';

        $objWriter->save($dir = $path . $file_name);

        force_download($dir, NULL);


    }

    function sub_county_report($vill = null)
    {




        //this is the beginning of the report

        if (isset($vill)) {

            $wher = $this->all_villages($vill, 'd.village');

        }


        //this is the part where the export is initialised

        $objPHPexcel = PHPExcel_IOFactory::load('excel_templates/template.xlsx');
        $objWorksheet = $objPHPexcel->getActiveSheet(0);
        ///Exports depends on the level


        $header = array('Sub County', 'Parish', 'Village', 'Households', 'Popn', 'Nets', 'Bales', 'Extra');

        $L='A';
        $hno=1;
        foreach ($header  as $h){
            $objWorksheet->getCell($L.$hno)->setValue($h);
            $L++;
        }

        $no = 2;


        if (isset($vill)) {

            $wher = $this->all_villages($vill, 'a.village');

        }

        $this->db->select('a.village,b.name,b.sub_county')->from('registration a')->join('villages b', 'a.village=b.village')->where('confirm', 1);

        isset($wher) ? $this->db->where($wher) : '';

        $sub_county = $this->db->order_by('a.id', 'desc')->group_by('b.sub_county')->get()->result();

        foreach ($sub_county as $q) {

            $np = $this->custom_library->getting_parish_in_sub_county($q->sub_county);


            $le='A';
            $objWorksheet->getCell($le . $no)->setValue($this->locations->get_location_name($q->sub_county));$le++;
            $objWorksheet->getCell($le . $no)->setValue($np['parish_no']);$le++;
            $objWorksheet->getCell($le . $no)->setValue($np['village_no']);$le++;
            $objWorksheet->getCell($le . $no)->setValue($np['house_holds']);$le++;
            $objWorksheet->getCell($le . $no)->setValue($np['population']);$le++;
            $objWorksheet->getCell($le . $no)->setValue($np['house_hold_nets']);$le++;
            $objWorksheet->getCell($le . $no)->setValue($np['bails']);$le++;
            $objWorksheet->getCell($le . $no)->setValue($np['extra_bails']);$le++;

            $no++;

        }


        $objWriter = PHPExcel_IOFactory::createWriter($objPHPexcel, 'Excel5');

        $path = 'downloads/';
        if (!is_dir($path)) //create the folder if it's not already exists
        {
            mkdir($path, 0777, TRUE);
        }
        $file_name =  'LLIN ' . $this->page_level2 . ' export ' . time() . '.xls';

        $objWriter->save($dir = $path . $file_name);

        force_download($dir, NULL);


    }

    function district_report($vill = null)
    {



        //this is the beginning of the report

        if (isset($vill)) {

            $wher = $this->all_villages($vill, 'd.village');

        }


        //this is the part where the export is initialised

        $objPHPexcel = PHPExcel_IOFactory::load('excel_templates/template.xlsx');
        $objWorksheet = $objPHPexcel->getActiveSheet(0);
        ///Exports depends on the level


        $header = array( 'District', 'Sub County', 'Parish', 'Village', 'Households', 'Popn', 'Nets', 'Bales', 'Extra');

        $L='A';
        $hno=1;
        foreach ($header  as $h){
            $objWorksheet->getCell($L.$hno)->setValue($h);
            $L++;
        }

        $no = 2;


        if (isset($vill)) {

            $wher = $this->all_villages($vill, 'a.village');

        }

        $this->db->select('a.village,b.name,a.district')->from('registration a')->join('villages b', 'a.village=b.village')->where('confirm', 1);

        isset($wher) ? $this->db->where($wher) : '';

        $district = $this->db->order_by('a.id', 'desc')->group_by('a.district')->get()->result();

        foreach ($district as $q) {

            $np = $this->custom_library->getting_sub_county_in_district($q->district);


            $le='A';
            $objWorksheet->getCell($le . $no)->setValue($this->locations->get_location_name($q->district));$le++;
            $objWorksheet->getCell($le . $no)->setValue($np['sub_county_no']);$le++;
            $objWorksheet->getCell($le . $no)->setValue($np['parish_no']);$le++;
            $objWorksheet->getCell($le . $no)->setValue($np['village_no']);$le++;
            $objWorksheet->getCell($le . $no)->setValue($np['house_holds']);$le++;
            $objWorksheet->getCell($le . $no)->setValue($np['population']);$le++;
            $objWorksheet->getCell($le . $no)->setValue($np['house_hold_nets']);$le++;
            $objWorksheet->getCell($le . $no)->setValue($np['bails']);$le++;
            $objWorksheet->getCell($le . $no)->setValue($np['extra_bails']);$le++;


            $no++;
        }


        $objWriter = PHPExcel_IOFactory::createWriter($objPHPexcel, 'Excel5');

        $path = 'downloads/';
        if (!is_dir($path)) //create the folder if it's not already exists
        {
            mkdir($path, 0777, TRUE);
        }
        $file_name =  'LLIN ' . $this->page_level2 . ' export ' . time() . '.xls';

        $objWriter->save($dir = $path . $file_name);

        force_download($dir, NULL);


    }




}

?>


