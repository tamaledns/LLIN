<?php

class model extends CI_Model
{

    function __construct()
    {
        parent::__construct();

    }


    function total_house_holds()
    {
        return $this->db->where(array('b.confirm' => 1))
            ->from('reg_detail a')
            ->join('registration b', 'a.data_id=b.id')
            ->count_all_results();
    }


    function total_house_hold_allocation()
    {

        $f = $this->total_villages();

        return $f['nets'];
    }

    function total_villages($parish = null)
    {

        $this->db
            ->select('distinct(a.village) as village,b.name')
            ->from('registration a')
            ->join('locations_view b', 'a.village=b.id')
            ->where('confirm', 1);
        isset($parish) ? $this->db->where(array('b.parish' => $parish)) : '';
        $villages = $this->db->group_by('a.village')->get()->result();

        //aggregating of the results to nets


        $nets = 0;
        foreach ($villages as $q) {

            $for_det = $this->db->select('b.person_vht,b.person_final')
                ->from('registration a')
                ->join('reg_detail b', 'b.data_id=a.id')
                ->where(array('a.village' => $q->village, 'confirm' => 1))->get()->result();

            $np = nets_popn_agg($for_det);

            $nets = $nets + $np['house_hold_nets'];


        }

//this is the part of the out put
        return $output = array(
            'count' => count($villages),
            'nets' => $nets
        );

    }

    function total_head_count_allocation()
    {

        $f = $this->total_villages();

        return $f['nets'];
    }

    function total_head_count()
    {
        return $this->db->select('sum(a.person_vht) as person_vht')
            ->from('reg_detail a')
            ->join('registration b', 'a.data_id=b.id')
            ->where(array('b.confirm' => 1))
            ->get()->row();


    }

    function total_parishes()
    {

        $this->db->select('a.village,b.name,b.parish')
            ->from('registration a')
            ->join('villages b', 'a.village=b.village')
            ->where('confirm', 1);

        $parish = $this->db->group_by('parish')->get()->result();

//aggregating of the results to nets


        $nets = 0;
        foreach ($parish as $q) {


            $np = $this->custom_library->getting_villages_in_parish($q->parish);


            $nets = $nets + $np['house_hold_nets'];
//                    $np['bails'],
//                    $np['extra_bails'],

        }

//this is the part of the out put

        return $output = array(
            'count' => count($parish),
            'nets' => $nets
        );

    }

    function total_subcounties()
    {

        $this->db->select('a.village,b.name,b.sub_county')
            ->from('registration a')
            ->join('villages b', 'a.village=b.village')
            ->where('confirm', 1);
        $sub_county = $this->db->group_by('b.sub_county')->get()->result();


//aggregating of the results to nets
        $net = 0;
        foreach ($sub_county as $q) {
            $np = $this->custom_library->getting_parish_in_sub_county($q->sub_county);

            $net = $net + $np['house_hold_nets'];
        }
//this is the part of the out put

        return $output = array(
            'count' => count($sub_county),
            'nets' => $net
        );
    }


    function total_districts()
    {


        $this->db->select('a.village,b.name,b.sub_county')
            ->from('registration a')
            ->join('villages b', 'a.village=b.village')
            ->where('confirm', 1);
        $country = $this->db->group_by('b.district')->get()->result();

//aggregating of the results to nets


//this is the part of the out put

        return $output = array(
            'count' => count($country),
            'nets' => 0
        );

    }

    function total_district_baseline($id)
    {

        $this->db->select('a.village,b.name,b.sub_county')
            ->from('registration a')
            ->join('villages b', 'a.village=b.village')
            ->where('confirm', 1);
        $forms = $this->db->order_by('a.id', 'desc')->group_by('b.sub_county')->where('a.district', $id)->get()->result();
        $baseline_total = 0;
        foreach ($forms as $f) {
            $baseline_total  += $this->baseline_subcounty($f->sub_county);
        }
        return $baseline_total;
    }

    function baseline_subcounty($id)
    {
        $base = $this->db->select()->from('baseline_subcounty')
            ->where('location_id', $id)
            ->get()->row();
        return count($base) == 0 ? 0 : $base->total;
    }

    function build_Geo_json($geo_type=null)
    {

//error_reporting(0);
        $qry = $this->db->select('*, ST_AsWKT(SHAPE) as wkb')->from('spartial')->limit(200)->get()->result();
//        print_r($qry);
# Build GeoJSON feature collection array
        $geojson = array(
            'type' => 'FeatureCollection',
            'crs' => array(
                'type' => 'name',
                'properties' => array('name' => 'urn:ogc:def:crs:OGC:1.3:CRS84')
            ),
            'features' => array()
        );

# Loop through rows to build feature arrays
        foreach ($qry as $properties) {


            # Remove wkb and geometry fields from properties


            $county=$properties->adm2_name;

            //getting totals for the county
            $geo_type?($np= $this->subcounties_in_county($county)):'';

//            Census Population, #HH Population, #Households, %Registered
            $feature = array(
                'type' => 'Feature',
                'properties' => array(
                    'ADM0_NAME' => humanize($properties->adm0_name),
                    "ADM1_NAME" => humanize($properties->adm1_name),
                    "ADM2_NAME" => humanize($county),
                    "Adm_Region" => humanize($properties->adm_region),
                    "Shape_Leng" => $properties->shape_leng,
                    "Shape_Area" => $properties->shape_area,
                    "base_line_total" => $baseline=$this->total_baseline($county),
                    "hh_popn" => $popn=$geo_type?$np['population']:0,
                    "households" => $geo_type?$np['house_holds']:0,
                    "percent_reg" =>$geo_type?(($baseline>0?number_format((($popn/$baseline)*100),2):0).'%'):0,
                    "distributed" =>$geo_type&&$geo_type=='distribution_map'?$np['picked']:0
                ),
//                "type": "Polygon", "coordinates":
                'geometry' => array(
                    'type' => 'Polygon',
                    'coordinates' => array($this->wkb_to_json($properties->wkb))
                )

            );
            # Add feature arrays to feature collection array
            array_push($geojson['features'], $feature);
        }

//        header('Content-type: application/json');
        $f = json_encode($geojson, JSON_NUMERIC_CHECK);

        return $f;
    }

    function subcounties_in_county($county)
    {
//        SELECT * FROM deron59_llin2.baseline_subcounty A LEFT JOIN deron59_llin2.spartial B ON a.county=b.adm2_name group by (a.county);

        $count = $this->db->select('location_id')->from('baseline_subcounty')->where(array('subcounty' => strtolower($county)))->get()->result();



        $sub_county_no=count($count);
        $parish_no=0;
        $village_no=0;
        $house_hold = 0;
        $population = 0;
        $house_hold_nets = 0;
        $bails = 0;
        $extra_bails = 0;
        $picked=0;


        foreach ($count as $q) {


            $np = $this->custom_library->getting_parish_in_sub_county($q->location_id);


            $parish_no += $np['parish_no'];
            $village_no += $np['village_no'];
            $house_hold += $np['house_holds'];
            $population += $np['population'];
            $house_hold_nets += $np['house_hold_nets'];
            $bails += $np['bails'];
            $extra_bails += $np['extra_bails'];
            $picked += $np['picked'];



        }

        return array(
            'sub_county_no' =>  $sub_county_no,
            'parish_no' =>  $parish_no,
            'village_no' =>  $village_no,
            'house_holds' => $house_hold,
            'population' => $population,
            'house_hold_nets' => $house_hold_nets,
            'bails' => $bails,
            'extra_bails' => $extra_bails,
            'picked'=>$picked

        );






    }

    function total_baseline($county,$geo_type=null){
       $to= $this->db->select('sum(total) as total')->from('baseline_subcounty')->like('subcounty',strtolower($county))->get()->row();

        return count($to)>0? $to->total:'0';
    }

    function wkb_to_json($wkb)
    {



        $geom = '';

        $rest = substr($wkb, 0, -2);
        $rest = substr($rest, 9);

        $rest=str_replace('(','',$rest);

        $rest=str_replace(')','',$rest);





        $exploded = explode(',', $rest);


        $e = array();
        foreach ($exploded as $ex) {
            $coordinates = explode(' ', $ex);

            array_push($e, $coordinates);
        }

        //$geom = $rest;
        return $e;

    }

    function build_Geo_json_subcounty($geo_type=null)
    {

//error_reporting(0);
        $qry = $this->db->select('*, ST_AsWKT(SHAPE) as wkb')->from('spatial')
//            ->limit(20)
            ->get()->result();
//        print_r($qry);
# Build GeoJSON feature collection array
        $geojson = array(
            'type' => 'FeatureCollection',
            'crs' => array(
                'type' => 'name',
                'properties' => array('name' => 'urn:ogc:def:crs:OGC:1.3:CRS84')
            ),
            'features' => array()
        );

# Loop through rows to build feature arrays
        foreach ($qry as $properties) {


            # Remove wkb and geometry fields from properties


            $scounty=$properties->sname_2013;

            //getting totals for the county
            $geo_type?($np=$this->get_subcounty($scounty,$properties->dname_2012)):'';
//            print_r($np);

//            print_r($geo_type);


//            Census Population, #HH Population, #Households, %Registered
            $feature = array(
                'type' => 'Feature',
                'properties' => array(
                    'dname_2012' => ($properties->dname_2012),
                    "cname_2013" => ($properties->cname_2013),
                    "sname_2013" => ($scounty),
//                    "Adm_Region" => humanize($properties->adm_region),
//                    "Shape_Leng" => $properties->shape_leng,
//                    "Shape_Area" => $properties->shape_area,
                    "base_line_total" => $baseline=$this->total_baseline($scounty),
                    "hh_popn" => $popn=$geo_type?$np['population']:0,
                    "households" => $geo_type?$np['house_holds']:0,
                    "percent_reg" =>$geo_type?(($baseline>0?number_format((($popn/$baseline)*100),2):0).'%'):0,
                    "distributed" =>$geo_type&&$geo_type=='distribution_map'?$picked=$np['picked']:0,
                    "percent_dist" =>$geo_type&&$geo_type=='distribution_map'?(($popn>0?number_format((($picked/$popn)*100),2):0).'%'):0
                ),
//                "type": "Polygon", "coordinates":
                'geometry' => array(
                    'type' => 'Polygon',
                    'coordinates' => array($this->wkb_to_json($properties->wkb))
                )

            );
            # Add feature arrays to feature collection array
            array_push($geojson['features'], $feature);
        }

//        header('Content-type: application/json');
        $f = json_encode($geojson, JSON_NUMERIC_CHECK);

        return $f;
    }

    function get_subcounty($county,$district)
    {
//        SELECT * FROM deron59_llin2.baseline_subcounty A LEFT JOIN deron59_llin2.spartial B ON a.county=b.adm2_name group by (a.county);

        $loc = $this->db->select('location_id')
            ->from('baseline_subcounty')
            ->where(array('district'=>$district))
            ->group_start()
            ->like(array('subcounty' => $county))->or_like(array('sam_subcounty' => $county))
            ->group_end()->get()->row();



        $parish_no=0;
        $village_no=0;
        $house_hold = 0;
        $population = 0;
        $house_hold_nets = 0;
        $bails = 0;
        $extra_bails = 0;
        $picked=0;


        if (count($loc)==1) {


            $np = $this->custom_library->getting_parish_in_sub_county($loc->location_id);


            $parish_no += $np['parish_no'];
            $village_no += $np['village_no'];
            $house_hold += $np['house_holds'];
            $population += $np['population'];
            $house_hold_nets += $np['house_hold_nets'];
            $bails += $np['bails'];
            $extra_bails += $np['extra_bails'];
            $picked += $np['picked'];



        }

        return array(

            'parish_no' =>  $parish_no,
            'village_no' =>  $village_no,
            'house_holds' => $house_hold,
            'population' => $population,
            'house_hold_nets' => $house_hold_nets,
            'bails' => $bails,
            'extra_bails' => $extra_bails,
            'picked'=>$picked

        );






    }





}