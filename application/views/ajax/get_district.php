<script src="<?php echo base_url() ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>


<script>
    $("#country").change(function(){
        var pc= $("#country").val();

        if(pc !='') {

            $.ajax({
                type: 'GET',
                url: '<?php echo base_url("index.php/ajax_api/get_city")?>/' + pc,
                beforeSend:function(){
                    $("#district").html('<option value=""><i class="fa fa-spin fa-spinner"><i> Please Wait...</option>');
                },
                success: function (d) {

                    $("#district").html(d);


                }


            });

            $.ajax({
                type: 'GET',
                url: '<?php echo base_url("index.php/ajax_api/dialing_code")?>/' + pc,
                beforeSend:function(){
                    $("#phone").val('Please Wait...');
                },
                success: function (d) {

                    $("#phone").val(d);


                }


            });


        }




    });





    $("#country_parent").change(function(){
        var pc= $("#country_parent").val();

        if(pc !='') {

            $.ajax({
                type: 'GET',
                url: '<?php echo base_url("index.php/ajax_api/get_city")?>/' + pc,
                beforeSend:function(){
                    $("#district_parent").html('<option value=""><i class="fa fa-spin fa-spinner"><i> Please Wait...</option>');
                },
                success: function (d) {

                    $("#district_parent").html(d);


                }


            });

            $.ajax({
                type: 'GET',
                url: '<?php echo base_url("index.php/ajax_api/dialing_code")?>/' + pc,
                beforeSend:function(){
                    $("#dialing_code").val('loading...');
                    $("#dialing_code2").val('loading...');
                },
                success: function (d) {

                    $("#dialing_code").val(d);
                    $("#dialing_code2").val(d);


                }


            });


        }

    });

    $("#country_parent").ready(function(){
        var pc= $("#country_parent").val();

        if(pc !='') {

            $.ajax({
                type: 'GET',
                url: '<?php echo base_url("index.php/ajax_api/get_city")?>/' + pc,
//                beforeSend:function(){
//                    $("#district_parent").html('<option value=""><i class="fa fa-spin fa-spinner"><i> Please Wait...</option>');
//                },
                success: function (d) {

                    $("#district_parent").append(d);


                }


            });


        }

    });




    $("#country_child").change(function(){
        var pc= $("#country_child").val();

        if(pc !='') {

            $.ajax({
                type: 'GET',
                url: '<?php echo base_url("index.php/ajax_api/get_school_city")?>/' + pc,
                beforeSend:function(){
                    $("#school_city").html('<option value=""><i class="fa fa-spin fa-spinner"><i> Please Wait...</option>');
                },
                success: function (d) {

                    $("#school_city").html(d);


                }


            });


            $("#class").html('<option value="">Select Class...</option>')
            if(pc=='UG'){

                <?php for($i=1;$i<=7;$i++){ ?>

                $("#class").append('<option value="Primary <?php echo $i ?>" <?php echo set_select("school_name", 'Primary'); ?>>Primary <?php echo $i ?></option>');

                <?php } ?>

            }else if(pc=='KE'){

                <?php for($i=1;$i<=8;$i++){ ?>

                $("#class").append('<option value="Standard <?php echo $i ?>" <?php echo set_select("school_name", 'Standard'); ?>>Standard <?php echo $i ?></option>');

                <?php } ?>
            }

        }

    });




    $("#country_child").ready(function(){
        var pc= $("#country_child").val();

        if(pc !='') {

            $.ajax({
                type: 'GET',
                url: '<?php echo base_url("index.php/ajax_api/get_school_city")?>/' + pc,
                success: function (d) {

                    $("#school_city").append(d);


                }


            });

            if(pc=='UG'){

                <?php for($i=1;$i<=7;$i++){ ?>

                $("#class").append('<option value="Primary <?php echo $i ?>" <?php echo set_select("school_name", 'Primary'.$i ); ?>>Primary <?php echo $i ?></option>');

                <?php } ?>

            }else if(pc=='KE'){

                <?php for($i=1;$i<=8;$i++){ ?>

                $("#class").append('<option value="Standard <?php echo $i ?>" <?php echo set_select("school_name", 'Standard' .$i); ?>>Standard <?php echo $i ?></option>');

                <?php } ?>
            }




        }

    });








    $("#school_city").change(function(){
        var pc= $("#school_city").val();

        if(pc !='') {

            $.ajax({
                type: 'GET',
                url: '<?php echo base_url("index.php/ajax_api/school")?>/' + pc,
                beforeSend:function(){
                    $("#school_name").html('<option value=""><i class="fa fa-spin fa-spinner"><i> Please Wait...</option>');
                },
                success: function (d) {

                    $("#school_name").html(d);


                }


            });




        }




    });

    $("#school_city").ready(function(){
        var pc= $("#school_city").val();

        if(pc !='') {

            $.ajax({
                type: 'GET',
                url: '<?php echo base_url("index.php/ajax_api/school")?>/' + pc,

                success: function (d) {

                    $("#school_name").append(d);


                }


            });




        }




    });



    //this get packages in different products
    $(".icheck_line-icon").click(function () {
        alert('');
        var dp = $("input[name=products]:checked").val();

        if (dp != '') {


            $.ajax({
                type: 'GET',
                url: '<?php echo base_url("index.php/ajax_api/get_city")?>/' + pc,
                beforeSend:function(){
                    $("#products_group").html('<option value=""><i class="fa fa-spin fa-spinner"><i> Please Wait...</option>');
                },
                success: function (d) {

                    $("#products_group").html(d);


                }


            });

        }
    });

    $("#parent_phone").keyup(function(){
        var pp=$("#parent_phone").val()

        if(pp!='' && pp.length >= 10 )
        {



            $.ajax({
                type: 'GET',
                url: '<?php echo base_url("index.php/ajax_api/parent_data")?>/' + pp,
                beforeSend:function(){
                    $("#parent_phone_btn").html('<i class="fa fa-spin fa-spinner"><i>');

                },
                success: function (d) {

                    $("#parent_phone_btn").html(d);


                }


            });
           // alert(pp);

        }
    });


</script>