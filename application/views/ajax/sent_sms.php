<script src="<?php echo base_url() ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>

<script>
    var TableDatatablesAjax = function () {

        var initPickers = function () {
            //init date pickers
            $('.date-picker').datepicker({
                rtl: App.isRTL(),
                autoclose: true
            });
        }

        var handleRecords = function () {

            var grid = new Datatable();

            grid.init({
                src: $("#<?php echo $title ?>"),
                onSuccess: function (grid, response) {
                    // grid:        grid object
                    // response:    json object of server side ajax response
                    // execute some code after table records loaded
                },
                onError: function (grid) {
                    // execute some code on network or other general error
                },
                onDataLoad: function(grid) {
                    // execute some code on ajax data load
                },
                loadingMessage: 'Loading...',
                dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options

                    // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                    // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
                    // So when dropdowns used the scrollable div should be removed.
                    "dom": "<'row'<'col-md-2 col-sm-12 pull-right'f><'col-md-10 col-sm-12'<'table-group-actions pull-left'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",

                    //"dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

                    "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

                    "lengthMenu": [
                        [10, 20, 50, 100, 150, -1],
                        [10, 20, 50, 100, 150, "All"] // change per page values here
                    ],
                    "pageLength": 10, // default record count per page
                    "ajax": {
                        "url": "<?php echo base_url('index.php/ajax_api/'.$title.'/'.$this->uri->segment(3)) ?>", // ajax source
                    },
                    "order": [
                        [1, "asc"]
                    ]// set first column as a default sort by asc
                }
            });

            // handle group actionsubmit button click
            grid.getTableWrapper().on('click', '.table-group-action-submit', function (e) {
                e.preventDefault();
//                var search = $(".table-group-search-input", grid.getTableWrapper());
                var from = $(".table-group-from-input", grid.getTableWrapper());
                var to = $(".table-group-to-input", grid.getTableWrapper());
                var sector = $(".table-group-sector-input", grid.getTableWrapper());
                var publication = $(".table-group-publication-input", grid.getTableWrapper());
                if (sector.val() != "" ||publication.val() != ""||from.val() != ""||to.val() != "") {
                    grid.setAjaxParam("customActionType", "group_search");
//                    grid.setAjaxParam("customSearchName", search.val());
                    grid.setAjaxParam("customFromName", from.val());
                    grid.setAjaxParam("customToName", to.val());
                    grid.setAjaxParam("customSectorName", sector.val());
                    grid.setAjaxParam("customPublicationName", publication.val());
                    grid.setAjaxParam("id", grid.getSelectedRows());
                    grid.getDataTable().ajax.reload();
                    grid.clearAjaxParams();
                }
            });
        }

        return {

            //main function to initiate the module
            init: function () {

                initPickers();
                handleRecords();
            }

        };

    }();

    jQuery(document).ready(function() {
        TableDatatablesAjax.init();
    });
</script>