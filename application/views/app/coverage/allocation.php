
<?php $baseline= $this->db->select()->from('baseline')->where(array('active'=>1,'baseline_year'=>2015))->get()->row(); ?>


<style>
    .table-bordered, .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
        text-align: right;
    }
</style>



<table class="table table-striped table-bordered table-advance table-hover" id="sample_1">

    <thead>
    <tr>
<!--        Registered	Nets Allocated	Nets Allocated %	Not Allocated
-->

        <th style="text-align: left;">  <i class="fa fa-globe"></i>  Location </th>
        <th>Registered</th>
        <th>Nets Allocated</th>
        <th>Nets Allocated%</th>
        <th>Not Allocated</th>
    </tr>
    </thead>
    <tbody>

    <tr>


        <td style="text-align: left;"  class="highlight">
            <div class="success"></div>
            <a href="javascript:;" style="text-decoration: none;">Population </a>
        </td>

        <td><?php $hc=$this->model->total_head_count(); echo number_format($hcc=$hc->person_vht); ?></td>
        <td><?php  echo number_format($this->model->total_house_hold_allocation()); ?></td>
        <td>100%</td>
        <td>0</td>


    </tr>
    <tr>


        <td style="text-align: left;"  class="highlight">
            <div class="success"></div>
            <a href="javascript:;" style="text-decoration: none;">Household </a>
        </td>

        <td><?php  $h=$this->model->total_house_holds(); echo number_format($h); ?></td>
        <td><?php  echo number_format($this->model->total_house_hold_allocation()); ?></td>
        <td>100%</td>
        <td>0</td>

    </tr>
    <tr>


        <td style="text-align: left;"  class="highlight">
            <div class="success"></div>
            <a href="javascript:;" style="text-decoration: none;">Village </a>
        </td>


        <td><?php $vi=$this->model->total_villages(); echo number_format($vi['count']); ?></td>
        <td><?php echo number_format($vi['nets']); ?></td>
        <td>100%</td>
        <td>0</td>


    </tr>
    <tr>

        <td style="text-align: left;"  class="highlight">
            <div class="success"></div>
            <a href="javascript:;" style="text-decoration: none;">Parish </a>
        </td>


        <td><?php $tp=$this->model->total_parishes(); echo number_format($tp['count']); ?></td>
        <td><?php  echo number_format($tp['nets']); ?></td>
        <td>100%</td>
        <td>0</td>


    </tr>
    <tr>
        <td style="text-align: left;"  class="highlight">
            <div class="success"></div>
            <a href="javascript:;" style="text-decoration: none;"> Sub-county </a>
        </td>
        <td><?php $tsc=$this->model->total_subcounties(); echo number_format($tsc['count']); ?></td>
        <td><?php echo number_format($tsc['nets']); ?></td>
        <td>100%</td>
        <td>0</td>


    </tr>
    <tr>

        <td style="text-align: left;"  class="highlight">
            <div class="success"></div>
            <a href="javascript:;" style="text-decoration: none;"> District </a>
        </td>
        <td><?php $td=$this->model->total_districts(); echo number_format($td=$td['count']); ?></td>
        <td><?php  echo number_format($tsc['nets']); ?></td>
        <td>100%</td>
        <td>0</td>


    </tr>

    </tbody>
</table>
