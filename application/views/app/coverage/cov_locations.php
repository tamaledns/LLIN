



<span class="print<?php echo $id ?>">
        <table class="table table-striped table-bordered table-advance table-hover">
            <thead>
            <tr>

                <th>District</th>
                <th> <i class="fa fa-caret-square-o-right"></i> Subcounty </th>
                <th> Parish </th>
                <th> #Village </th>
                <th> #Census </th>
                <th> #HH Popn </th>
                <th> %HH Popn </th>
                <th> #HouseHolds </th>
                <th> #Allcation </th>
                <th>#Distribution</th>
                <th>%Distribution</th>
                <th>#Not Distributed</th>
                <!--                <th> #Bailes </th>-->
                <!--                <th> #Extra </th>-->
            </tr>
            </thead>
            <tbody class="tbody">


            <?php $dp = $this->custom_library->getting_sub_county_in_district($id); ?>
            <tr class="warning">


                <td style="text-align: left;" class="highlight bold">
                    <div class="success"></div>
                    <a href="javascript:;"
                       style="text-decoration: none;"><?php echo $district_label = $this->locations->get_location_name($id) ?> </a>
                </td>

                <td class="bold">
                    <?php echo $dp['sub_county_no'] ?>
                </td>

                <td class="bold"> <?php echo $dp['parish_no'] ?></td>


                <td class="bold">

                    <?php echo $dp['village_no'] ?>
                </td>


                <td class="bold">

                    <?php echo $baseline_district= $this->model->total_district_baseline($id) ?>
                </td>


                <td class="bold">
                    <?php echo $hh_pop_district=$dp['population'] ?>
                </td>

                 <td class="bold">

                    <?php echo  $baseline_district==0?0:number_format((($hh_pop_district/$baseline_district)*100),2) ?>
                </td>

                <td class="bold">

                    <?php echo $dp['house_holds'] ?>

                </td>
                <td class="bold">
                    <?php
                    echo $nets_dp = $dp['house_hold_nets'];
                    ?>

                </td>
                <td class="bold"><?php echo $dist_dp = $dp['picked'] ?></td>

                <td class="bold"><?php echo $nets_dp == 0 ? 0.00 : number_format((($dist_dp / $nets_dp) * 100), 2); ?></td>
                <td class="bold"><?php echo $not_dist_dp = ($nets_dp - $dist_dp) ?></td>

            </tr>

            <?php


            // $forms = $this->db->select()->from('registration')->where(array('district'=>$id))->group_by('village')->get()->result();

            $this->db->select('a.village,b.name,b.sub_county')
                ->from('registration a')
                ->join('villages b', 'a.village=b.village')
                ->where('confirm', 1);
            $forms = $this->db->order_by('a.id', 'desc')->group_by('b.sub_county')->where('a.district', $id)->get()->result();


            $n2 = 1;
            foreach ($forms as $f) {

                $np = $this->custom_library->getting_parish_in_sub_county($f->sub_county);
                $subcounty_name = $this->locations->get_location_name($f->sub_county);


                $this->db->select('a.village,b.name,b.parish')
                    ->from('registration a')
                    ->join('villages b', 'a.village=b.village')
                    ->where('confirm', 1);
                $parish = $this->db->order_by('a.id', 'desc')->group_by('b.parish')->where('b.sub_county', $f->sub_county)->get()->result();
                ///Getting the baseline of the subcounty


                ?>
                <tr>


                    <td style="background-color: #fff;"></td>
                    <td class="danger">
                        <?php echo $subcounty_name ?>
                    </td>

                    <td class="danger"><?php echo count($parish) ?></td>


                    <td class="danger">

                        <?php echo $np['village_no'] ?>
                    </td>


                <td class="danger">

                    <?php echo $baseline_subcounty=$this->model->baseline_subcounty($f->sub_county); ?>
                </td>



                    <td class="danger">
                        <?php echo $hh_pop_subcounty= $np['population'] ?>
                    </td>
                     <td class="danger">

                    <?php echo $baseline_subcounty==0?0: number_format((($hh_pop_subcounty/$baseline_subcounty)*100),2) ?>
                </td>


                    <td class="danger">

                        <?php echo $np['house_holds'] ?>

                    </td>
                    <td class="danger">
                        <?php
                        echo $nets = $np['house_hold_nets'];
                        ?>

                    </td>
                    <td class="danger"><?php echo $dist = $np['picked'] ?></td>

                    <td class="danger"><?php echo number_format((($dist / $nets) * 100), 2) ?></td>
                    <td class="danger"><?php echo $not_dist = ($nets - $dist) ?></td>

                </tr>

                <?php

                foreach ($parish as $f) {

                    $parish_name = $this->locations->get_location_name($f->parish);


                    $this->db->select('a.village,b.name,b.parish')
                        ->from('registration a')
                        ->join('villages b', 'a.village=b.village')
                        ->where('confirm', 1);
                    $villages = $this->db->order_by('a.id', 'desc')->group_by('b.village')->where('b.parish', $f->parish)->get()->result();

                    $np = $this->custom_library->getting_villages_in_parish($f->parish);

                    ?>


                    <tr>

                        <td colspan="2" style="background-color: #fff;"> </td>
                        <td class="info">
                            <?php echo $parish_name; ?>
                        </td>





                        <td class="info">
                            <?php echo $np['village_no']; ?>
                            </td>
                         <td class="info">

                    <?php //echo $baseline_subcounty ?>
                </td>
                        <td class="info">

                            <?php echo $np['population']; ?>
                        </td>

                         <td class="info">

                    <?php //echo $percent_popn ?>
                </td>


                        <td class="info">

                            <?php echo $np['house_holds']; ?>
                        </td>

                        <td class="info">
                            <?php echo $nets = $np['house_hold_nets']; ?>
                        </td>
                        <td class="info"><?php echo $dist = $np['picked'] ?></td>

                        <td class="info"><?php echo number_format((($dist / $nets) * 100), 2) ?></td>
                        <td class="info"><?php echo $not_dist = ($nets - $dist) ?></td>


                    </tr>

                    <?php

                    foreach ($villages as $f) {

                        $village_name = $this->locations->get_location_name($f->village);


                        $for_det = $this->db->select('b.person_vht,b.person_final,no_picked')
                            ->from('registration a')
                            ->join('reg_detail b', 'b.data_id=a.id')
                            ->where(array('a.village' => $f->village, 'confirm' => 1))->get()->result();


                        $np = nets_popn_agg($for_det);

                        ?>


                        <tr>


                            <td colspan="3" style="background-color: #fff;"> </td>
                            <td>
                                <?php echo $village_name ?>
                            </td>
                             <td>

                    <?php //echo $baseline_subcounty ?>
                </td>




                            <td>

                                <?php echo $np['population']; ?>
                            </td>

                             <td>

                    <?php //echo $percent_popn ?>
                </td>


                            <td>

                                <?php echo count($for_det); ?>

                            </td>
                            <td>
                                <?php echo $nets = $np['house_hold_nets'] ?>
                            </td>
                            <td><?php echo $dist = $np['picked'] ?></td>

                            <td><?php echo number_format((($dist / $nets) * 100), 2) ?></td>
                            <td><?php echo $not_dist = ($nets - $dist) ?></td>

                        </tr>


                    <?php }

                    ?>


                <?php }

                ?>


                <?php $n2++;
            } ?>

            </tbody>
        </table>
    </span>