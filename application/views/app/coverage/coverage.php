<div class="row">

    <div class="col-md-12">
        <!-- BEGIN Portlet PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-globe"></i><?php echo humanize($subtitle .' ' .$title) ?></div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"> </a>
<!--                    <a href="#portlet-config" data-toggle="modal" class="config"> </a>-->
                    <a href="javascript:;" data-load="true" data-url="<?php echo base_url('ajax_api/'.$this->page_level2.$subtitle) ?>" class="reload"> </a>
                    <a href="javascript:;" class="fullscreen"> </a>
<!--                    <a href="javascript:;" class="remove"> </a>-->
                </div>
            </div>
            <div class="portlet-body portlet-empty"> </div>
        </div>
        <!-- END Portlet PORTLET-->
    </div>
<!--    <div class="col-md-6">-->
<!--        <!-- BEGIN Portlet PORTLET-->
<!--        <div class="portlet box red">-->
<!--            <div class="portlet-title">-->
<!--                <div class="caption">-->
<!--                    <i class="fa fa-gift"></i>Advance Form </div>-->
<!--                <div class="tools">-->
<!--                    <a href="javascript:;" class="collapse"> </a>-->
<!--                    <a href="#portlet-config" data-toggle="modal" class="config"> </a>-->
<!--                    <a href="javascript:;" class="reload" data-load="true" data-url="portlet_ajax_content_2.html"> </a>-->
<!--                    <a href="javascript:;" class="fullscreen"> </a>-->
<!--                    <a href="javascript:;" class="remove"> </a>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="portlet-body form portlet-empty"> </div>-->
<!--        </div>-->
<!--        <!-- END Portlet PORTLET-->
<!--    </div>-->
</div>