<!--this the json data-->


<link rel="stylesheet" href="<?php echo base_url() ?>map/lib/leaflet/leaflet.css"/>
<link rel="stylesheet" href="<?php echo base_url() ?>map/lib/leaflet/leaflet.label.css"/>
<script src="<?php echo base_url() ?>map/lib/leaflet/leaflet.js"></script>
<script src="<?php echo base_url() ?>map/lib/leaflet/leaflet.label.js"></script>
<!--<script src="--><?php //echo base_url() ?><!--map/data/states.geojson"></script>-->
<!--    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>-->

<style>
    #map {

        height: 100%;
    }
</style>

<style>#map { width: 100%; height: 650px; }
    .info {
        padding: 6px 8px;
        font: 14px/16px Arial, Helvetica, sans-serif;
        /*background: white;*/
        background: rgba(255,255,255,0.8);
        box-shadow: 0 0 15px rgba(0,0,0,0.2);
        border-radius: 5px;
    }
    .info-title{
        border-bottom: solid thin #FF8200;
        color: #FF8200;
        border-color: #FF8200;
        font-size: 14px !important;
        font-family: MontserratBold, Arial, Helvetica, sans-serif;
        font-weight: bold;
    }
    .info-count {
        font-size: 2em;
        padding-right: 10px;
        padding-bottom: 15px;
        line-height: 100%;
        font-family: MontserratBold, Arial, Helvetica, sans-serif;
        font-weight: bold;
    }


    .info h4 {
        font-weight: bold;
        margin: 0 0 5px; color: #777; }

    .legend { text-align: left; line-height: 18px; color: #555; } .legend i { width: 18px; height: 18px; float: left; margin-right: 8px; opacity: 0.7; }</style>

<div id='map'></div>

<!--<script type="text/javascript" src="us-states.js"></script>-->

<!--<script src="data/states.geojson"></script>-->

<script>
    var states = <?php echo $geojson; ?>
</script>



<script type="text/javascript">

    var map = L.map('map').setView([0.9, 32.1], 7);


    // control that shows state info on hover
    var info = L.control({position: 'topleft'});

    info.onAdd = function (map) {

        this._div = L.DomUtil.create('div', 'info');
        this.update();
        return this._div;
    };


    info.update = function (props) {

        this._div.innerHTML = '' +

            (props ?

            '<div class="info-count">'+ props.ADM2_NAME +'</div>' +
            '<div class="info-title">Census Population</div>' +
            '<div class="info-count">' + props.base_line_total + '</div>' +
            '<div class="info-title">#HH Population</div>' +
            '<div class="info-count">' + props.hh_popn + '</div>' +
            '<div class="info-title">#Households</div>' +
            '<div class="info-count">' + props.households + '</div>' +
            '<div class="info-title">%Registered</div>'+
            '<div class="info-count">' + props.percent_reg + '</div>'

                :  '');

    };


    info.addTo(map);





    function getStateColor(region) {

        if (region == 'Northern') {
            return 'green';
        } else if (region == 'Central') {

            return 'blue';
        } else if (region == 'Western') {

            return 'red';
        } else if (region == 'Eastern') {

            return 'purple';
        } else {
            return 'black';
        }

    }

    //    0 	#ff0000	rgb(255, 0, 0)	hsl(0, 100%, 50%)
    //    15 	#ff4000	rgb(255, 64, 0)	hsl(15, 100%, 50%)
    //    30 	#ff8000	rgb(255, 128, 0)	hsl(30, 100%, 50%)
    //    45 	#ffbf00	rgb(255, 191, 0)	hsl(45, 100%, 50%)
    //    48 	#ffcc00	rgb(255, 204, 0)	hsl(48, 100%, 50%)
    //    60 	#ffff00	rgb(255, 255, 0)	hsl(60, 100%, 50%)
    //    75 	#bfff00	rgb(191, 255, 0)	hsl(75, 100%, 50%)
    //    75 	#608000	rgb(96, 128, 0)	hsl(75, 100%, 25%)
    //    90 	#408000	rgb(64, 128, 0)	hsl(90, 100%, 25%)
    //    105 	#208000	rgb(32, 128, 0)	hsl(105, 100%, 25%)
    //    120 	#008000	rgb(0, 128, 0)	hsl(120, 100%, 25%)
    //    135 	#008020	rgb(0, 128, 32)	hsl(135, 100%, 25%)
    // get color depending on population density value
    function getColor(d) {
        return d > 1000000 ? '#006600' :
            d > 500000  ? '#00b300' :
                d > 200000  ? '#66cc00' :
                    d > 100000  ? '#bfff00' :
                        d > 50000   ? '#ffcc00' :
                            d > 20000   ? '#ff8000' :
                                d > 10000   ? '#ff4000' :
                                    '#bb0000';
    }

    function style(feature) {
        return {
            weight: 2,
            opacity: 1,
            color: 'white',
            dashArray: '3',
            fillOpacity: 0.9,
//            fillColor: getStateColor(feature.properties.Adm_Region)
//            fillColor: getColor(feature.properties.base_line_total)
            fillColor: getColor(feature.properties.hh_popn)
        };
    }

    function highlightFeature(e) {
        var layer = e.target;

        layer.setStyle({
            weight: 3,
            color: '#666',
            dashArray: '',
            fillOpacity: 0.7
        });

        if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
            layer.bringToFront();
        }

        info.update(layer.feature.properties);
    }

    var geojson;

    function resetHighlight(e) {
        geojson.resetStyle(e.target);
        info.update();
    }

    function zoomToFeature(e) {
        map.fitBounds(e.target.getBounds());
    }

    function onEachFeature(feature, layer) {
        layer.on({
            mouseover: highlightFeature,
            mouseout: resetHighlight,
            click: zoomToFeature
        });
    }

    geojson = L.geoJson(states, {
        style: style,
        onEachFeature: onEachFeature
    }).addTo(map);

    map.attributionControl.addAttribution('Population data &copy;');


    var legend = L.control({position: 'bottomright'});

    legend.onAdd = function (map) {

        var div = L.DomUtil.create('div', 'info legend'),
            grades = [1000, 10000, 20000, 50000, 100000, 200000, 500000, 1000000, 10000000],
            labels = [],
            from, to;

        for (var i = 0; i < grades.length; i++) {
            from = grades[i];
            to = grades[i + 1];

            labels.push(
                '<i style="background:' + getColor(from + 1) + '"></i> ' +
                from + (to ? '&ndash;' + to : '+'));
        }

        div.innerHTML = labels.join('<br>');
        return div;
    };

    legend.addTo(map);




</script>

