<?php $baseline= $this->db->select()->from('baseline')->where(array('active'=>1,'baseline_year'=>2015))->get()->row(); ?>


<style>
    .table-bordered, .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
        text-align: right;
    }
</style>


<table class="table table-striped table-bordered table-advance table-hover" id="sample_1">

    <thead>
    <tr>

        <th style="text-align: left;">  <i class="fa fa-globe"></i>  Location </th>
        <th>Baseline</th>
        <th>Registered</th>
        <th>Registered%</th>
        <th>Not Registered</th>
    </tr>
    </thead>
    <tbody>

    <tr>


        <td style="text-align: left;"  class="highlight">
            <div class="success"></div>
            <a href="javascript:;" style="text-decoration: none;">Population </a>
        </td>

        <td><?php echo number_format($bp=$baseline->popullation) ?></td>
        <td><?php $hc=$this->model->total_head_count(); echo number_format($hcc=$hc->person_vht); ?></td>
        <td><?php echo number_format(($hcc/$bp)*100,3) ?>%</td>
        <td><?php echo number_format(($bp-$hcc)); ?></td>


    </tr>
    <tr>


        <td style="text-align: left;"  class="highlight">
            <div class="success"></div>
            <a href="javascript:;" style="text-decoration: none;">Household </a>
        </td>

        <td><?php echo number_format($bh=$baseline->household) ?></td>
        <td><?php  $h=$this->model->total_house_holds(); echo number_format($h); ?></td>
        <td><?php echo number_format(($h/$bh)*100,3) ?>%</td>
        <td><?php echo number_format(($bh-$h)); ?></td>

    </tr>
    <tr>


        <td style="text-align: left;"  class="highlight">
            <div class="success"></div>
            <a href="javascript:;" style="text-decoration: none;">Village </a>
        </td>


        <td><?php echo number_format($bv=$baseline->village) ?></td>
        <td><?php $vi=$this->model->total_villages(); echo number_format($vi=$vi['count']); ?></td>
        <td><?php echo number_format(($vi/$bv)*100,3) ?>%</td>
        <td><?php echo number_format($bv-$vi); ?></td>


    </tr>
    <tr>

        <td style="text-align: left;"  class="highlight">
            <div class="success"></div>
            <a href="javascript:;" style="text-decoration: none;">Parish </a>
        </td>


        <td><?php echo number_format($bp=$baseline->parish) ?></td>
        <td><?php $tp=$this->model->total_parishes(); echo number_format($tp=$tp['count']); ?></td>
        <td><?php echo number_format(($tp/$bp)*100,3) ?>%</td>
        <td><?php echo number_format($bp-$tp); ?></td>


    </tr>
    <tr>
        <td style="text-align: left;"  class="highlight">
            <div class="success"></div>
            <a href="javascript:;" style="text-decoration: none;"> Sub-county </a>
        </td>
        <td><?php echo number_format($bsc=$baseline->sub_county) ?></td>
        <td><?php $tsc=$this->model->total_subcounties(); echo number_format($tsc=$tsc['count']); ?></td>
        <td><?php echo number_format(($tsc/$bsc)*100,3) ?>%</td>
        <td><?php echo number_format($bsc-$tsc); ?></td>


    </tr>
    <tr>

        <td style="text-align: left;"  class="highlight">
            <div class="success"></div>
            <a href="javascript:;" style="text-decoration: none;"> District </a>
        </td>
        <td><?php echo number_format($bd=$baseline->district) ?></td>
        <td><?php $td=$this->model->total_districts();
            echo number_format($td=$td['count']); ?></td>
        <td><?php echo number_format(($td/$bd)*100,3) ?>%</td>
        <td><?php echo number_format($bd-$td); ?></td>


    </tr>

    </tbody>
</table>
