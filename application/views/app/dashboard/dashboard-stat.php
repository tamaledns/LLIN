<style>
    .dashboard-stat2 .display .number small {
        font-size: 14px;
        color: #AAB5BC;
        font-weight: 600;
        text-transform: none;
    }
</style>

<?php $baseline= $this->db->select()->from('baseline')->where(array('active'=>1,'baseline_year'=>2015))->get()->row();

?>


<?php
$bp=$baseline->popullation;
$hc=$this->model->total_head_count();  number_format($hcc=$hc->person_vht);

$reg_p=(($hcc/$bp)*100);

?>



<div class="row">

    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 bordered tooltips" data-original-title="<?php echo "Baseline : ".number_format($bp);  ?>">
            <div class="display">
                <div class="number">
                    <h3 class="font-red-haze">
                        <!--                        <span data-counter="counterup" data-value="1349">0</span>-->
                        <span  class="head_count" >0</span>
                    </h3>
                    <small>REGISTERED</small>
                </div>
                <div class="icon">
                    <i class="icon-users"></i>
                </div>
            </div>
            <div class="progress-info">
                <div class="progress">
                                        <span style="width: <?php echo $reg_p ?>%;" class="progress-bar progress-bar-success red-haze">
                                            <span class="sr-only"><?php echo number_format($reg_p,3) ?>% progress</span>
                                        </span>
                </div>
                <div class="status">
                    <div class="status-title"> progress </div>
                    <div class="status-number"> <?php echo number_format($reg_p,3) ?>% </div>
                </div>
            </div>
        </div>
    </div>


    <?php

    $bh=$baseline->household;
    $h=$this->model->total_house_holds();
    $h_p=(($h/$bh)*100);


    ?>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 bordered tooltips" data-original-title="<?php echo "Baseline : ".number_format($bh);  ?>"" >
            <div class="display">
                <div class="number">
                    <h3 class="font-green-sharp">
                        <span  class="house-holds" >0</span>
<!--                        <span data-counter="counterup" class="house-holds" data-value="2000">0</span>-->
                    </h3>
                    <small>HOUSEHOLDS</small>
                </div>
                <div class="icon">
                    <i class="icon-home"></i>
                </div>
            </div>

            <div class="progress-info">
                <div class="progress">
                                        <span style="width: <?php echo $h_p ?>%;" class="progress-bar progress-bar-success green-sharp">
                                            <span class="sr-only"><?php number_format($h_p,3) ?>% change</span>
                                        </span>
                </div>
                <div class="status">
                    <div class="status-title"> PROGRESS </div>
                    <div class="status-number"> <?php echo number_format($h_p,3) ?>% </div>
                </div>
            </div>


        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 bordered tooltips nets_allocated_tooltip"  data-original-title="0">
            <div class="display">
                <div class="number">
                    <h3 class="font-blue-sharp">
                        <span  class="nets_allocation" >0</span>
<!--                        <span data-counter="counterup" data-value="567"></span>-->
                    </h3>
                    <small>LLIN's ALLOCATED</small>
                </div>
                <div class="icon">
                    <i class="icon-pie-chart"></i>
                </div>
            </div>
            <div class="progress-info">
                <div class="progress">
                                        <span style="width: 100%;" class="progress-bar progress-bar-success blue-sharp">
                                            <span class="sr-only">100% Progress</span>
                                        </span>
                </div>
                <div class="status">
                    <div class="status-title"> Progress </div>
                    <div class="status-number"> 100% </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 bordered tooltips nets_allocated_tooltip"  data-original-title="0">
            <div class="display">
                <div class="number">
                    <h3 class="font-purple-soft">
                        <span  class="nets_picked" >0</span>
<!--                        <span data-counter="counterup" data-value="276"></span>-->
                    </h3>
                    <small>LLIN's DISTRIBUTED</small>
                </div>
                <div class="icon">
                    <i class="icon-check"></i>
                </div>
            </div>
            <div class="progress-info">
                <div class="progress">
                                        <span style="width:0%;" class="inbox-note progress-bar progress-bar-success purple-soft ">
                                            <span class="sr-only inbox-not">0% change</span>
                                        </span>
                </div>
                <div class="status">
                    <div class="status-title"> PROGRESS </div>
                    <div class="status-number  inbox-not"> 0% </div>
                </div>
            </div>
        </div>
    </div>

</div>

