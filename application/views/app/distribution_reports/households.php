<?php //$this->load->view($this->page_level.$this->page_level2.'tree_diagram'); ?>


    <link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />

<?php echo form_open(); ?>
<?php //print_r($this->input->post()) ?>
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable ">
                <div class="portlet-title hidden" style="    padding: 0px 20px 0px;">


                    <div class="caption font-dark" style="    padding: 19px 0;">
                        <i class="icon-home font-dark"></i>
                        <span class="caption-subject bold uppercase"><?php echo humanize($title) ?></span>
                        <?php //echo anchor($this->page_level.$this->page_level2.'new',' <i class="fa fa-plus"></i> Add new','class="btn btn-sm green-jungle"'); ?>
                    </div>


                    <div class="tools hidden">
                        <div class="dt-button buttons-print btn dark btn-outline"><i class="fa fa-export"></i>Print</div>
                        <div class="dt-button buttons-print btn green btn-outline"><i class="fa fa-export"></i>PDF</div>
                        <div class="dt-button buttons-print btn purple btn-outline"><i class="fa fa-export"></i>CSV</div>
                    </div>
                </div>
                <div class="portlet-body table-responsive ">
                    <div class="table-container">
                        <div class="table-actions-wrapper <?php echo $this->custom_library->role_exist('Export Distribution Reports')?'':'hidden' ?>">


                            <select name="export"  style="width: 80px;" class="btn red btn-sm  btn-outline dropdown-toggle"  onchange="this.form.submit()">
                                <option value="">Export</option>
                                <option value="export_pdf">PDF</option>
                                <option value="distribution_report">Allocation List</option>
                                <option value="excel">Excel</option>

                            </select>

                        </div>
                        <table class="table table-striped table-bordered table-hover table-checkable" id="<?php echo $subtitle ?>">
                            <thead>
                            <tr role="row" class="filter">

                                <td colspan="11">
                                <?php $this->load->view('ajax/locations_filter'); ?>
                                </td>


                            </tr>
                            <tr role="row" class="heading">
                                <th width="1%">
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" />
                                        <span></span>
                                    </label>
                                </th>


                                <!--                            <th width="20%" style="white-space: nowrap;"> VHT </th>-->
                                <th width="5%"> Household </th>
                                <th width="1%"> ID Number </th>
                                <th width="2%"> Chalk ID </th>
                                <th width="2%"> Phone </th>
                                <th width="5%"> Village </th>
                                <th width="1%">Subcounty </th>
                                <th width="1%"> #HH Popn </th>
                                <th width="1%"> Alloc ation </th>
                                <th width="1%"> Distri bution  </th>
                                <th width="1%"> Actions </th>
                            </tr>
<!--                            Household Name | ID Number | Phone |  Village |Subcounty|Population | Nets-->
                            </thead>
                            <tbody> </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- End: life time stats -->
        </div>
    </div>
<?php echo form_close() ?>


<?php
$this->load->view('ajax/'.$subtitle);

?>