<?php //$this->load->view($this->page_level.$this->page_level2.'tree_diagram'); ?>


<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<?php //print_r($this->input->post()) ?>

<div class="row">
    <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light portlet-fit portlet-datatable ">
            <div class="portlet-title hidden" style="    padding: 0px 20px 0px;">


                <div class="caption font-dark" style="    padding: 19px 0;">
                    <i class="icon-home font-dark"></i>
                    <span class="caption-subject bold uppercase"><?php echo humanize($title) ?></span>
                    <?php //echo anchor($this->page_level.$this->page_level2.'new',' <i class="fa fa-plus"></i> Add new','class="btn btn-sm green-jungle"'); ?>
                </div>


                <div class="tools hidden">
                    <div class="dt-button buttons-print btn dark btn-outline"><i class="fa fa-export"></i>Print</div>
                    <div class="dt-button buttons-print btn green btn-outline"><i class="fa fa-export"></i>PDF</div>
                    <div class="dt-button buttons-print btn purple btn-outline"><i class="fa fa-export"></i>CSV</div>
                </div>
            </div>
            <div class="portlet-body   table-responsive ">



                <?php echo form_open(''); ?>
                <div class="table-container">
                    <div class="table-actions-wrapper">

                        <div class="tools   <?php echo $this->custom_library->role_exist('Export Distribution Reports')?'':'hidden' ?>">



                            <select name="export"  class="btn red btn-sm  btn-outline dropdown-toggle"  onchange="this.form.submit()">
                                <option value="">Export</option>
                                <option value="export_pdf">PDF</option>
                                <option value="excel">Excel</option>

                            </select>

                        </div>


                        <div class="form-inline hidden">

                            <div class="form-group">
                                <select name="action" class="table-group-action-inpu form-control input-inline input-small input-sm">
                                    <option value="">Request Type...</option>
                                    <option value="store">Store</option>
                                    <option value="release">Release</option>
                                    <option value="return">Return</option>
                                    <!--                                    <option value="approve">Approve</option>-->

                                </select>


                                <button type="submit" class="btn btn-sm green table-group-action-submi">
                                    <i class="fa fa-check"></i> Submit</button>
                            </div>

                            <div class="form-group hidden">

                                <div class="input-group input-large date-picker input-daterange" data-date="<?php echo date('Y-m-d') ?>" data-date-format="yyyy-mm-dd">
                                    <span class="input-group-addon">From </span>
                                    <input type="text" class="form-control table-group-from-input input-sm" name="from" value="<?php echo set_value('from') ?>">
                                    <span class="input-group-addon">to </span>
                                    <input type="text" class="form-control table-group-to-input input-sm" name="to" value="<?php echo set_value('to')?>">
                                </div>
                                <button type="submit" class="btn btn-sm green table-group-action-submit">
                                    <i class="icon-equalizer"></i> Filter</button>

                            </div>
                        </div>
                    </div>

                        <table class="table table-striped  table-bordered table-hover table-checkable" id="village_report">
                            <thead>
                            <tr role="row" class="filter">

                                <td colspan="8">
                                <?php $this->load->view('ajax/locations_filter'); ?>
                                </td>


                            </tr>
                            <tr role="row" class="heading">
                                <th width="1%">
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" name="i[]" class="group-checkable" data-set="#sample_2 .checkboxes" />
                                        <span></span>
                                    </label>
                                </th>


                                <th> District </th>
                                <th> Sub County </th>
                                <th> Parish </th>
                                <th> Village </th>
                                <th> Households </th>
                                <th>#HH Popn </th>
                                <th> Allocation </th>

                            </tr>



                            </thead>
                            <tbody> </tbody>
                        </table>

                </div>
                <?php echo form_close(); ?>

            </div>
        </div>
        <!-- End: life time stats -->
    </div>
</div>

<?php
$this->load->view('ajax/village_report');

?>



