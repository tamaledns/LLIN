


        <?php

        $attr=array(
            'method'=>'#',
            'class'=>'inline'
        );

       // echo form_open('#',$attr)
        ?>

        <div class="row">

        <div class="col-md-12">

            <div class="portlet light bordered">


                <div class="portlet-title">
                    <div class="caption font-green-haze">
                        <i class="icon-note font-green-haze"></i>
                        <span class="caption-subject bold uppercase"> Input Form</span>
                    </div>


                </div>
                <div class="portlet-body form">


                    <div class="row">
                    <div class="form-group">
                        <div class="col-md-12">


                            <div class="col-md-3">

                                <label class="control-label">VHT Phone</label>
                                <input name="vht_phone"  type="text" maxlength="10" minlength="10"  class="form-control input-xs phone"  value=""/>
                                <?php echo form_error('vht_phone', '<label class="text-danger">', '</label>'); ?>
                                <label class="text-danger vht_phone"></label>


                            </div>
                            <span id="vht_name">


                                </span>




                        </div>




                    </div>

                    </div>



                </div>
            </div>
        </div>
        </div>

       <?php $no=1; ?>


        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet light bordered">

                    <div class="portlet-title">
                        <div class="caption font-green-haze">
                            <i class="icon-credit-card font-green-haze"></i>
                            <span class="caption-subject bold uppercase"> Form Serials </span>

                        </div>
                        <div class="tools  font-green-haze">
                            <span class="progress"></span>
                        </div>


                    </div>

                    <div class="portlet-body serials">



                        <div class="form-group hidden" style="margin-bottom: 0px;">
                            <div class="md-checkbox-inline pull-left">

                                    <div class="col-md-3">
                                        <div class="md-checkbox">

                                            <input type="checkbox" id="checkbox<?php echo $no ?>"
                                                   class="md-check"
                                                   name="permissions[]"
                                                   value="" <?php echo set_checkbox('permissions[]', '',true ); ?> />
                                            <label for="checkbox<?php echo $no ?>">
                                                <span></span>
                                                <span class="check"></span>
                                                <span
                                                    class="box"></span><i style="white-space: nowrap;">Serial No</i>
                                            </label>
                                        </div>
                                    </div>

                            </div>
                            <?php echo form_error('permissions', '<label class="text-danger">', '<label>') ?>


                            <div class="form-group">
                                <label class="col-md-3 control-label">Inline Checkboxes</label>
                                <div class="col-md-9">
                                    <div class="mt-checkbox-inline">
                                        <label class="mt-checkbox">
                                            <input type="checkbox" id="inlineCheckbox21" value="option1"> Checkbox 1
                                            <span></span>
                                        </label>
                                        <label class="mt-checkbox">
                                            <input type="checkbox" id="inlineCheckbox22" value="option2"> Checkbox 2
                                            <span></span>
                                        </label>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-actions hidden">
                            <div class="row">
                                <!--                        col-md-offset-2-->
                                <div class="col-md-12">
                                    <button type="reset" class="btn default"> <i class="fa fa-remove"></i> Cancel</button>
                                    <button type="submit" class="btn green-jungle"><i class="fa fa-save"></i> Save</button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->

            </div>


        </div>




        <script src="<?php echo base_url() ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>


        <?php //echo form_close() ?>

        <script>

            $('.phone').keyup(function(){

                var first_no=$(this).val();
                var variable2 = first_no.substring(0, 2);

                if(variable2.length>1 && variable2 != '07'){


                    $( this ).css( "color", "red" );
                    $( this ).css( "border-color", "red" );
                    alert('Invalid Phone Number entered');
                }else{

                    $( this ).css( "color", "#555" );
                    $( this ).css( "border-color", "#c2cad8" );


                    if(first_no.length==10){

                        //alert(first_no);


                        $.ajax({
                            type: 'GET',
                            url: '<?php echo base_url("index.php/ajax_api/get_vht_details")?>/' + first_no,
                            beforeSend:function(){
                                $("#vht_name").html('<div style="text-align: center; "><i  class="fa fa-lg fa-spinner fa-pulse"></i></div>');
                            },
                            success: function (d) {

                                //  $("#city").html(d);
                                $("#vht_name").html(d);


                            }


                        });

                      //  $('#vht_name').html(first_no+'<br/>Dennis<br/>Abim');

                    }


                }
                //alert(variable2);


            });



        </script>


