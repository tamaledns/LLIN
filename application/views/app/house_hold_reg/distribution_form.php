<?php //echo $this->uri->segment(1); ?>

<?php $household=$this->db->select('a.*,b.form_num')->from('reg_detail a')->join('registration b','a.data_id=b.id')->where(array('a.id'=>$id/date('Y')))->get()->row(); ?>
<?php
$final_pop=$household->person_final!=0?$household->person_final:$household->person_vht;
$hp= nets_agg_only($final_pop); ?>

<?php if($this->uri->segment(1)=='ajax_api'){ ?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Distribution of <b> <?php echo $household->last_name.' '.$household->first_name ?></b> Expected to Pick : <b> <?php  echo  $hp['house_hold_nets']  ?> Nets</b></h4>
</div>
<div class="modal-body">
<?php } ?>

    <div class="row">
        <div class="<?php echo  $this->uri->segment(1)=='ajax_api'?'col-md-12':'col-md-6'; ?>">
            <?php echo form_open('app/house_hold_reg/distribution_form/'.$id) ?>
            <div class="portlet light">
                <?php if($this->uri->segment(1)=='app'){ ?>
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>Left Aligned </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                        <a href="javascript:;" class="reload"> </a>
                        <a href="javascript:;" class="remove"> </a>
                    </div>
                </div>
                <?php } ?>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->

                    <input name="form_num" type="text" hidden value="<?php echo $household->form_num;  ?>">

                    <div class="form-body">

                        <div class="form-group col-md-4">
                            <label>Signed ?</label>
                            <div class="mt-radio-inline">

                                <label class="mt-radio">
                                    <input type="radio" name="signed"  <?php echo  set_radio('signed', 'N'); ?>  value="N" checked> No
                                    <span></span>
                                </label>

                                <label class="mt-radio">
                                    <input type="radio" name="signed" <?php echo  set_radio('signed', 'Y'); ?>   value="Y"> Yes
                                    <span></span>
                                </label>

                            </div>
                            <?php echo form_error('signed', '<span class="help-block text-danger">', '</span>'); ?>
                        </div>

                        <div class="form-group  col-md-4">
                            <label class="control-label">Date</label>
                            <input type="text"  required name="sign_date"  data-date-format="yyyy-mm-dd" value="<?php echo set_value('sign_date') ?>" class="form-control  date date-picker" placeholder="Enter Sign Date">
                            <?php echo form_error('sign_date', '<span class="help-block text-danger">', '</span>'); ?>
                        </div>


                        <div class="form-group  col-md-4">
                            <label class="control-label">No. Distributed</label>
                            <input type="number" name="distributed_no" required class="form-control" placeholder="No. Distributed">
                            <?php echo form_error('distributed_no', '<span class="help-block text-danger">', '</span>'); ?>
                        </div>

                        <div class="form-group last ">
                            <label class="control-label text-danger bold">NOTE : </label>
                            <p class="form-control-static text-primary"> Enter the Distribution results </p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn green">Submit</button>
                        <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                    </div>
                    <?php echo form_close(); ?>
                    <!-- END FORM-->
                </div>
            </div>

        </div>
    </div>

<?php echo  $this->uri->segment(1)=='ajax_api'?'</div>':''; ?>

<!--<div class="modal-footer">-->
<!--    <button type="button" class="btn default" data-dismiss="modal">Close</button>-->
<!--    <button type="button" class="btn blue">Save changes</button>-->
<!--</div>-->


    <?php if( $this->uri->segment(1)=='ajax_api'){ ?>
    <script src="<?php echo base_url() ?>assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
    <?php } ?>

