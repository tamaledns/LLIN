<?php


function house_hold_reg($type = null,$id=null)
    {
        $data = array(
            'title' => $title=$this->uri->segment(2),
            'subtitle' => '',
            'page_level' => $this->page_level

        );
        $root = $this->page_level . $this->page_level2;

        $this->load->view($this->page_level . 'header', $data);

        $data = array('tree' => $this);


        switch ($type) {
            case 'new';
            default:


                $this->form_validation
                    //this is the form filters
                    ->set_rules('form_date', 'Form Date', 'trim')
                    ->set_rules('serial_no', 'Serial No', 'trim')
                    //this the details of the current form
                    ->set_rules('district', 'District', 'trim')
                    ->set_rules('subcounty', 'Subcounty', 'trim')
                    ->set_rules('parish', 'Parish', 'trim')
                    ->set_rules('village', 'Village', 'trim')
                    ->set_rules('vht_name', 'VHT Name', 'trim')
                    ->set_rules('vht_code', 'VHT Code', 'trim')
                    ->set_rules('vht_code', 'VHT Code', 'trim')
                    ///this the number of selected households
                    ->set_rules('house_holds[][household_head]', 'House Hold Head', 'trim')
                    ->set_rules('phones[][phone]', 'Phone', 'trim')
                    ->set_rules('national_ids[][national_id]', 'National ID', 'trim')
                    ->set_rules('persons[][total_persons]', 'Total Persons', 'trim');

                if ($this->form_validation->run() == true) {


                    switch ($this->input->post('submit')) {

                        case 'filter':

                            $form = $this->db->select('a.phone,a.form_num,b.name,b.code,c.village,d.parish,e.subcounty,f.district,c.id as village_id')
                                ->from('forms a')
                                ->join('vht b', 'a.phone=b.phone')
                                ->join('village c', 'c.id=b.village')
                                ->join('parish d', 'd.id=c.parish')
                                ->join('subcounty e', 'e.id=d.subcounty')
                                ->join('district f', 'f.id=e.district')
                                ->where(array('form_num' => $f_id = $this->input->post('serial_no')))->get()->row();

                            if (count($form) == 0) {
                                $data['alert'] = $f_id;
                            }
                            $data['form'] = $form;


                            break;
                        case 'house_hold_form':

                            //ID, Form_Num, Village, VHTName, VHTPhone, VHTCode, Date, Point, LC1, Signed, LCDate, Entered_by, Last_update, StartTime, StopTime, delFlag, confirm, created_on, created_by
                            $form_data=array(
                                'form_num'=>$this->input->post('form_num'),
                                'village'=>$this->input->post('village_id'),
                                'vhtname'=>$this->input->post('vht_name'),
                                'vhtphone'=>$this->input->post('vht_phone'),
                                'vhtcode'=>$this->input->post('vht_code'),

                                'created_by' => $this->session->userdata('id'),
                                'created_on' => $time=time()

//                                    'Date'=>1,
//                                    'LC1'=>1,
//                                    'Form_Num'=>1,
//                                    'Form_Num'=>1,
                            );

                            $this->db->insert('registration',$form_data);

                            //getting the id of the form entered

                            $values=array(
                                'Form_Num'=>$this->input->post('form_num'),
                                'Village'=>$this->input->post('village_id'),
                                'VHTName'=>$this->input->post('vht_name'),
                                'VHTPhone'=>$this->input->post('vht_phone'),
                                'VHTCode'=>$this->input->post('vht_code'),

                                'created_by' => $this->session->userdata('id'),
                                'created_on' => $time

                            );
                            $form_id= $this->db->select('id')->from('registration')->where($values)->get()->row();



                            //this is the part of ente

                            if(count($form_id)==1){


//                                ID, DataID, Head, Tel, NID, Person

                                $form_id=$form_id->id;
                                $no = 0;
                                foreach ($this->input->post('house_holds') as $reg) {

                                    $house_holds = $this->input->post('house_holds');
                                    $phones = $this->input->post('phones');
                                    $national_ids = $this->input->post('national_ids');
                                    $persons = $this->input->post('persons');

                                    $house_hold_details = array(

                                        'data_id' => $form_id,
                                        'head' => $house_holds[$no]['household_head'],
                                        'tel' => $phones[$no]['phone'],
                                        'national_id' => $national_ids[$no]['national_id'],
                                        'person' => $persons[$no]['total_persons'],

                                        'created_by' => $this->session->userdata('id'),
                                        'created_on' => time()
                                    );

                                    $this->db->insert('reg_detail', $house_hold_details);
                                    //array_push($tt,$form_data);

                                }

                            }

                            break;

                    }


                }
                else {

                }


                $this->load->view($root . 'forms', $data);
                break;
            case 'list_forms':

                $this->load->view($root .$type , $data);

                break;

            case 'view':

                $this->form_validation
                    ->set_rules('house_holds[][household_head]', 'House Hold Head', 'trim')
                    ->set_rules('phones[][phone]', 'Phone', 'trim')
                    ->set_rules('national_ids[][national_id]', 'National ID', 'trim')
                    ->set_rules('persons[][total_persons]', 'Total Persons', 'trim');



                if ($this->form_validation->run() == true) {

                    switch ($this->input->post('submit')) {

                        case 'confirm':

                            break;
                        case 'update':


                            // $form_id=$form_id->id;
                            $no = 0;
                            foreach ($this->input->post('house_holds') as $reg) {

                                $ids = $this->input->post('ids');
                                $house_holds = $this->input->post('house_holds');
                                $phones = $this->input->post('phones');
                                $national_ids = $this->input->post('national_ids');
                                $persons = $this->input->post('persons');

                                $house_hold_details = array(

//                                        'data_id' => $form_id,
                                    'head' => $house_holds[$no]['household_head'],
                                    'tel' => $phones[$no]['phone'],
                                    'national_id' => $national_ids[$no]['national_id'],
                                    'person' => $persons[$no]['total_persons'],
                                    'updated_by' => $this->session->userdata('id'),
                                    'updated_on' => time()
                                );

                                $this->db->where(array('id'=>$ids[$no]['id']))->update('reg_detail', $house_hold_details);
                                //array_push($tt,$form_data);

                            }

                            break;
                    }

                }


                $form = $this->db->select('a.phone,a.form_num,b.name,b.code,c.village,d.parish,e.subcounty,f.district,c.id as village_id')
                    ->from('forms a')
                    ->join('vht b', 'a.phone=b.phone')
                    ->join('village c', 'c.id=b.village')
                    ->join('parish d', 'd.id=c.parish')
                    ->join('subcounty e', 'e.id=d.subcounty')
                    ->join('district f', 'f.id=e.district')
                    ->where(array('form_num' => $id))->get()->row();

                $data['form'] = $form;
                $this->load->view($root . $type, $data);

                break;
        }


        $this->load->view($this->page_level . 'footer_table', $data);
    }

?>