<div class="row">


    <input hidden name="reg_id" type="number" value="<?php echo $registered_forms[0]->id ?>">


    <!--                    this is the end of query for entered forms-->

    <div class="col-md-12" style="border-bottom:  dotted thin silver">

        <h4 class="note note-info col-md-6"><b>Original : <?php echo strlen($registered_forms[0]->first_name)>0? $registered_forms[0]->first_name.' '.$registered_forms[0]->last_name:'Not Known' ?></b> <?php echo trending_date($registered_forms[0]->created_on) ?>

        <?php

        //                        getting house holds for the specific form_id
        $form1=$this->db->select()->from('reg_detail')->where(array('data_id'=>$registered_forms[0]->id))->order_by('row_no','asc')->get()->result();

        echo '<span class="label label-primary pull-right">Records : '.$count1=count($form1).'</span>';

       ?>
<!--             this is the query for form 2 -->
        </h4>


        <?php if(isset($registered_forms[1])){ ?>
            <h4 class="note note-info col-md-6">

                <b>Verification : <?php echo strlen($registered_forms[1]->first_name)>0?$registered_forms[1]->first_name.' '.$registered_forms[1]->last_name:'Not Known'; ?> </b> <?php echo trending_date($registered_forms[1]->created_on) ?>


            <?php
            //                        getting house holds for the specific form_id

            $count2= $this->db->where(array('data_id'=>$registered_forms[1]->id))->from('reg_detail')->count_all_results();

            echo '<span class="label label-primary pull-right">Records : '.$count2.'</span>';
            ?>
            </h4>
        <?php }else{
            $count2=0;
            $form2=array();
            echo '<h4 class="note note-info col-md-6">

                            <b>Form2 : Not Known</b><span class="label label-primary pull-right">Records : '.$count2.'</span></h4>';
        } ?>


        </div>
    <div class="col-md-12">


        <div class="table-scrollable">
        <table class="table table-bordered table-striped  table-hover">
            <thead>
            <tr>
            <tr>
<th></th>
                <th colspan="2">Household Head</th>
                <th>Phone</th>
                <th>ID-No</th>
                <th>Chalk ID</th>
                <th colspan="3">Total Persons</th>
                <th style="width: 1px;"></th>
            </tr>
            <tr>

                <th></th>
                <th  style="width: 180px; !important;">&nbsp;Surname</th>
                <th  style="width: 180px; !important;">First Name</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th style="width: 100px; !important;"></th>
                <th   style="width: 70px; !important;">VHT</th>
                <th   style="width: 70px; !important;">PC</th>
                <th   style="width: 70px; !important;">FINAL</th>
                <th></th>
            </tr>

            </thead>
            <tbody>

            <?php

            $no=1;
            foreach($form1 as $f1){ ?>

                <tr>

                    <td style="white-space: nowrap;"><?php echo $no.' Original '; ?></td>
                    <td>

                        <input required class="form-control hidden input-sm input-small no_background_table" name="ids[][id]" placeholder="Household Head"
                               value="<?php echo $f1->id ?>">

                        <input  required class="form-control input-sm input-small no_background_table" name="sir_names[][sir_name]" placeholder="Surname"
                                value="<?php echo $f1->last_name ?>">
                    </td>
                    <td>
                        <input  required class="form-control input-sm input-small no_background_table" name="first_names[][first_name]" placeholder="First Name"
                                value="<?php echo $f1->first_name ?>">
                    </td>
                    <td>
                        <input required class="form-control input-sm  input-small no_background_table"  style="min-width: 70px !important; "  placeholder="Phone" minlength="10" maxlength="12"
                               name="phones[][phone]" value="<?php echo $f1->tel ?>">
                    </td>
                    <td>
                        <input class="form-control input-sm input-small no_background_table" placeholder="ID-no" name="national_ids[][national_id]"
                               value="<?php echo $f1->national_id ?>">
                    </td>
                    <td>
                        <input class="form-control input-sm input-small no_background_table" placeholder="Chalk Id" name="chalk_ids[][chalk_id]"
                               value="<?php echo $f1->chalk_id ?>">
                    </td>

                    <?php
                    //verifying the collection entry


                    $collection_var=$f1->person_vht!=$f1->person_sc? 'collection_var':'';

                    ?>


                    <td>
                        <input <?php echo $f1->person_final!=0?'readonly':''; ?> required class="form-control col-md-2 input-sm no_background_table <?php echo $collection_var ?> " placeholder="Total Persons" name="persons_vht[][total_persons]"
                                                                                 value="<?php echo $f1->person_vht ?>">
                    </td>
                    <td>
                        <input <?php echo $f1->person_vht==0||$f1->person_final!=0?'readonly data-original-title="Please first enter VHT first"':''; ?> class="form-control col-md-2 input-sm no_background_table tooltips <?php echo $collection_var ?> " placeholder="Total Persons" name="persons_sc[][total_persons]"
                                                                                                                                                        value="<?php echo $f1->person_sc ?>">
                    </td>
                    <td>
                        <input  <?php echo $f1->person_sc==0?'readonly data-original-title="Please first enter PC first"':''; ?> class="form-control col-md-2 input-sm no_background_table tooltips" placeholder="Total Persons" name="persons_final[][total_persons]"
                                                                                                                                 value="<?php echo $f1->person_sc==$f1->person_vht?$f1->person_vht: $f1->person_final ?>">
                    </td>

                </tr>


<!--                this is the second record-->

                <?php

                if(isset($registered_forms[1]->id)){
                $f2=$this->db->select()->from('reg_detail')->where(array('data_id'=>$registered_forms[1]->id,'row_no'=>$f1->row_no))->order_by('row_no','asc')->get()->row(); ?>

                <?php if(count($f2)==1){ ?>
                <tr>

                    <td  style="white-space: nowrap;"><?php echo $no.' Verified '; ?></td>

                    <td style="padding: 8px !important; white-space: nowrap;" class="<?php  echo  trim(strtolower($f1->last_name))!=  trim(strtolower($f2->last_name))?' flagged':''; ?>">
                        <?php echo $f2->last_name ?>

                    </td>

                    <td style="padding: 8px !important; white-space: nowrap;" class="<?php  echo  trim(strtolower($f1->first_name))!=  trim(strtolower($f2->first_name))?' flagged':''; ?>">
                        <?php echo $f2->first_name ?>

                    </td>
                    <td class="<?php  echo   trim(strtolower($f1->tel))!= trim(strtolower($f2->tel))?' flagged':''; ?>">
                        <?php echo $f2->tel ?>
                    </td>
                    <td class="<?php  echo  trim(strtolower($f1->national_id))!= trim(strtolower($f2->national_id))?' flagged':''; ?>">
                        <?php echo $f2->national_id ?>
                    </td>
                    <td class="<?php  echo   trim(strtolower($f1->chalk_id))!= trim(strtolower($f2->chalk_id))?' flagged':''; ?>">
                        <?php echo $f2->chalk_id ?>
                    </td>
                    <td  class="<?php  echo  trim(strtolower($f1->person_vht))!= trim(strtolower($f2->person_vht))?' flagged':''; ?>">
                        <?php echo $f2->person_vht ?>
                    </td>
                    <td  class="<?php  echo  trim(strtolower($f1->person_sc))!= trim(strtolower($f2->person_sc))?' flagged':''; ?>">
                        <?php echo $f2->person_sc ?>
                    </td>
                    <td  class="<?php  echo  trim(strtolower($f1->person_final))!= trim(strtolower($f2->person_final))?' flagged':''; ?>">
                        <?php echo $f2->person_final ?>
                    </td>

                </tr>
                    <?php }else{
                    echo '<tr><td colspan="9">No record Found</td></tr>';
                }
                }else{
                    echo '<tr><td colspan="9">No record Found</td></tr>';
                } ?>


<!--                this is the end of the second record-->
                <tr style="background-color: #eef1f5;"><td colspan="9" style="    padding: 0px;">&nbsp;</td></tr>
            <?php    $no++;
            } ?>


            <?php

            if($count1!=$count2) {
                $extra_form = $count1 > $count2 ? 0 : 1;

                $extra = $this->db->select()->from('reg_detail')
                    ->where(array('data_id' => $registered_forms[$extra_form]->id,'row_no >='=>$no))

                    ->order_by('row_no', 'asc')
                    //->limit(3,2)
                    ->get()->result();

                $no2=$no;
                foreach ($extra as $ex ){ ?>

                    <tr class="danger">

                        <td  style="white-space: nowrap;"><?php echo $no2.' Extra '; $no2++; ?></td>
                        <td>

                            <input required class="form-control hidden input-sm input-small no_background_table" name="ids[][id]" placeholder="Household Head"
                                   value="<?php echo $ex->id ?>">

                            <input  required class="form-control input-sm input-small no_background_table" name="sir_names[][sir_name]" placeholder="Surname"
                                    value="<?php echo $ex->last_name ?>">
                        </td>
                        <td>
                            <input  required class="form-control input-sm input-small no_background_table" name="first_names[][first_name]" placeholder="First Name"
                                    value="<?php echo $ex->first_name ?>">
                        </td>
                        <td>
                            <input required class="form-control input-sm  input-small no_background_table"  style="min-width: 70px !important; "  placeholder="Phone" minlength="10" maxlength="12"
                                   name="phones[][phone]" value="<?php echo $ex->tel ?>">
                        </td>
                        <td>
                            <input class="form-control input-sm input-small no_background_table" placeholder="ID-no" name="national_ids[][national_id]"
                                   value="<?php echo $ex->national_id ?>">
                        </td>
                        <td>
                            <input class="form-control input-sm input-small no_background_table" placeholder="Chalk Id" name="chalk_ids[][chalk_id]"
                                   value="<?php echo $ex->chalk_id ?>">
                        </td>

                        <?php
                        //verifying the collection entry


                        $collection_var=$ex->person_vht!=$ex->person_sc? 'collection_var':'';

                        ?>


                        <td>
                            <input <?php echo $ex->person_final!=0?'readonly':''; ?> required class="form-control col-md-2 input-sm no_background_table <?php echo $collection_var ?> " placeholder="Total Persons" name="persons_vht[][total_persons]"
                                                                                     value="<?php echo $ex->person_vht ?>">
                        </td>
                        <td>
                            <input <?php echo $ex->person_vht==0||$f1->person_final!=0?'readonly data-original-title="Please first enter VHT first"':''; ?> class="form-control col-md-2 input-sm no_background_table tooltips <?php echo $collection_var ?> " placeholder="Total Persons" name="persons_sc[][total_persons]"
                                                                                                                                                            value="<?php echo $ex->person_sc ?>">
                        </td>
                        <td>
                            <input  <?php echo $ex->person_sc==0?'readonly data-original-title="Please first enter PC first"':''; ?> class="form-control col-md-2 input-sm no_background_table tooltips" placeholder="Total Persons" name="persons_final[][total_persons]"
                                                                                                                                     value="<?php echo $ex->person_sc==$ex->person_vht?$f1->person_vht: $ex->person_final ?>">
                        </td>

                    </tr>


             <?php   }
            }

            ?>

            </tbody>
        </table>
        </div>


    </div>


</div>

<button name="submit" value="update" type="submit" class="btn btn-sm btn-info"><i
        class="fa fa-edit"></i> Update
</button>

<button name="submit" value="save_final" type="submit" class="btn btn-sm green"><i
        class="fa fa-check"></i> Save Final
</button>
