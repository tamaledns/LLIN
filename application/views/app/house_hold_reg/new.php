<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />


<style>
    .no_background {
        background: none !important;
        border: none;
        /*width: 60%;*/
    }
    .no_background_table {
        background: none;
        /*border: none;*/
        width: 100% !important;
    }
    .table {
        width: 100%;
        margin-bottom: 10px;
    }

    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
        padding: 5px;
        /* line-height: 1.42857; */
        /* vertical-align: top; */
        border-top: 1px solid #e7ecf1;
    }


    .icon-delete{
        cursor: pointer;
    }
</style>



<?php
//this show the form status

if(isset($form) ) {

    if ($form->status == 'in_field') {

        $status = '<div style="width:100px;" class="label  label-danger">' . humanize($form->status) . '</div>';
    }
    elseif ($form->status == 'in_store') {

        $status = '<div style="width:100px;" class="label  label-warning">' . humanize($form->status) . '</div>';
    }
    elseif ($form->status == 'released') {

        $status = '<div style="width:100px;" class="label  label-primary">' . humanize($form->status) . '</div>';
    }
    elseif ($form->status == 'returned') {

        $status = '<div style="width:100px;" class="label  label-success">' . humanize($form->status) . '</div>';
    }
    else {
        $status = '<div style="width:100px;" class="label  label-danger">In field</div>';
    }
}else{
    $status = '<div style="width:100px;" class="label  label-danger">In field</div>';
}

?>





<!-- BEGIN FORM-->

<!--ISO Date: <input type="text" value="" data-mask="_/___"/><br/>-->
<!--ISO Date: <input type="text" value="____-__-__" data-mask="____-__-__"/><br/>-->
<!--Telephone: <input type="text" value="(___) ___-____" data-mask="(___) ___-____"/><br/>-->

<!-- END FORM-->




<?php echo validation_errors(); ?>
<div class="row">

    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-title hidden">
                <div class="caption">
                    <i class="fa fa-file"></i>House Hold Registration Form
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"> </a>
                    <!--                    <a href="#portlet-config" data-toggle="modal" class="config"> </a>-->
                    <!--                    <a href="javascript:;" class="reload"> </a>-->
<!--                                        <a href="javascript:;" class="remove"> </a>-->
                </div>
            </div>
            <div class="portlet-body">

                <?php
                $form_attributes = array(
                    'class' => 'form-inlin',
                );
                ?>


                <?php echo form_open('', $form_attributes) ?>

                <?php

                if (isset($alert)) {
                    $alert = array(
                        'alert' => 'warning',
                        'message' => 'Form <b>(' . $alert . ')</b> cannot be found !!!',
                        'hide' => 1
                    );
                    //$this->load->view('alert', $alert);
                }



                ?>

                <input name="village_id" hidden class="form_control no_background"
                       value="<?php echo isset($form) ? $form->village : '' ?>"/>
                <input name="form_num" hidden class="form_control no_background"
                       value="<?php echo isset($form) ? $form->form_num : '' ?>"/>



                <table class="table table-bordered table-striped  table-hover">
                    <tbody>
                    <tr>

                        <?php  $path=$this->locations->get_path($form->village);
                        //$village_name=$this->locations->get_location_name($form->village);

                        // print_r($path);
                        ?>
                        <td><b>District : <input name="district" readonly class="form_control no_background"
                                                 value="<?php echo isset($form) ?  $path[1]['name']  : '' ?>"/> </b>
                        <input name="district_id" hidden value="<?php echo $path[1]['id']  ?>">

                        </td>
                        <td><b>Subcounty : <input name="subcounty" readonly class="form_control no_background"
                                                  value="<?php echo isset($form) ? $path[2]['name']  : '' ?>"/> </b>

                            <input name="sub_county_id" hidden value="<?php echo $path[2]['id']  ?>">

                        </td>
                        <td><b>Parish : <input name="parish" readonly class="form_control no_background"
                                               value="<?php echo isset($form) ? $path[3]['name'] : '' ?> "/> </b>

                            <input name="parish_id" hidden value="<?php echo $path[3]['id']  ?>">

                        </td>
                        <td><b>Village : <input name="village" readonly class="form_control no_background"
                                                value="<?php echo isset($form) ?   $path[4]['name'] : '' ?>"/> </b></td>
                    </tr>
                    <tr>
                        <td><b>VHT Name : <input name="vht_name" readonly class="form_control no_background"
                                                 value="<?php echo isset($form) ? $form->first_name.' '.$form->last_name : '' ?>"/> </b></td>

                        <td><b>VHT Phone : <input name="vht_phone" readonly class="form_control no_background"
                                                  value="<?php echo isset($form) ? $form->phone : '' ?>"/> </b></td>

                        <td><b  <?php echo $this->session->user_type==5?'hidden':'' ?>>Form Status : <?php echo $status ?> </b></td>
                        <td><b>Date Created : <?php echo isset($form) ? trending_date(strtotime($form->date_created)) : '' ?> </b></td>
                    </tr>
                    </tbody>
                </table>

                <?php $vht_code=$form->code ?>

                <input value="<?php echo $vht_code ?>" name="vht_code" hidden>




                <!--                    This is the part where get the two entered forms  -->


                <?php
                $registered_forms=$this->db->where(array('form_num'=>$form->form_num))->from('registration')->count_all_results();

                ?>

                <!--                    this is the end of query for entered forms-->
                <?php if($registered_forms>=2){

                    $data=array(
                        'alert'=>'warning',
                        'message'=>'This form has already been entered twice '.anchor($this->page_level.$this->page_level2.'view/'.$form->form_num,'Click to View The Forms'),
                        'hide' => 1
                    );
                    $this->load->view('alert',$data);

                }else{ ?>
                <div class="row">


                    <input autocomplete="off" type="number" hidden name="entry_times" value="<?php echo $registered_forms+1 ?>">


                    <div class="col-md-12">

                        <hr/>



                        <div class="form-group col-md-2 ">


                            <label class="bold">Signed/Stamped ?</label>
                            <div class="mt-radio-inline">
                                <label class="mt-radio">
                                    <input type="radio" checked class="is_signed"  <?php echo  set_radio('is_signed', '1'); ?> required name="is_signed" id="optionsRadios4" value="1"> Yes
                                    <span></span>
                                </label>
                                <label class="mt-radio">
                                    <input type="radio" required class="is_signed" name="is_signed" <?php echo  set_radio('is_signed', '0'); ?>  id="optionsRadios5" value="0"> No
                                    <span></span>
                                </label>

                            </div>



                        </div>

                        <div class="form-group col-md-3 chairman">

                            <label class="bold tooltips" data-original-title="Name of LC Chair Person" >LC Chair Person</label>
                            <?php echo form_error('lc_chairperson', '<label style="color:#ff0000;">', '</label>'); ?>
                                <input autocomplete="off" required=""    class="form-control chairman_input" name="lc_chairperson"  value="<?php echo set_value('lc_chairperson') ?>">
                        </div>



                        <div class="form-group col-md-3 chairman">

                            <label class="bold tooltips" data-original-title="Date when it was Stamped or Signed">Date Stamped</label>
                            <?php echo form_error('sign_date', '<label style="color:#ff0000;">', '</label>'); ?>


                                <input  class="form-control date date-picker chairman_input"   autocomplete="off" required=""  data-date-format="yyyy-mm-dd" name="sign_date"
                                       value="<?php echo set_value('sign_date') ?>">


                        </div>
                        <div class="form-group col-md-3">
                            <label class="bold">Collection Date</label>
                            <input class="form-control date date-picker" autocomplete="off" required="" name="collection_date" data-date-format="yyyy-mm-dd" >
                        </div>


                        <div class="form-group col-md-1 hidden">

                            <label class="bold">Requester</label>
                            <?php echo form_error('requester', '<label style="color:#ff0000;">', '</label>'); ?>

                            <input autocomplete="off" class="form-control no_background" name="requester">

                        </div>
                    <div class="form-group col-md-1 hidden">

                            <label class="bold">Approver</label>
                            <?php echo form_error('approver', '<label style="color:#ff0000;">', '</label>'); ?>

                            <input autocomplete="off" class="form-control no_background" name="approver">

                        </div>


                    </div>

                    <div class="col-md-12">



                        <table class="table table-bordered table-striped  table-hover" id="example">
                            <thead>
                            <tr>

                                <th colspan="2">Household Head</th>
                                <th>Phone</th>
                                <th>ID-No</th>
                                <th>Chalk ID</th>
                                <th colspan="3">Total Persons</th>
                                <th style="width: 1px;"></th>
                            </tr>
                             <tr>

                                  <th  style="width: 180px; !important;">&nbsp;Surname</th>
                                  <th  style="width: 180px; !important;">First Name</th>
                                  <th>&nbsp;</th>
                                  <th>&nbsp;</th>
                                  <th style="width: 100px; !important;"></th>
                                  <th   style="width: 70px; !important;">VHT</th>
                                  <th   style="width: 70px; !important;">PC</th>
                                  <th   style="width: 70px; !important;">FINAL</th>
                                 <th></th>
                                </tr>

                            </thead>
                            <tbody  id="house_form">


<?php for($i=1;$i<=10;$i++){ ?>
                                
                                <tr class="ro">


                                    <td>
                                        <input   class="form-control input-sm input-small no_background_table" name="sir_names[][sir_name]" placeholder="SurName"
                                                value="">
                                    </td>
                                    <td>
                                        <input   class="form-control input-sm input-small no_background_table" name="first_names[][first_name]" placeholder="First Name"
                                                value="">
                                    </td>
                                    <td>
                                        <input  class="form-control input-sm  input-small no_background_table phone" autocomplete="off"  placeholder="Phone" minlength="10" maxlength="10"
                                               name="phones[][phone]" value="">
                                    </td>
                                    <td>
                                        <input class="form-control input-sm input-small no_background_table" autocomplete="off" placeholder="ID-No" name="national_ids[][national_id]"  maxlength="13"
                                               value="">
                                    </td>
                                    <td><?php echo $vht_code ?>/

                                        <input class="form-control input-sm no_background_table" style="width: 70px !important; display: inline;" maxlength="3"  autocomplete="off" placeholder="Chalk Id" name="chalk_ids[][chalk_id]"  value="">
<!--                                        data-mask="_/___"-->
                                    </td>
                                    <td>
                                        <input  class="form-control col-md-2 input-sm no_background_table" autocomplete="off" placeholder="VHT" name="persons_vht[][total_persons]"
                                               value="">
                                    </td>
                                    <td><input class="form-control col-md-2 input-sm no_background_table" autocomplete="off" placeholder="PC" name="persons_sc[][total_persons]"
                                               value="" ></td>
                                    <td>
                                        <input class="form-control col-md-2 input-sm no_background_table" autocomplete="off" placeholder="Final" name="persons_final[][total_persons]"
                                               value="">
                                    </td>
                                    <td> <i class="fa fa-close fa-lg icon-delete text-danger"></i> </td>

                                </tr>

                            <?php } ?>



                            </tbody>
                        </table>


                    </div>




                </div>
            </div>






        <button name="submit" value="house_hold_form" type="submit" class="btn btn-sm green"><i
                class="fa fa-save"></i> Save Form
        </button>
        <button type="reset" class="btn btn-sm btn-info"><i class="fa fa-save"></i> Reset </button>

            <div id="add_more" class="btn btn-sm btn-warning pull-right"><i class="fa fa-plus"></i> Add Entry</div>

            <?php } ?>

            <?php echo form_close(); ?>

        </div>
    </div>
</div>

</div>


<script src="<?php echo base_url() ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<!--<script src="--><?php //echo base_url() ?><!--assets/global/scripts/app.min.js" type="text/javascript"></script>-->
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<!--<script src="--><?php //echo base_url() ?><!--assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>-->
<!-- END PAGE LEVEL SCRIPTS -->



<script>
    $("#add_more").click(function (e) {
        e.preventDefault();
        $("#house_form").append(' <tr  class="ro"><td> <input class="form-control input-sm input-small no_background_table" name="sir_names[][sir_name]" placeholder="Sir Name" value=""> </td> <td> <input class="form-control input-sm input-small no_background_table" name="first_names[][first_name]" placeholder="First Name"  value=""> </td> <td>  <input required autocomplete="off"  class="form-control input-sm  input-small no_background_table phone"  placeholder="Phone" maxlength="10" minlength="10" name="phones[][phone]" value="">  </td> <td> <input autocomplete="off" class="form-control input-sm input-small no_background_table" placeholder="ID-No"  maxlength="14" name="national_ids[][national_id]"  value="">   </td>' +'<td> <?php echo $vht_code ?>/<input class="form-control input-sm no_background_table" style="width: 70px !important; display: inline;" maxlength="3"  autocomplete="off" placeholder="Chalk Id" name="chalk_ids[][chalk_id]"  value="">   </td>' + ' <td>  <input autocomplete="off" required class="form-control col-md-2 input-sm no_background_table" placeholder="VHT" name="persons_vht[][total_persons]" value="">  </td> <td><input required autocomplete="off" class="form-control col-md-2 input-sm no_background_table" placeholder="SC" name="persons_sc[][total_persons]" value="" ></td> <td> <input required autocomplete="off" class="form-control col-md-2 input-sm no_background_table" placeholder="Final" name="persons_final[][total_persons]" value=""></td>  <td> <i class="fa fa-close fa-lg icon-delete text-danger" ></i> </td> </tr>');
    })



    $('.icon-delete').click(function(){
        //return confirm ("Are Sure you want to remove this row")
       $(this).closest('tr').remove();
        //$(this).parents('tr').remove();

        });

    Array.prototype.forEach.call(document.body.querySelectorAll("*[data-mask]"), applyDataMask);

    function applyDataMask(field) {
        var mask = field.dataset.mask.split('');

        // For now, this just strips everything that's not a number
        function stripMask(maskedData) {
            function isDigit(char) {
                return /\d/.test(char);
            }
            return maskedData.split('').filter(isDigit);
        }

        // Replace `_` characters with characters from `data`
        function applyMask(data) {
            return mask.map(function(char) {
                if (char != '_') return char;
                if (data.length == 0) return char;
                return data.shift();
            }).join('')
        }

        function reapplyMask(data) {
            return applyMask(stripMask(data));
        }

        function changed() {
            var oldStart = field.selectionStart;
            var oldEnd = field.selectionEnd;

            field.value = reapplyMask(field.value);

            field.selectionStart = oldStart;
            field.selectionEnd = oldEnd;
        }

        field.addEventListener('click', changed)
        field.addEventListener('keyup', changed)
    }


    $('.is_signed').click(function(){
        var is=$('input:radio[name=is_signed]:checked').val();

        if(is==0){
            $('.chairman').hide();
            $('.chairman_input').removeAttr('required');
        }else{
            $('.chairman').show();
            $('.chairman_input').attr('required','');
        }

    });


    $('.phone').keyup(function(){

        var first_no=$(this).val();
        var variable2 = first_no.substring(0, 2);

        if(variable2.length>1 && variable2 != '07'){


            $( this ).css( "color", "red" );
            $( this ).css( "border-color", "red" );
            alert('Invalid Phone Number entered');
        }else{
            $( this ).css( "color", "#555" );
            $( this ).css( "border-color", "#c2cad8" );
        }
        //alert(variable2);


    });


//    var name = "cat1234"
//
//    var variable2 = name.substring(0, 3);

</script>

