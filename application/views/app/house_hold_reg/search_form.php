
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php echo base_url(); ?>assets/pages/css/search.min.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->
<?php //print_r($array=$this->session->permission);

//echo  $this->session->user_type;
?>

<div class="search-page search-content-2">
    <div class="search-bar bordered">
        <div class="row">
            <div class="col-md-7">
                <?php echo form_open($this->page_level.$this->page_level2.'search_form') ?>
                <div class="input-group">
                    <input type="text" name="search" maxlength="6" value="<?php echo set_value('search') ?>" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                                            <button class="btn blue uppercase bold" type="submit">Search</button>
                                        </span>
                </div>
                <?php echo form_close(); ?>
            </div>
            <div class="col-md-5">
                <p class="search-desc clearfix"> Please enter the form Number, The Search Number is a Six Digit Number </p>
                <?php echo form_error('search','<span class="alert text-danger">','</span>') ?>
            </div>
        </div>
    </div>
    <div class="row <?php echo $this->input->post('search')?'':'hidden'; ?>">
        <div class="col-md-12">
            <div class="search-container bordered">
                <ul>
                    <li class="search-item-header">
                        <div class="row">
                            <div class="col-sm-9 col-xs-8">
                                <h3>Search results...<?php echo $this->input->post('search')? $this->input->post('search'):'';?></h3>
                            </div>
                            <div class="col-sm-3 col-xs-4 hidden">
                                <div class="form-group">
                                    <select class="bs-select form-control">
                                        <option>Questions</option>
                                        <option>Answers</option>
                                        <option>Users</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </li>

                    <?php


                    if(count($form)==0){
                        $alert=array(
                            'alert'=>'info',
                            'message'=>'Form cannot be found',
                            'hide'=>1
                        );
                        $this->load->view('alert',$alert);
                    }else{
                    if($form->nos >=2 && $this->session->user_type==5){

                            $alert=array(
                                'alert'=>'info',
                                'message'=>'Form cannot be found',
                                'hide'=>1
                            );
                            $this->load->view('alert',$alert);

                    }else{

                    if(isset($form)){  //print_r($form);


                if ($form->status == 'in_field') {

                    $status = '<div style="width:100%;" class="btn btn-sm btn-danger">' . humanize($form->status) . '</div>';
                }
                elseif ($form->status == 'in_store') {

                    $status = '<div style="width:100%;" class="btn btn-sm btn-warning">' . humanize($form->status). '</div>';
                }
                elseif ($form->status == 'released') {

                    $status = '<div style="width:100%;" class="btn btn-sm btn-primary">' . humanize($form->status) . '</div>';
                }
                elseif ($form->status == 'returned') {

                    $status = '<div style="width:100%;" class="btn btn-sm green-jungle">' . humanize($form->status). '</div>';
                }
                else {
                    $status = '<div style="width:100%;" class="btn btn-sm btn-danger">In field</div>';
                }


                 $path = $this->locations->get_path($form->village);


                        ?>
                    <li class="search-item clearfix">
                        <div class="search-content">
                            <div class="row">
                                <div class="col-sm-4 col-xs-10">
                                    <h2 class="search-title">


                                        <?php echo anchor($this->page_level . '/house_hold_reg/new/' . $form->form_num, 'Form Num : '.$form->form_num, 'title="View"', 'target="_blank"') ?>
                                    </h2>
                                    <p class="search-desc"> Name :
                                        <a href="javascript:;"><?php echo $form->first_name.' '.$form->last_name ?></a> -
                                        <span class="font-grey-salt">Date : <?php echo  trending_date(strtotime($form->date_created)) ?></span>
                                    </p>
                                </div>
                                <div class="col-sm-2 col-xs-4">
                                    <p class="search-counter-number"> <?php echo $form->phone ?></p>
                                    <p class="search-counter-label uppercase">Phone</p>
                                </div>
                                <div class="col-sm-2 col-xs-4">
                                    <p class="search-counter-number"><?php echo  $path[4]['name']; ?></p>
                                    <p class="search-counter-label uppercase">Village</p>
                                </div>
                                <div class="col-sm-<?php echo $this->session->user_type==5?'4':'2' ?> col-xs-4">
                                    <p class="search-counter-number"><?php echo  $path[2]['name']; ?></p>
                                    <p class="search-counter-label uppercase">Sub county</p>
                                </div>

                                <div class="col-sm-2 col-xs-4 <?php echo $this->session->user_type==5?'hidden':'' ?>">
                                    <p class="search-counter-number"><?php echo  $status; ?></p>
                                    <p class="search-counter-label uppercase">Status</p>
                                </div>
                            </div>
                    </li>
                    <?php
                    }

                    }

                    }
                    ?>


                </ul>
                <div class="search-pagination hidden">
                    <ul class="pagination">
                        <li class="page-active">
                            <a href="javascript:;"> 1 </a>
                        </li>
                        <li>
                            <a href="javascript:;"> 2 </a>
                        </li>
                        <li>
                            <a href="javascript:;"> 3 </a>
                        </li>
                        <li>
                            <a href="javascript:;"> 4 </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
</div>