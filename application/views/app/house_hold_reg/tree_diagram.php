<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="<?php echo base_url() ?>assets/global/plugins/jstree/dist/themes/default/style.min.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->


<div class="col-md-3">
    <div class="portlet light  bordered">

        <div class="portlet-body">


            <div id="tree_1" class="tree-demo">
                <ul>
                    <li  data-jstree='{ "selected" : true , "opened" : true }'> Uganda

                        <?php

                        $tree->display_children(1,0);

                        ?>
                        <!--                            <ul>-->
                        <!--                                <li data-jstree='{ "selected" : true }'>-->
                        <!--                                    <a href="javascript:;"> Initially </a>-->
                        <!--                                </li>-->
                        <!--                                <li data-jstree='{ "icon" : "fa fa-briefcase icon-state-success " }'> custom icon URL </li>-->
                        <!--                                <li data-jstree='{ "opened" : true }'> initially open-->
                        <!--                                    <ul>-->
                        <!--                                        <li data-jstree='{ "disabled" : true }'> Disabled Node </li>-->
                        <!--                                        <li data-jstree='{ "type" : "file" }'> Another node </li>-->
                        <!--                                    </ul>-->
                        <!--                                </li>-->
                        <!--                                <li data-jstree='{ "icon" : "fa fa-warning icon-state-danger" }'> Custom icon class (bootstrap) </li>-->
                        <!--                            </ul>-->
                    </li>
                    <!--                        <li data-jstree='{ "type" : "file" }'>-->
                    <!--                            <a href="http://www.jstree.com"> Clickanle link node </a>-->
                    <!--                        </li>-->
                </ul>
            </div>
        </div>
    </div>
</div>