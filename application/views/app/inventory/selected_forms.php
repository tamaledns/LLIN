<style>
    .simplelineicons-demo .item-box {
        /*display: inline-block;*/
        font-size: 12px;
        /*margin: 0 -.22em 1em 0;*/
        /*padding-left: 1em;*/
        /*width: 100%;*/
    }

    .mt-checkbox, .mt-radio {
        display: inline-block;
        position: relative;
        padding-left: 30px;
        margin-bottom: 5px;
        margin-top: 5px;
        cursor: pointer;
        font-size: 14px;
        webkit-transition: all .3s;
        -moz-transition: all .3s;
        -ms-transition: all .3s;
        -o-transition: all .3s;
        transition: all .3s;
    }

    .simplelineicons-demo .item-box .item {
        background-color: #fff;
        color: #33383e;
        border-radius: 8px;
        display: inline-block;
        /* padding: 10px; */
        width: 98%;
    }

    .simplelineicons-demo .item-box {
        width: 24.333%;
    }

    .portlet.light>.portlet-title>.caption>.caption-subject {
        font-size: 11px;
    }

    .portlet.light.portlet-fit>.portlet-title {
        padding: 8px 20px 0px;
    }

    .portlet.light.portlet-fit>.portlet-body {
        padding: 0px 20px 5px;
    }
    .portlet {
        margin-top: -10px;
        margin-bottom: 25px;
        padding: 0;
        border-radius: 4px;
    }
    .item{
        padding: 0 !important;
    }



</style>




<div class="row">

<?php if($subtitle=='view'||$subtitle=='approve'||$subtitle=='reject'){

//echo $id;

    $form = $this->db->select('a.*,b.village,c.status as form_status')->from('inventory a')->join('forms c','c.form_num=a.form_num')->join('vht b','c.phone=b.phone')->where(array('a.request_code' => $id))->get()->result();



    if ($form[0]->status == 'pending') {

        $label=humanize($form[0]->status);
        $alert='info';

    }
    elseif ($form[0]->status == 'approved') {

        $label=humanize($form[0]->status);
        $alert='success';



    }
    elseif ($form[0]->status == 'rejected') {

        $label=humanize($form[0]->status);
        $alert='danger';


    }else{

        $label=humanize('Pending');
        $alert='info';


    }

//form status

//    print_r($form[0]);



    ?>




        <div class="col-md-12">

            <div class="portlet light ">

                <div class="portlet-body">

                    <div class="row">


                        <div class="col-md-12">
                            <div class="mt-element-ribbon bg-grey-steel"  style="margin-bottom: 0;">



                                <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color- ribbon-border-dash-hor uppercase ribbon-shadow ">
                                    <div class="ribbon-sub ribbon-clip"></div> Request Code : <b><?php echo $form[0]->request_code ?></b></b> </div>


                                <div class="ribbon ribbon-right ribbon-clip ribbon-shadow ribbon-border-dash-hor  uppercase">
                                    <div class="ribbon-sub ribbon-clip ribbon-right"></div>
                                        Request Type: <b><?php echo $form[0]->request_type ?></b> </div>


                                <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-<?php echo $alert ?> uppercase ribbon-shadow ">
                                    <div class="ribbon-sub ribbon-clip"></div> Request status : <?php echo $label ?> </div>




                                <div class="row">

                                    <div class="col-md-12" style="padding-bottom: 10px;padding-top: 10px;">


                                <div class="col-md-5">

                                    <b>Requested On : </b>  <?php  echo trending_date($form[0]->created_on) ?><br/>

                                    <b>Requested by : </b> <?php echo $this->custom_library->get_user_full_name($form[0]->requested_by) ?>

                                </div>

                                <div class="col-md-5">
                                    <b>Approved On : </b> <?php echo $form[0]->approved_on!=0?trending_date($form[0]->approved_on):'' ?><br/>

                                    <b>Approved by : </b> <?php echo  $this->custom_library->get_user_full_name($form[0]->approved_by) ?>
                                </div>



                                <div class="col-md-2 pull-right">

                                    <?php echo $this->custom_library->role_exist('Approve Request')?($form[0]->status == 'pending'? anchor($this->page_level.$this->page_level2.'approve/'.$form[0]->request_code,'<i class="fa fa-check"></i> Approve','class="btn btn-sm green-jungle col-md-12" onclick="return confirm(\'are sure you want to do this ?\')"'):''):'hidden'; ?>

                                    <?php echo $this->custom_library->role_exist('Reject Request')?($form[0]->status == 'pending'? anchor($this->page_level.$this->page_level2.'reject/'.$form[0]->request_code,'<i class="fa fa-ban"></i> Reject','class="btn btn-sm btn-danger  col-md-12" onclick="return confirm(\'are sure you want to do this ?\')"'):''):'' ?>

                                </div>

                                    </div>


                                </div>


                            </div>
                        </div>

                    </div>


                </div>
            </div>
        </div>

        <div class="col-md-12" >

            <div class="simplelineicons-demo">

                <?php

                foreach ($form as $form):

                    $path=$this->locations->get_path($form->village);


                    if ($form->form_status == 'in_field') {

                        $form_label=humanize($form->form_status);
                        $form_alert='danger';

                    }
                    elseif ($form->form_status == 'in_store') {
                        $form_label=humanize($form->form_status);
                        $form_alert='warning';
                    }
                    elseif ($form->form_status == 'released') {


                        $form_label=humanize($form->form_status);
                        $form_alert='primary';

                    }
                    elseif ($form->form_status == 'returned') {
                        $form_label=humanize($form->form_status);
                        $form_alert='success';
                    }else{



                        $form_label=humanize('In Field');
                        $form_alert='danger';

                    }



                    ?>



                    <span class="item-box">
                                                <span class="item">



                                                       <div class="portlet mt-element-ribbon light portlet-fit" style="margin-bottom: 0;">
                                <div class="ribbon ribbon-right ribbon-clip ribbon-shadow ribbon-border-dash-hor ribbon-color-<?php echo $form_alert ?>">
                                    <div class="ribbon-sub ribbon-clip ribbon-right"></div> Form Status :<b> <?php echo $form_label ?> </b></div>
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class=" icon-layers font-blue"></i>
                                        <span class="caption-subject font-blue bold "><?php echo $form->form_num ?>:</span>
                                    </div>
                                </div>
                                                           <div class="portlet-body">

                                                                <?php echo  $path[2]['name'].' <b>'.$path[4]['name'].'</b>' ?>

                                                           </div>

                            </div>
                                                </span>






                                            </span>


                    <?php

                endforeach;

                ?>



            </div>
        </div>







    <?php
}else{ ?>

    <?php echo form_open($this->page_level.$this->page_level2.'complete_request','class="inline"') ?>

    <div class="col-md-12">

        <div class="portlet light bordered">

            <div class="portlet-body form">


                <div class="modal-footer">
                    <div class="col-md-10">
                        <label class="control-label col-md-2" style="white-space: nowrap;">Requester</label>

                        <div class="col-md-4">

                            <?php $users=$this->db->select()->from('users')->get()->result(); ?>

                            <select class="form-control" name="requester">

                                <option   value="" <?php echo set_select('requester', '', true); ?>>Select Requester</option>

                                <?php echo count($users) == 0 ? '<option value="" ' . set_select('requester', '', true) . '>There no users</option>' : '' ?>

                                <?php foreach ($users as $sp): ?>

                                    <option   value="<?php echo $sp->id ?>" <?php echo set_select('sponsor', $sp->id,$this->session->id==$sp->id?true:''); ?>><?php echo $sp->first_name.' '.$sp->last_name  ?></option>
                                <?php endforeach; ?>


                            </select>
                            <?php echo form_error('requester', '<label class="text-danger">', '</label>'); ?>
                            <label class="text-danger sponsor_label"></label>



                        </div>

                        <div class="col-md-6">
                            <label class="control-label col-md-4" style="white-space: nowrap;">Requested</label>

                            <div class="col-md-8">
                            <input readonly class="form-control" type="text" name="request_type" value="<?php echo $request_type ?>"/>
                            </div>
                        </div>


                    </div>



                    <div class="col-md-2">
                        <button type="submit" name="complete_request" class="btn blue">Complete Request</button>
                    </div>

                </div>



            </div>
        </div>
    </div>

    <div class="col-md-12">

        <div class="simplelineicons-demo">

                   <span class="item-box ">
                                                <span class="ite form-inline">

                                                   <label class="bold" style="    margin-top: 5px;
    font-size: 15px;">
<!--                                                       <span aria-hidden="true">-->Scan Barcode
                                       <input name="barcode"  type="text" maxlength="6" minlength="6"  class="form-control input-xs"  value=""/>

                                                   </label>
                                                </span>



                                            </span>

            <?php

            if(isset($ids)) {
                foreach ($ids as $id):

                    $form = $this->db->select('a.form_num,b.village')->from('forms a')->join('vht b', 'a.phone=b.phone')->where(array('a.id' => $id))->get()->row();

                    ?>
                    <?php if (count($form) == 1) {

                    $path = $this->locations->get_path($form->village);


                    ?>


                    <span class="item-box">
                                                <span class="item">

                                                   <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
<!--                                                       <span aria-hidden="true">-->
                                                           <input name="form_num[]" checked type="checkbox"
                                                                  class="group-checkable"
                                                                  value="<?php echo $form->form_num ?>"/>    <span></span>
                                                       <!--                                                       </span> -->
                                                       &nbsp;<b><?php echo $form->form_num ?>
                                                           :</b> <?php echo $path[2]['name'] . ' <b>' . $path[4]['name'] . '</b>' ?> </label>
                                                </span>



                                            </span>
                <?php }
                else { ?>

                    <span class="item-box">
                <span class="item  font-green-jungle">
                                                    <span aria-hidden="true"
                                                          class="icon-like"></span> &nbsp;(<?php echo $form->form_num ?>
                    ) <?php echo humanize($user->status); ?> </span>
   </span>
                <?php } ?>

                    <?php

                endforeach;
            }

            ?>



        </div>
    </div>

    <?php echo form_close() ?>




<?php } ?>

</div>
