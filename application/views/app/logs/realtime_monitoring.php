<?php
//print_r($this->session);

?>

    <div class="row">
        <div class="col-md-12">
<!--            --><?php //print_r($this->session) ?>
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable ">

                <div class="portlet-body">
                    <div class="table-container">
                        <div class="table-actions-wrapper">


                            <?php  echo anchor($this->page_level.$this->page_level2.'all','All','class="btn btn-default"') ?>
                            <div class="btn-group hidden">
                                <a class="btn btn-default" href="javascript:;" data-toggle="dropdown">
                                    Transactions
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <?php echo anchor($this->page_level.$this->page_level2.'transfer','Transfer') ?>
                                    </li>
                                    <li>
                                        <?php echo anchor($this->page_level.$this->page_level2.'cashout','Cashout') ?>
                                    </li>
                                    <li>
                                        <?php echo anchor($this->page_level.$this->page_level2.'hold','Hold') ?>
                                    </li>
                                    <li>
                                        <?php echo anchor($this->page_level.$this->page_level2.'canceled','Canceled') ?>
                                    </li>
                                    <li>
                                        <?php echo anchor($this->page_level.$this->page_level2.'release','Release Transaction') ?>
                                    </li>
                                    <li>
                                        <?php echo anchor($this->page_level.$this->page_level2.'approve','Approve Transaction') ?>
                                    </li>

                                </ul>
                            </div>



                            <div class="btn-group">
                                <a class="btn btn-default" href="javascript:;" data-toggle="dropdown">
                                    Account
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <?php echo anchor($this->page_level.$this->page_level2.'account_creation','Account Creation') ?>
                                    </li>
                                    <li>
                                        <?php echo anchor($this->page_level.$this->page_level2.'account_deletion','Account Deletion') ?>
                                    </li>
                                    <li>
                                        <?php echo anchor($this->page_level.$this->page_level2.'account_modification','Account Modification') ?>
                                    </li>
                                    <li>   <?php echo anchor($this->page_level.$this->page_level2.'password_change','Password Change') ?></li>

                                </ul>
                            </div>


                            <?php  echo anchor($this->page_level.$this->page_level2.'realtime_monitoring','Realtime Monitoring','class="btn btn-default"') ?>
                        </div>
                        <table class="table table-striped table-bordered table-hover table-checkable" id="realtime_monitoring">
                            <thead>
                            <tr role="row" class="heading">
                                <!--                            <th width="1%">-->
                                <!--                                <input type="checkbox" class="group-checkable"> </th>-->
                                <th width="1%"> # </th>
                                <th width="5%"> Last Login </th>
                                <th width="5%"> Username </th>
                                <th width="5%"> Ip Address </th>
                                <th width="5%"> Full Name </th>
                                <th width="10%"> email </th>
                                <th width="5%"> Browser </th>
                                <th width="2%"> Platform </th>
                            </tr>
                            <tr role="row" class="filter" hidden>

                                <!--                            <td><input type="text" class="form-control form-filter input-sm" name="title"> </td>-->
                                <td>
                                    <input type="text" class="form-control form-filter input-sm" name="title">

                                </td>

                                <td>
                                    <input type="text" class="form-control form-filter input-sm" name="content"> </td>
                                <td>
                                    <select name="sectors" class="form-control form-filter input-sm">
                                        <option value="">Select...</option>
<!--                                        --><?php //foreach ($this->db->select()->from('sectors')->get()->result() as $pb_type): ?>
<!---->
<!---->
<!--                                            <option value="--><?php //echo $pb_type->id ?><!--">--><?php //echo humanize($pb_type->sector); ?><!--</option>-->
<!--                                        --><?php //endforeach; ?>
                                    </select></td>
                                <td>
                                    <select name="publication_type" class="form-control form-filter input-sm">
                                        <option value="">Select...</option>
<!--                                        --><?php //foreach ($this->db->select()->from('publication_types')->get()->result() as $pb_type): ?>
<!---->
<!---->
<!--                                            <option value="--><?php //echo $pb_type->id ?><!--">--><?php //echo humanize($pb_type->title); ?><!--</option>-->
<!--                                        --><?php //endforeach; ?>
                                    </select>

                                <td>

                                    <div class="input-group date date-picker margin-bottom-5" data-date-format="dd-mm-yyyy">
                                        <input type="text" class="form-control form-filter input-sm" readonly name="date" placeholder="From">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-sm default" type="button">
                                                                    <i class="fa fa-calendar"></i>
                                                                </button>
                                                            </span>
                                    </div>

                                </td>
                                <td>
                                    <input type="text" class="form-control form-filter input-sm" name="author">
                                </td>
                                <td>
                                    <select name="status" class="form-control form-filter input-sm">
                                        <option value="">Select...</option>
                                        <option value="pending">Pending</option>
                                        <option value="evaluated">Evaluated</option>
                                        <option value="published">Published</option>
                                        <option value="canceled">Canceled</option>
                                    </select>
                                </td>
                                <td>
                                    <div class="margin-bottom-5">
                                        <button class="btn btn-sm green btn-outline filter-submit margin-bottom">
                                            <i class="fa fa-search"></i> Search</button>
                                    </div>
                                    <button class="hidden btn btn-sm red btn-outline filter-cancel">
                                        <i class="fa fa-times"></i> Reset</button>
                                </td>
                            </tr>
                            </thead>
                            <tbody> </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- End: life time stats -->
        </div>
    </div>

<?php
$this->load->view('ajax/realtime_monitoring');
?>