<?php


?>


<link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet" type="text/css" />

<div class="row">

    <div class="col-md-12">
<?php //print_r($id); ?>
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title hidden">
                <div class="caption font-red-sunglo">
                    <span class="caption-subject bold uppercase">Add Item</span>
                </div>
                <div class="actions">

                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body form">

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12"   id="body" >



                            <?php echo form_open()?>
                                <div class="form-body">

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">
                                           <span class="pull-right"> Phone </span>
                                        </label>
                                        <div class="col-md-10">
                                            <div class="input-group">


                                                <input type="text" class="form-control" id="phone" name="phone" value="<?php echo $phones  ?>" data-role="tagsinput">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label"><span class="pull-right">Message</span></label>
                                        <div class="col-md-10">
                                            <div class="input-group">
                                                                    <span class="input-group-addon">
                                                                        <i class="fa fa-envelope"></i>
                                                                    </span>
                                                                    <textarea name="message" max="150" required id="message" rows="5" class="form-control" >
                                                                     </textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue" id="send_message">Send Message</button>

                </div>
                <?php echo form_close(); ?>

            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>

<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js" type="text/javascript"></script>
<script>


    $("#send_message").click(function () {

        var p = $('#phone').val();
        p=p.replace(/,/g, "_")
        var m = $('#message').val();
        var mm = m.trim();

        if (p != '' && mm != '') {

            $.ajax({
                type: 'GET',
                url: '<?php echo base_url("index.php/ajax_api/bulk_sms")?>/' + p + '/' + mm,
                beforeSend: function () {
                    $("#body").html('<div class="note note-info note-shadow"><i class="fa fa-circle-o-notch fa-spin" ></i> Sending Message Please Wait....</div>');
                },
                success: function (d) {

                    $("#body").html('<div class="note note-success note-shadow"><i class="fa fa-check" ></i> Message Sent successfully</div>');


                }


            });
        }


    })
</script>
