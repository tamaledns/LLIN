<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="<?php echo base_url(); ?>assets/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/jquery-file-upload/blueimp-gallery/blueimp-gallery.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/jquery-file-upload/css/jquery.fileupload.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/jquery-file-upload/css/jquery.fileupload-ui.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<link href="<?php echo base_url(); ?>assets/apps/css/inbox.min.css" rel="stylesheet" type="text/css" />



<!--<input id="demo" class="tags-input" value="jquery,jquery plugin">-->
<!-- BEGIN PAGE BASE CONTENT -->
<div class="inbox">
<?php //print_r($this->input->post()); ?>
    <?php echo validation_errors(); ?>
    <div class="row">
        <div class="col-md-2">
            <div class="inbox-sidebar">
                <a href="javascript:;" data-title="Compose" class="btn red compose-btn btn-block"> <i class="fa fa-edit"></i> Compose </a>
<!--                <a href="javascript:;" data-title="Compose" class="btn yellow-gold compose-btn-sms btn-block">  <i class="fa fa-edit"></i> Compose SMS </a>-->
                <?php echo anchor('ajax_api/message_form','<i class="fa fa-edit"></i>  Send SMS','data-target="#ajax" data-toggle="modal" class="btn yellow-gold  btn-block"') ?>
                <ul class="inbox-nav">
                    <li class="active">
                        <a href="javascript:;" class="inbox-menu" data-type="inbox" data-title="Received Messages"> Received
                            <span class="badge badge-success  inbox-not">0</span>
                        </a>
                    </li>
<!--                    <li>-->
<!--                        <a href="javascript:;" data-type="important" data-title="Inbox"> Important </a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="javascript:;" data-type="sent" data-title="Sent"> Sent </a>-->
<!--                    </li>-->
                    <li>
                        <a href="javascript:;" class="outbox-menu" data-type="outbox" data-title="Sent Messages"> Sent </a>
                    </li>
<!--                    <li>-->
<!--                        <a href="javascript:;" data-type="draft" data-title="Draft"> Draft-->
<!--                            <span class="badge badge-danger">8</span>-->
<!--                        </a>-->
<!--                    </li>-->
                    <li class="divider"></li>
<!--                    <li>-->
<!--                        <a href="javascript:;" class="sbold uppercase" data-title="Trash"> Trash-->
<!--                            <span class="badge badge-info">23</span>-->
<!--                        </a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="javascript:;" data-type="inbox" data-title="Promotions"> Promotions-->
<!--                            <span class="badge badge-warning">2</span>-->
<!--                        </a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="javascript:;" data-type="inbox" data-title="News"> News </a>-->
<!--                    </li>-->
                </ul>
                <ul class="inbox-contacts hidden">
                    <li class="divider margin-bottom-30"></li>
                    <li>
                        <a href="javascript:;">
                            <img class="contact-pic" src="../assets/pages/media/users/avatar4.jpg">
                            <span class="contact-name">Adam Stone</span>
                            <span class="contact-status bg-green"></span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;">
                            <img class="contact-pic" src="../assets/pages/media/users/avatar2.jpg">
                            <span class="contact-name">Lisa Wong</span>
                            <span class="contact-status bg-red"></span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;">
                            <img class="contact-pic" src="../assets/pages/media/users/avatar5.jpg">
                            <span class="contact-name">Nick Strong</span>
                            <span class="contact-status bg-green"></span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;">
                            <img class="contact-pic" src="../assets/pages/media/users/avatar6.jpg">
                            <span class="contact-name">Anna Bold</span>
                            <span class="contact-status bg-yellow"></span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;">
                            <img class="contact-pic" src="../assets/pages/media/users/avatar7.jpg">
                            <span class="contact-name">Richard Nilson</span>
                            <span class="contact-status bg-green"></span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-10">
            <div class="inbox-body">
                <div class="inbox-header">
                    <h1 class="pull-left">Inbox</h1>
                    <form class="form-inline pull-right" action="index.html">
                        <div class="input-group input-medium">
                            <input type="text" class="form-control" placeholder="Search">
                                                <span class="input-group-btn">
                                                    <button type="submit" class="btn green">
                                                        <i class="fa fa-search"></i>
                                                    </button>
                                                </span>
                        </div>
                    </form>
                </div>
                <div class="inbox-content"> </div>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE BASE CONTENT -->

</div>
</div>
<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->

<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner">   <?php echo date('Y') ?> &copy;  <?php echo ucfirst($this->site_options->title('site_name')) ?>.

    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>


<!--DOC: Aplly "modal-cached" class after "modal" class to enable ajax content caching-->
<div class="modal fade" id="ajax" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <img src="<?php echo base_url() ?>/assets/global/img/loading-spinner-grey.gif" alt="" class="loading">
                <span> &nbsp;&nbsp;Loading... </span>
            </div>
        </div>
    </div>
</div>


<!-- END FOOTER -->
<!--[if lt IE 9]>
<script src="<?php echo base_url(); ?>assets/global/plugins/respond.min.js"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url(); ?>assets/global/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery-file-upload/js/vendor/tmpl.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery-file-upload/js/vendor/load-image.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery-file-upload/js/vendor/canvas-to-blob.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery-file-upload/blueimp-gallery/jquery.blueimp-gallery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery-file-upload/js/jquery.iframe-transport.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery-file-upload/js/jquery.fileupload.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery-file-upload/js/jquery.fileupload-process.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery-file-upload/js/jquery.fileupload-image.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery-file-upload/js/jquery.fileupload-audio.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery-file-upload/js/jquery.fileupload-video.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery-file-upload/js/jquery.fileupload-validate.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery-file-upload/js/jquery.fileupload-ui.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?php echo base_url(); ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="<?php echo base_url(); ?>assets/layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>

<?php $this->load->view($this->page_level.'messaging/messaging_js') ?>

<?php //$this->load->view($this->page_level.'messaging/auto-tag-complete') ?>

<?php $this->load->view('ajax/inbox_notification') ?>

<!--<script src="--><?php //echo base_url(); ?><!--assets/layouts/layout4/scripts/demo.min.js" type="text/javascript"></script>-->
<!--<script src="--><?php //echo base_url(); ?><!--assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>-->
<!-- END THEME LAYOUT SCRIPTS -->
</body>

</html>