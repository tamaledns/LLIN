

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-green-haze">
                    <i class="icon-user font-green-haze"></i>
                    <span class="caption-subject bold uppercase"> New Permission</span>
                </div>

                <div class="actions">
                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                    <!--                    --><?php //echo anchor($this->page_level.$this->page_level2,' <i class="fa fa-users"></i> Users','class="btn btn-circle btn-warning btn-sm"'); ?>
                </div>
            </div>
            <div class="portlet-body form">

                <?php echo form_open('',array('class'=>'form-horizontal')) ?>

                <div class="form-body">



                    <div class="form-group form-md-line-input">
                        <label class="col-md-2 control-label" for="form_control_1">Permission</label>
                        <div class="col-md-10">
                           <input required class="form-control" type="text" name="permission" value="<?php echo set_value('permission') ?>">
                            <label for="form_control_1"> <?php echo form_error('permission','<span style=" color:red;">','</span>') ?></label>
                            <span class="help-block">Insert Permission</span>
                        </div>

                    </div>


                    <div class="form-group form-md-line-input">
                        <label class="col-md-2 control-label" for="form_control_1">Group</label>
                        <div class="col-md-10">
                           <input required class="form-control" type="text" name="group" value="<?php echo set_value('group') ?>">
                            <label for="form_control_1"> <?php echo form_error('group','<span style=" color:red;">','</span>') ?></label>
                            <span class="help-block">Group</span>
                        </div>

                    </div>


                    <div class="form-group form-md-line-input">
                        <label class="col-md-2 control-label" for="form_control_1">Description</label>
                        <div class="col-md-10">
                            <textarea class="form-control" name="description"><?php echo set_value('description') ?></textarea>
                            <label for="form_control_1"> <?php echo form_error('description','<span style=" color:red;">','</span>') ?></label>
                            <span class="help-block">Insert Description about the Permission</span>
                        </div>

                    </div>



                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-2 col-md-10">
                            <button type="reset" class="btn default"> <i class="fa fa-remove"></i> Cancel</button>
                            <button type="submit" class="btn green-jungle"><i class="fa fa-save"></i> Save</button>
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->

    </div>


</div>