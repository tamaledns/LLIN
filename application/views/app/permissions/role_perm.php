<?php
//user type is treated as role

$role=$this->db->select()->from('user_type')->where(array('id'=>$id))->get()->row();

//print_r($role);

?>


<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-green-haze">
                    <i class="icon-user font-green-haze"></i>
                    <span class="caption-subject bold uppercase"> <?php echo $role->title ?></span><?php echo strlen($role->description)>0? '('.$role->description.')':'' ?>
                </div>

                <div class="actions">
                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                    <!--                    --><?php //echo anchor($this->page_level.$this->page_level2,' <i class="fa fa-users"></i> Users','class="btn btn-circle btn-warning btn-sm"'); ?>
                </div>
            </div>
            <div class="portlet-body form">

                <?php echo form_open('',array('class'=>'form-horizontal')) ?>


                <?php

                $no=1;

                foreach($this->db->select('perm_group')->from('permissions')->group_by('perm_group')->get()->result() as $pe): ?>

                    <div style="border-bottom: dashed thin grey;"><?php echo humanize($pe->perm_group) ?></div>
                <div class="form-body">

                    <div class="form-group" style="margin-bottom: 0px;">
                        <div class="md-checkbox-inline pull-left">
                            <?php




                            foreach($this->db->select()->from('permissions')->where(array('perm_group'=>$pe->perm_group))->get()->result() as $perm):

                              $selected_perm=$this->db->select()->from('role_perm')->where(array('role_id'=>$role->id,'perm_id'=>$perm->id))->get()->row();

                                ?>
                                <?php if(strlen($perm->title)>0) { ?>
                                    <div class="col-md-3">
                                        <div class="md-checkbox">

                                            <input type="checkbox" id="checkbox<?php echo $no ?>"
                                                   class="md-check"
                                                   name="permissions[]"
                                                   value="<?php echo $perm->id ?>" <?php echo set_checkbox('permissions[]', $perm->id,count($selected_perm)&&$perm->id==$selected_perm->perm_id?true:''); ?> />
                                            <label for="checkbox<?php echo $no ?>">
                                                <span></span>
                                                <span class="check"></span>
                                                <span
                                                    class="box"></span><i style="white-space: nowrap;"> <?php echo humanize($perm->title); ?></i>
                                            </label>
                                        </div>
                                    </div>
                                    <?php

                                    $no++;
                                }
                            endforeach; ?>




                        </div>
                        <?php echo form_error('permissions', '<label class="text-danger">', '<label>') ?>
                    </div>


                </div>

                <?php endforeach;  ?>
                <div class="form-actions">
                    <div class="row">
<!--                        col-md-offset-2-->
                        <div class="col-md-12">
                            <button type="reset" class="btn default"> <i class="fa fa-remove"></i> Cancel</button>
                            <button type="submit" class="btn green-jungle"><i class="fa fa-save"></i> Save</button>
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->

    </div>


</div>
