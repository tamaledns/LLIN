
<!-- BEGIN PAGE CONTENT-->
<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />

<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">


                <div class="caption font-dark hidden-print">


                    <?php echo form_open('','class="form-inline" ')   ?>
                    <?php echo anchor($this->page_level.$this->page_level2.'new',' <i class="fa fa-plus"></i> New Role','class="btn btn-sm green-jungle"'); ?>
                    <div class="form-group hidden">
                        <div class="">


                            <div class="input-group">
                                <select class="form-control input-sm" name="status" >
                                    <option value="" <?php echo set_select('status','',true) ?>>Select Status</option>
                                    <option value="1" <?php echo set_select('status','1') ?>>Active</option>
                                    <option value="2" <?php echo set_select('status','2') ?>>Blocked</option>

                                </select>
                            </div>
                            <button class="btn green btn-sm" type="submit"><i class="fa fa-sliders"></i> Apply</button></div>
                    </div>

                    <?php echo form_close(); ?>

                </div>
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_1">

                    <thead>
                    <tr>
                        <th width="2"></th>
                        <th style="width: 10%"> Name </th>
                        <th  style="width: 80%;">Permissions</th>


                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    $roles=$this->db->select()->from('user_type')->get()->result();

                    $no=1;
                    foreach($roles as $r): ?>
                    <tr>
                        <td><?php echo $no; ?></td>



                        <td style="white-space: nowrap;">

                            <?php echo anchor($this->page_level.$this->page_level2.'edit/'.$r->id*date('Y'),$r->title) ?>
                        </td>

                        <td >
                            <?php

                           $p=$this->db->select('a.id,b.title')->from('role_perm a')->join('permissions b','a.perm_id=b.id')->where(array('role_id'=>$r->id))->get()->result();
                            $n=0;
                            $np=count($p);
                           foreach ($p as $perms){

                               echo '<span class="label label-sm label-primary" >'.$perms->title.' ';
                              echo  anchor($this->page_level.$this->page_level2.'remove_permission/'.$perms->id*date('Y'),' <i class="fa fa-minus"></i>','class="badge badge-danger"');

                               echo '</span>';

                               $n++;
                               echo $np==$n?' ':' ';
                           }

                            ?>



                        </td>

                        <td style="width: 80px;">
                            <div class="btn-group">
                                <a class="btn green-jungle btn-sm" href="javascript:;" data-toggle="dropdown">
                                    <i class="fa fa-cogs"></i> Action <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li hidden>
                                        <?php echo anchor($this->page_level.$this->page_level2.'edit_role/'.$r->id*date('Y'),'  <i class="fa fa-eye"></i> View') ?>
                                    </li>
                                    <li>
                                        <?php echo anchor($this->page_level.$this->page_level2.'edit_role/'.$r->id*date('Y'),'  <i class="fa fa-pencil"></i> Edit') ?>
                                    </li>
                                    <li>
                                        <?php echo anchor($this->page_level.$this->page_level2.'role_perm/'.$r->id*date('Y'),'  <i class="fa fa-plus"></i> Add Permissions') ?>
                                    </li>

                                </ul>
                            </div>
                        </td>
                    </tr>
                    <?php $no++; endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->

    </div>
</div>
<!-- END PAGE BASE CONTENT -->

