<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css"/>
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php echo base_url() ?>assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->

<!--<link href="--><?php //echo base_url() ?><!--assets/admin/pages/css/tasks.css" rel="stylesheet" type="text/css"/>-->
<!-- END PAGE LEVEL STYLES -->
<?php $prof=$this->db->select()->from('users')->where('id',$this->session->userdata('id'))->get()->result();
$prof=$prof[0];
?>
<div class="row">
<div class="col-md-12">
    <!-- BEGIN PROFILE SIDEBAR -->
    <div class="profile-sidebar" style="width:250px;">
        <!-- PORTLET MAIN -->
        <div class="portlet light profile-sidebar-portlet">
            <!-- SIDEBAR USERPIC -->
            <div class="profile-userpic">
                <img src="<?php echo strlen($prof->photo)>0?base_url().$prof->photo:base_url().'assets/profile_placeholder.png' ?>" class="img-responsive" alt="">
            </div>
            <!-- END SIDEBAR USERPIC -->
            <!-- SIDEBAR USER TITLE -->
            <div class="profile-usertitle">
                <div class="profile-usertitle-name">
                   <?php echo ucwords($prof->first_name.' '.$prof->last_name); ?>
                </div>

            </div>
            <!-- END SIDEBAR USER TITLE -->


        </div>
        <!-- END PORTLET MAIN -->

    </div>
    <!-- END BEGIN PROFILE SIDEBAR -->
    <!-- BEGIN PROFILE CONTENT -->
    <div class="profile-content">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light">
                    <div class="portlet-title tabbable-line">
                        <div class="caption caption-md">
                            <i class="icon-globe theme-font hide"></i>
                            <span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
                        </div>
                        <ul class="nav nav-tabs">
                            <li class="<?php echo $subtitle=='user_profile'||$subtitle=='info'?'active':''; ?>">
                                <a href="#tab_1_1" data-toggle="tab">Personal Info</a>
                            </li>
                            <li class="<?php echo $subtitle=='change_avatar'?'active':''; ?>">
                                <a href="#tab_1_2" data-toggle="tab">Change Avatar</a>
                            </li>
                            <li <?php echo $subtitle=='change_password'?'active':''; ?>>
                                <a href="#tab_1_3" data-toggle="tab">Change Password</a>
                            </li>

                        </ul>
                    </div>
                    <div class="portlet-body">
                        <div class="tab-content">
                            <!-- PERSONAL INFO TAB -->
                            <div class="tab-pane <?php echo $subtitle=='user_profile'||$subtitle=='info'?'active':''; ?>" id="tab_1_1">
                                <?php echo form_open($this->uri->slash_segment('1').'profile/info') ?>
                                    <div class="form-group">
                                        <label class="control-label">Username </label>
                                        <input type="text" readonly placeholder="<?php echo $prof->username; ?>" class="form-control"/>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Full Names</label><?php echo form_error('fullname','<label style="color:red;">','</label>') ?>
                                        <input type="text" name="fullname" placeholder=" <?php echo ucwords($prof->first_name.' '.$prof->last_name); ?>" value="<?php echo ucwords($prof->first_name.' '.$prof->last_name); ?>" class="form-control"/>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Email </label><?php echo form_error('email','<label style="color:red;">','</label>') ?>
                                        <input type="email" name="email" placeholder="Add your email" value="<?php echo $prof->email ?>" class="form-control"/>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Phone </label> <?php echo form_error('phone','<label style="color:red;">','</label>') ?>
                                        <input type="text" name="phone" placeholder="Add Your Mobile Phone" value="<?php echo $prof->phone; ?>" class="form-control"/>
                                    </div>
                                <div class="form-group">
                                    <label class="control-label">Current Password </label> <?php echo form_error('password','<label style="color:red;">','</label>') ?>
                                    <input type="password" name="password" autocomplete="off" placeholder="What is your current password ?"  class="form-control"/>
                                </div>


                                    <div class="margiv-top-10">
                                        <button  class="btn green-haze" type="submit"> Update Changes </button>
                                        <button type="reset" class="btn default">
                                            Cancel </button>
                                    </div>
                                <?php echo form_close(); ?>
                            </div>
                            <!-- END PERSONAL INFO TAB -->
                            <!-- CHANGE AVATAR TAB -->
                            <div class="tab-pane <?php echo $subtitle=='change_avatar'?'active':''; ?>" id="tab_1_2">
                                <p>
                                    You Can Make Changes to the photo.
                                </p>
                                <?php echo form_open_multipart($this->uri->slash_segment('1').'profile/change_avatar')?>
                                    <div class="form-group">

                                        <?php if( isset($error)){?>
                                            <span class="font-red-mint" >
                                        <?php echo  $error; ?>

                                    </span>
                                        <?php } echo form_error('image','<label style="color:red;">','</label>') ?>

                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                <img src="<?php echo strlen($prof->photo)>0?base_url().$prof->photo:base_url().'assets/assets/img/avatars/placeholder.png' ?>" alt=""/>
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                                            </div>
                                            <div>
																<span class="btn default btn-file">
																<span class="fileinput-new">
																Select image </span>
																<span class="fileinput-exists">
																Change </span>
																<input type="file" name="image" required>
																</span>
                                                <a href="#" class="btn default fileinput-exists" data-dismiss="fileinput">
                                                    Remove </a>
                                            </div>
                                        </div>

                                    </div>

                                     <?php echo form_error('pa','<label style="color:red;">','</label>') ?>
                                    <input type="hidden" name="pa"  placeholder="What is your current password ?"  class="form-control"/>

                                    <div class="margin-top-10">

                                        <button type="submit" class="btn green-jungle">Upload <i class="fa fa-upload"></i></button>
                                        <a href="#" class="btn default">
                                            Cancel </a>
                                    </div>
                                <?php echo form_close() ?>
                            </div>
                            <!-- END CHANGE AVATAR TAB -->
                            <!-- CHANGE PASSWORD TAB -->
                            <div class="tab-pane <?php echo $subtitle=='change_password'?'active':''; ?>" id="tab_1_3">
                                <?php echo form_open($this->uri->slash_segment('1').'profile/change_password') ?>
                                    <div class="form-group">
                                        <label class="control-label">Current Password </label><?php echo form_error('current_password','<label style="color: red;">','</label>') ?>
                                        <input name="current_password" autocomplete="off" type="password" class="form-control"/>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">New Password </label><?php echo form_error('new_pass','<label style="color: red;">','</label>') ?>
                                        <input name="new_pass" autocomplete="off" type="password" class="form-control"/>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Re-type New Password </label><?php echo form_error('rpt_pass','<label style="color: red;">','</label>') ?>
                                        <input name="rpt_pass" autocomplete="off" type="password" class="form-control"/>
                                    </div>
                                    <div class="margin-top-10">
                                        <button type="submit" class="btn green-haze">
                                            Change Password </button>
                                        <button type="reset" class="btn default">
                                            Cancel </button>
                                    </div>
                               <?php echo form_close(); ?>
                            </div>
                            <!-- END CHANGE PASSWORD TAB -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END PROFILE CONTENT -->
</div>
<div/>

<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo base_url() ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>

<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>

<!--    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="<?php echo base_url() ?>assets/pages/scripts/profile.min.js" type="text/javascript"></script>
<!--    <!-- END PAGE LEVEL SCRIPTS -->
