<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/multiselect/css/multi-select.css">

<?php //$this->load->view($this->page_level.$this->page_level2.'select');?>



<?php
$selected_accounts='';

if(isset($s_account)) {

    if(count($s_account)){
        // print_r($data_en);
        $selected_accounts = " WHERE (";
        $got = count($s_account);
        $n = 0;

        foreach ($s_account as $d) {

            //$d=strlen($d)>0?$d:"''";

            $selected_accounts .= " smsc ='$d'";

            $n++;
            $n != $got ? $selected_accounts .= " OR" : "";
        }
        $selected_accounts .= " )";
    }

}

//print_r($selected_accounts);


?>

<?php
$srvc='';

if(isset($serve)) {

    if(count($serve)){
        // print_r($data_en);
        $srvc = "  AND (";
        $got = count($serve);
        $n = 0;

        foreach ($serve as $d) {

            //$d=strlen($d)>0?$d:"''";

            $srvc .= " service ='$d'";

            $n++;
            $n != $got ? $srvc .= " OR" : "";
        }
        $srvc .= " )";
    }

}

//print_r($srvc);


?>
<?php

$sql="SELECT  DISTINCT (smsc) as `sender` FROM `kannel_msgs`  $selected_accounts";
$rep=$this->db->query($sql)->result();
?>


<?php


//$last_day= strtotime('friday this week');

$begin = new DateTime( date('Y-m-d',$fdate) );
$end = new DateTime( date('Y-m-d',$last_day) );
//$ldate=$last_day

$interval = DateInterval::createFromDateString('1 day');
$period = new DatePeriod($begin, $interval, $end->modify( '+1 day' ));

//print_r($period);

?>


<div class="row">


    <div class="col-md-12">
        <!-- BEGIN Portlet PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">

                <div class="caption  font-dark">
                    <!--                    Range From --><?php //echo date('d-m-Y',$fdate). ' to '. date('d-m-Y',$last_day) ?>
                    <?php // echo anchor($this->page_level.$this->page_level2.'new',' <i class="fa fa-plus"></i> New','class="btn green-jungle"'); ?>


                    <!--                    <form class="form-inline" role="form">-->
                    </form>
                    <?php echo form_open($this->page_level.$this->page_level2.'message_logs', 'class="form-inline hidden-print"') ?>

                    <div class="form-group">

                        <div class="input-group input-large date-picker input-daterange" data-date="<?php echo date('Y-m-d') ?>" data-date-format="yyyy-mm-dd">
                            <span class="input-group-addon">From </span>
                            <input type="text" class="form-control" name="from" placeholder="<?php echo date('Y-m-d',$fdate) ?>" value="<?php echo set_value('from') ?>">
                            <span class="input-group-addon">to </span>
                            <input type="text" class="form-control" name="to" placeholder="<?php echo date('Y-m-d',$last_day)?>" value="<?php echo set_value('to') ?>">
                        </div>

                        <div class="modal fade" id="accounts" tabindex="-1" role="basic" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                        <h4 class="modal-title sbold red">Select Senders</h4>
                                    </div>
                                    <div class="modal-body">



                                        <?php

                                        foreach( $this->db->select('distinct(smsc) as sender')->from('kannel_msgs')->get()->result() as $t){
                                            $options[$t->sender] = $t->sender;
                                        }

                                        $attr=array(
                                            'class'=>'pre-selected-options'
                                        );
                                        //                        id="pre-selected-options"
                                        echo form_multiselect('selected_account[]', $options,'',$attr);
                                        ?>

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Select</button>
                                        <!--                                        <button type="button" class="btn green">Save changes</button>-->
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>

                        <div class="modal fade" id="services" tabindex="-1" role="basic" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                        <h4 class="modal-title sbold red">Select Services</h4>
                                    </div>
                                    <div class="modal-body">



                                        <?php

                                        foreach( $this->db->select('service')->from('services')->get()->result() as $t){
                                            $option[$t->service] = humanize($t->service);
                                        }

                                        $attr=array(
                                            'class'=>'pre-selected-options'
                                        );
                                        //                        id="pre-selected-options"
                                        echo form_multiselect('services[]', $option,'',$attr);
                                        ?>

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Select</button>
                                        <!--                                        <button type="button" class="btn green">Save changes</button>-->
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>

                        <select  class="hidden table-group-action-input form-control input-medium" name="month">
                            <option value="" <?php echo set_select('month', ''); ?>>Select month...</option>
                            <?php for($i=1;$i<=12;$i++){ ?>
                                <option value="<?php echo $i ?>" <?php echo set_select('month', $i,date('n')==$i?true:''); ?>><?php echo monthName($i-1); ?></option>
                            <?php } ?>

                        </select>

                    </div>
                    <a class="btn yellow-gold btn-outline sbold" data-toggle="modal" href="#accounts"><i class="fa fa-users"></i> Accounts </a>
                    <a class="btn yellow-gold btn-outline sbold" data-toggle="modal" href="#services"><i class="fa fa-cubes"></i> Services </a>

                    <button type="reset" class="btn red"><i class="fa fa-refresh"></i> Reset</button>
                    <button type="submit" class="btn green"><i class="fa fa-sliders"></i> Filter</button>
                    <?php echo anchor($this->page_level.$this->page_level2.'upload_logs','<i class="fa fa-upload"></i> Upload Logs','class="btn btn-info"') ?>



                    <?php echo form_close(); ?>





                    <!--                    --><?php //echo anchor($this->page_level.$this->page_level2.'update_accounts','Update Account','class="blue tooltips"'); ?>
                </div>


            </div>
            <div class="portlet-body portlet-empty">
                <?php if($this->input->post()){ ?>
                    <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1" >

                        <thead>
                        <tr>
                            <th width="2">#</th>
                            <th width="2">Account</th>


                            <?php
                            $networks=Networks('UG');
                            // print_r($networks);

                            foreach($networks as $u){ ?>
                                <th> <?php echo strtoupper(humanize($u['title'])); ?> </th>
                            <?php } ?>

                            <th>Total</th>

                        </tr>
                        </thead>
                        <tbody>


                        <?php
                        $no=1;
                        foreach($rep as $r): ?>

                            <tr>
                                <td><?php echo $no; $no++; ?></td>
                                <td><?php echo  strlen($r->sender)>0?strtoupper($r->sender):'Not Defined'; ?></td>

                                <?php
                                $sub_total=0;
                                foreach($networks as $u){ ?>

                                    <?php

                                    $receiver=$u['code'];
                                    $sqql="SELECT COUNT(*) AS `numrows` FROM `kannel_msgs` `a` WHERE `smsc` = '$r->sender' $srvc AND `message_time` >= $fdate AND `message_time` <= $last_day AND `receiver` LIKE '%$receiver%'  ESCAPE '!'";

                                    $sm=$this->db->query($sqql)->row();

                                    $smses=$sm->numrows;

                                    ?>

                                    <td> <?php echo number_format($smses);

                                        $sub_total=$sub_total+$smses;
                                        ?> </td>
                                <?php } ?>
                                <th><?php echo number_format($sub_total); ?></th>
                            </tr>
                        <?php  endforeach; ?>

                        </tbody>

                    </table>

                <?php }else{
                    $data=array(
                        'alert'=>'info',
                        'message'=>'Filter to display data',
                        'hide'=>1
                    );
                    $this->load->view('alert',$data);

                } ?>
            </div>
        </div>
        <!-- END Portlet PORTLET-->
    </div>




</div>



<!--<script src="--><?php //echo base_url() ?><!--assets/global/plugins/jquery.min.js" type="text/javascript"></script>-->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<!--<script src="--><?php //echo base_url() ?><!--assets/global/scripts/app.min.js" type="text/javascript"></script>-->
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<!--<script src="--><?php //echo base_url() ?><!--assets/pages/scripts/portlet-ajax.js" type="text/javascript"></script>-->
<!-- END PAGE LEVEL SCRIPTS -->



<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo base_url() ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?php echo base_url() ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url() ?>assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<!--<script src="--><?php //echo base_url() ?><!--assets/pages/scripts/table-datatables-fixedheader.min.js" type="text/javascript"></script>-->

<script src="<?php echo base_url() ?>assets/multiselect/js/jquery.multi-select.js"></script>
<script type="text/javascript">
    // run pre selected options
    $('.pre-selected-options').multiSelect();

</script>

<!--<script src="--><?php //echo base_url() ?><!--assets/pages/scripts/ui-modals.min.js" type="text/javascript"></script>-->
<!-- END PAGE LEVEL SCRIPTS -->






