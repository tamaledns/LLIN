

<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php echo base_url() ?>assets/apps/css/todo-2.min.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->


<div class="row">
    <div class="col-md-12">
        <!-- BEGIN TODO SIDEBAR -->
        <div class="todo-ui">
            <div class="todo-sidebar">
                <div class="portlet light ">
                    <div class="portlet-title">
                        <div class="caption" data-toggle="collapse" data-target=".todo-project-list-content">
                            <span class="caption-subject font-green-sharp bold uppercase"><i class="fa fa-filter"></i> Filter </span>
                            <span class="caption-helper visible-sm-inline-block visible-xs-inline-block">click to view project list</span>
                        </div>
                        <div class="actions">
                            <div class="btn-group">
                                <a class="btn green btn-circle btn-outline btn-sm todo-projects-config" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <i class="icon-settings"></i> &nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a href="javascript:;"> New Project </a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="javascript:;"> Pending
                                            <span class="badge badge-danger"> 4 </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;"> Completed
                                            <span class="badge badge-success"> 12 </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;"> Overdue
                                            <span class="badge badge-warning"> 9 </span>
                                        </a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="javascript:;"> Archived Projects </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body todo-project-list-content">
                        <div class="todo-project-list">
                            <ul class="nav nav-stacked">
                                <li>
                                    <?php

                                    echo form_open();?>

                                    <div class="input-group">
                                    <input type="text" class="form-control" name="search" value="<?php echo set_value('search') ?>" placeholder="Search...">
                                        <span class="input-group-btn">
                                            <button class="btn blue" type="submit">Go!</button>

                                        </span>
                                    </div>

                                   <?php echo form_close();

                                    ?>
                                </li>
                                <?php ?>
                                <?php foreach($this->db->select('')->from('sectors')->get()->result() as $sc): ?>
                                <li  class="<?php echo $sc->id==$id?'active':'' ?>">
                                    <?php
                                   $pubs_count= $this->db->where('sector',$sc->id)->from('publications')->count_all_results();
                                    ?>
                                    <?php echo anchor($this->page_level.$this->page_level2.'sector/'.$sc->id*date('Y'),'<span class="badge badge-success"> '.$pubs_count.' </span>'.$sc->sector) ?>
                                </li>
                                <?php endforeach; ?>

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="portlet light hidden">
                    <div class="portlet-title">
                        <div class="caption" data-toggle="collapse" data-target=".todo-project-list-content-tags">
                            <span class="caption-subject font-red bold uppercase">TAGS </span>
                            <span class="caption-helper visible-sm-inline-block visible-xs-inline-block">click to view</span>
                        </div>
                        <div class="actions">
                            <div class="actions">
                                <a class="btn btn-circle grey-salsa btn-outline btn-sm" href="javascript:;">
                                    <i class="fa fa-plus"></i> Add </a>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body todo-project-list-content todo-project-list-content-tags">
                        <div class="todo-project-list">
                            <ul class="nav nav-pills nav-stacked">
                                <li>
                                    <a href="javascript:;">
                                        <span class="badge badge-danger"> 6 </span> Pending </a>
                                </li>
                                <li>
                                    <a href="javascript:;">
                                        <span class="badge badge-info"> 2 </span> Completed </a>
                                </li>
                                <li>
                                    <a href="javascript:;">
                                        <span class="badge badge-success"> 14 </span> In Progress </a>
                                </li>
                                <li>
                                    <a href="javascript:;">
                                        <span class="badge badge-warning"> 6 </span> Closed </a>
                                </li>
                                <li>
                                    <a href="javascript:;">
                                        <span class="badge badge-info"> 2 </span> Delivered </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END TODO SIDEBAR -->
            <!-- BEGIN TODO CONTENT -->
            <div class="todo-content">
                <div class="portlet  ">
                    <!-- PROJECT HEAD -->
                    <div class="portlet-title hidden">
                        <div class="caption">
                            <i class="icon-bar-chart font-green-sharp hide"></i>
                            <span class="caption-helper">GlobalEx Tasks:</span> &nbsp;
                            <span class="caption-subject font-green-sharp bold uppercase">Tune Website</span>
                        </div>
                        <div class="actions">
                            <div class="btn-group">
                                <a class="btn green btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> MANAGE
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a href="javascript:;"> New Task </a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="javascript:;"> Pending
                                            <span class="badge badge-danger"> 4 </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;"> Completed
                                            <span class="badge badge-success"> 12 </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;"> Overdue
                                            <span class="badge badge-warning"> 9 </span>
                                        </a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="javascript:;"> Delete Project </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- end PROJECT HEAD -->
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-4">


                                <div class="todo-tasklist">

                                    <?php

                                    $data=array(
                                        'alert'=>'info',
                                        'message'=>'No Publications found...',
                                        'hide'=>1

                                    );
                                    count($publications)==0?$this->load->view('alert',$data):'';

                                    foreach($publications as $pub):
                                    ?>

                                    <div class="todo-tasklist-item todo-tasklist-item-border-red">
                                        <img class="todo-userpi pull-left widget-news-left-elem" src="<?php echo base_url('assets/download-placeholder.png') ?>" width="120px" height="130px">

                                        <div class="todo-tasklist-controls pul-left">

                                                                <span class="todo-tasklist-date">  <b>Sector :</b> <?php echo $pub->sector_name ?>  </span>
                                                                <span class="todo-tasklist-date">  <i class="fa fa-calendar"></i> <?php echo date('d M Y',$pub->publication_date) ?></span>
                                            <span class="todo-tasklist-date">  Author <i class="fa fa-user"></i> <?php echo humanize($pub->author) ?></span>


                                            <?php
                                            echo   strlen($pub->attachment)>0?('<a href="'.base_url($pub->attachment).'" target="_blank" class="btn btn-sm btn-info pull-right"><i class="fa fa-download"></i> Download</a>'):'';
                                            ?>




                                        </div>


                                        <a  href="<?php echo base_url('index.php/'.$this->page_level.$this->page_level2.'view_publication/'.$pub->id*date('Y')) ?>" class="todo-tasklist-item-title" style="font-size: x-large; text-decoration: none;"> <?php $title= ucfirst($pub->title) ?>

                                            <?php  ?>
                                            <?php echo highlight_phrase($title, $this->input->post('search'), '<span style="color:#990000;background-color: #FBFF00;">', '</span>'); ?>

                                        </a>
                                        <div class="todo-tasklist-item-text">
                                            <?php  $content=ucfirst($pub->content) ?>
                                            <?php echo highlight_phrase($content, $this->input->post('search'), '<span style="color:#990000;background-color: #FBFF00;">', '</span>'); ?>
                                        <a href="<?php echo base_url('index.php/'.$this->page_level.$this->page_level2.'view_publication/'.$pub->id*date('Y')) ?>" class="btn btn-sm btn-warning pull-right"><i class="icon-arrow-right"></i> Read More</a>
                                        </div>

                                    </div>
                                    <?php

                                    endforeach;

                                    ?>

                                </div>


                                <?php $this->load->view('pag'); ?>

                                <div class="search-pagination pagination-rounded pull-right">
                                    <ul class="pagination">
                                        <?php //echo $pg; ?>
                                    </ul>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
            <!-- END TODO CONTENT -->
        </div>
    </div>
    <!-- END PAGE CONTENT-->
</div>