<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-users font-dark"></i>
                    <span class="caption-subject bold uppercase">Selected Countries&nbsp;</span>
                </div>
                <div class="actions pull-left">

            <?php echo anchor($this->page_level.$this->page_level2.'new_country',' <i class="fa fa-plus"></i> New Country','class="btn green-jungle btn-sm"'); ?>
                </div>
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_1">

                    <thead>
                    <tr>
                        <th width="5">#</th>
                        <th> Country </th>
                        <th> Country Code </th>
                        <th> Zip Code </th>
                        <th width="70"> Status </th>
                        <th width="70">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no=1;
                    foreach($this->db->select('country, a2_iso, a3_un, num_un, dialing_code,status')->from('selected_countries')->get()->result() as $c): ?>
                        <tr>
                            <td><?php echo $no; ?></td>
                            <td> <?php echo $c->country ?> </td>
                            <td> <?php echo $c->a2_iso ?> </td>
                            <td> <?php echo $c->dialing_code ?> </td>
                            <td><?php echo $c->status==1?'<div class="btn btn-sm green-jungle" style="width: 70px;">active</div>':'<div class="btn btn-sm btn-danger" style="width: 70px;">Blocked</div>' ?></td>
                            <td><div class="btn-group">
                                    <a class="btn green-jungle btn-sm" href="javascript:;" data-toggle="dropdown">
                                        <i class="fa fa-cogs"></i> Action <i class="fa fa-angle-down"></i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li>

                                            <?php echo anchor($this->page_level.$this->page_level2.'delete_country/'.$c->a2_iso,'  <i class="fa fa-trash-o"></i> Delete','onclick="return confirm(\'Are you sure you want to delete ?\')"') ?>
                                        </li>
                                        <li>

                                            <?php echo $c->status==0? anchor($this->page_level.$this->page_level2.'unblock_country/'.$c->a2_iso,'  <i class="fa fa-check"></i> Unblock'): anchor($this->page_level.$this->page_level2.'ban_country/'.$c->a2_iso,'  <i class="fa fa-ban"></i> Block' ,'onclick="return confirm(\'You are about to ban Country from accessing the System \')"') ?>
                                        </li>

                                    </ul>
                                </div></td>
                        </tr>
                    <?php
                    $no++;
                    endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->

    </div>
</div>
<!-- END PAGE BASE CONTENT -->