<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-globe font-dark"></i>
                    <span class="caption-subject bold uppercase">Publication Type&nbsp; </span>
                </div>
                <div class="actions pull-left">

            <?php echo anchor($this->page_level.$this->page_level2.'new_resource_type',' <i class="fa fa-plus"></i> New Resource Type','class="btn green-jungle btn-sm"'); ?>
                </div>
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_1">

                    <thead>
                    <tr>
                        <th width="5">#</th>
                        <th> Title</th>
                        <th width="70">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no=1;
                    foreach($this->db->select()->from('resource_types')->get()->result() as $c): ?>
                        <tr>
                            <td><?php echo $no; ?></td>
                            <td> <?php echo $c->title ?> </td>


                            <td><div class="btn-group">
                                    <a class="btn green-jungle btn-sm" href="javascript:;" data-toggle="dropdown">
                                        <i class="fa fa-cogs"></i> Action <i class="fa fa-angle-down"></i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">

                                        <li>

                                            <?php echo anchor($this->page_level.$this->page_level2.'edit_resource_type/'.$c->id*date('Y'),' <i class="fa fa-edit"></i> Edit Resource Type'); ?>
                                        </li>
                                        <li>

                                            <?php echo anchor($this->page_level.$this->page_level2.'delete_resource_type/'.$c->id*date('Y'),' <i class="fa fa-trash"></i> Delete','onclick="return confirm(\'Are sure want to delete this item ?\')"'); ?>
                                        </li>

                                    </ul>
                                </div></td>
                        </tr>
                    <?php
                    $no++;
                    endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->

    </div>
</div>
<!-- END PAGE BASE CONTENT -->