<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css"/>

<link href="<?php echo base_url() ?>assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
<!--<link href="--><?php //echo base_url() ?><!--assets/admin/pages/css/profile.css" rel="stylesheet" type="text/css"/>-->
<link href="<?php echo base_url() ?>assets/admin/pages/css/tasks.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->
<?php $data['prof']=$prof=$this->db->select()->from('users')->where('id',$id)->get()->row();

?>
<div class="col-md-12">
    <!-- BEGIN PROFILE SIDEBAR -->
    <div class="profile-sidebar" style="width:250px;">
        <!-- PORTLET MAIN -->
        <div class="portlet light profile-sidebar-portlet">
            <!-- SIDEBAR USERPIC -->
            <div class="profile-userpic">
                <img src="<?php echo strlen($prof->photo)>0?base_url().$prof->photo:base_url().'assets/profile_placeholder.png' ?>" class="img-responsive" alt="">
            </div>
            <!-- END SIDEBAR USERPIC -->
            <!-- SIDEBAR USER TITLE -->
            <div class="profile-usertitle">
                <div class="profile-usertitle-name">
                   <?php echo ucwords($prof->first_name.' '.$prof->last_name); ?>
                </div>

            </div>
            <!-- END SIDEBAR USER TITLE -->


        </div>
        <!-- END PORTLET MAIN -->

    </div>
    <!-- END BEGIN PROFILE SIDEBAR -->
    <!-- BEGIN PROFILE CONTENT -->
    <div class="profile-content">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light">
                    <div class="portlet-title tabbable-line">
                        <div class="caption caption-md">
                            <i class="icon-globe theme-font hide"></i>
                            <span class="caption-subject font-blue-madison bold uppercase"> <?php echo $prof->first_name.' '.$prof->last_name; ?> Profile Account &nbsp;</span>
                        </div>
                        <div class="actions pull-left">

                            <?php echo anchor($this->page_level.$this->page_level2.'new',' <i class="fa fa-plus"></i> New','class="btn green-jungle btn-sm"'); ?>
                            <?php echo anchor($this->page_level.$this->page_level2,' <i class="fa fa-users"></i> Users','class="btn btn-warning btn-sm"'); ?>
                        </div>
                        <ul class="nav nav-tabs">
                            <li class="<?php echo $subtitle=='edit'||$subtitle=='info'?'active':''; ?>">
                                <a href="#tab_1_1" data-toggle="tab">Personal Info</a>
                            </li>
                            <li class="<?php echo $subtitle=='change_avatar'?'active':''; ?>">
                                <a href="#tab_1_2" data-toggle="tab">Change Avatar</a>
                            </li>
                            <li <?php echo $subtitle=='change_password'?'active':''; ?>>
                                <a href="#tab_1_3" data-toggle="tab">Change Password</a>
                            </li>
<!--                            --><?php //if($prof->user_type==4){ ?>
<!--                            <li --><?php //echo $subtitle=='documents'?'active':''; ?><!-->
<!--                                <a href="#documents" data-toggle="tab">Documents</a>-->
<!--                            </li>-->
<!--                            --><?php //} ?>

                        </ul>
                    </div>
                    <div class="portlet-body">
                        <div class="tab-content">
                            <!-- PERSONAL INFO TAB -->

                            <div class="tab-pane <?php echo $subtitle=='edit'||$subtitle=='info'?'active':''; ?>" id="tab_1_1">


                                <?php echo form_open($this->page_level.$this->page_level2.'edit/'.$prof->id*date('Y')) ?>
                                    <div class="form-group">
                                        <label class="control-label">Username </label>
                                        <input type="text" readonly placeholder="<?php echo $prof->username; ?>" class="form-control"/>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">First Name</label><?php echo form_error('first_name','<label style="color:red;">','</label>') ?>
                                        <input type="text" name="first_name" placeholder=" <?php echo ucwords($prof->first_name); ?>" value="<?php echo ucwords($prof->first_name); ?>" class="form-control"/>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Last Name</label><?php echo form_error('last_name','<label style="color:red;">','</label>') ?>
                                        <input type="text" name="last_name" placeholder=" <?php echo ucwords($prof->last_name); ?>" value="<?php echo ucwords($prof->last_name); ?>" class="form-control"/>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Email </label><?php echo form_error('email','<label style="color:red;">','</label>') ?>
                                        <input type="email" name="email" placeholder="Add your email" value="<?php echo $prof->email ?>" class="form-control"/>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Phone </label> <?php echo form_error('phone','<label style="color:red;">','</label>') ?>
                                        <input type="text" name="phone" placeholder="Add Your Mobile Phone" value="<?php echo $prof->phone; ?>" class="form-control"/>
                                    </div>

<!--                                This is the for the branch Select-->

                                <div class="form-group">
                                    <label class="control-label">Role </label> <?php echo form_error('role','<label style="color:red;">','</label>') ?>
                                    <div class="form-group">
                                        <?php $rol=$this->db->select('title')->from('user_type')->where('id',$prof->user_type)->get()->row(); ?>
                                        <select class="form-control" name="role">
                                            <option value="<?php echo $prof->user_type ?>" <?php echo set_select('role', $prof->user_type, TRUE); ?> ><?php echo  $rol->title ?></option>
                                            <?php foreach($this->db->select('id,title')->from('user_type')->order_by('id','asc')->get()->result() as $role): ?>
                                                <option value="<?php echo $role->id ?>" <?php echo set_select('role', $role->id); ?> ><?php echo $role->title ?></option>
                                            <?php  endforeach; ?>
                                        </select>
                                    </div>
                                </div>



                                    <div class="margiv-top-10">
                                        <hr/>
                                        <button  class="btn green-haze" type="submit"> Update Changes </button>
                                        <button type="reset" class="btn default">
                                            Cancel </button>
                                    </div>

                                <?php echo form_close(); ?>

                            </div>

                            <!-- END PERSONAL INFO TAB -->
                            <!-- CHANGE AVATAR TAB -->
                            <div class="tab-pane <?php echo $subtitle=='change_avatar'?'active':''; ?>" id="tab_1_2">
                                <p>
                                    You Can Make Changes to the photo.
                                </p>
                                <?php echo form_open_multipart($this->page_level.$this->page_level2.'change_avatar/'.$prof->id*date('Y'))?>
                                    <div class="form-group">

                                        <?php if( isset($error)){?>
                                            <span class="font-red-mint" >
                                        <?php echo  $error; ?>

                                    </span>
                                        <?php } echo form_error('image','<label style="color:red;">','</label>') ?>

                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                <img src="<?php echo strlen($prof->photo)>0?base_url().$prof->photo:base_url().'assets/assets/img/avatars/placeholder.png' ?>" alt=""/>
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                                            </div>
                                            <div>
																<span class="btn default btn-file">
																<span class="fileinput-new">
																Select image </span>
																<span class="fileinput-exists">
																Change </span>
																<input type="file" name="image" required>
																</span>
                                                <a href="#" class="btn default fileinput-exists" data-dismiss="fileinput">
                                                    Remove </a>
                                            </div>
                                        </div>

                                    </div>

                                     <?php echo form_error('pa','<label style="color:red;">','</label>') ?>
                                    <input type="hidden" name="pa"  placeholder="What is your current password ?"  class="form-control"/>

                                    <div class="margin-top-10">

                                        <button type="submit" class="btn green-jungle">Upload <i class="fa fa-upload"></i></button>
                                        <a href="#" class="btn default">
                                            Cancel </a>
                                    </div>
                                <?php echo form_close() ?>
                            </div>
                            <!-- END CHANGE AVATAR TAB -->
                            <!-- CHANGE PASSWORD TAB -->
                            <div class="tab-pane <?php echo $subtitle=='change_password'?'active':''; ?>" id="tab_1_3">
                                <?php echo form_open($this->page_level.$this->page_level2.'change_password/'.$prof->id*date('Y')) ?>
                                    <div class="form-group hidden">
                                        <label class="control-label">Current Password </label><?php echo form_error('current_password','<label style="color: red;">','</label>') ?>
                                        <input name="current_password" autocomplete="off" type="password" class="form-control"/>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">New Password </label><?php echo form_error('new_pass','<label style="color: red;">','</label>') ?>
                                        <input name="new_pass" autocomplete="off" type="password" class="form-control"/>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Re-type New Password </label><?php echo form_error('rpt_pass','<label style="color: red;">','</label>') ?>
                                        <input name="rpt_pass" autocomplete="off" type="password" class="form-control"/>
                                    </div>
                                    <div class="margin-top-10">
                                        <button type="submit" class="btn green-haze">
                                            Change Password </button>
                                        <button type="reset" class="btn default">
                                            Cancel </button>
                                    </div>
                               <?php echo form_close(); ?>
                            </div>
                            <!-- END CHANGE PASSWORD TAB -->
                            <!-- Documents TAB -->
<!--                            --><?php //if($prof->user_type==4){ ?>
<!--                            <div class="tab-pane --><?php //echo $subtitle=='documents'?'active':''; ?><!--" id="documents">-->
<!---->
<!--                                --><?php //$this->load->view($this->page_level.$this->page_level2.'assigned_documents',$data); ?>
<!---->
<!--                            </div>-->
<!--                            --><?php //} ?>
                            <!-- END Documents TAB -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END PROFILE CONTENT -->
</div>

<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo base_url() ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>

<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
