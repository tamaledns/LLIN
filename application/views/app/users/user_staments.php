<!-- BEGIN PAGE CONTENT-->
<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />

<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">


                <div class="caption font-dark hidden-print">
                    User Wallets
                    <?php //echo anchor($this->page_level.$this->page_level2.'new',' <i class="fa fa-plus"></i> New','class="btn green-jungle"'); ?>



                </div>
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_1">

                    <thead>
                    <tr>
                        <th width="2">#</th>
                        <th width="70">Image</th>
                        <th> Name </th>

                        <th> Balance </th>
                        <th width="70">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php


                    $no=1;

                    foreach($t as $w): ?>
                        <tr>
                            <td><?php echo $no; ?></td>
                            <td>
                                <?php
                                $pd=$this->db->select('full_name,photo')->from('users')->where('id',$w->user)->get()->row();
                                echo img(array('src'=>$pd->photo,'height'=>'32')); ?>
                            </td>
                            <td> <?php echo ucwords($pd->full_name); ?> </td>

                            <td> <?php echo number_format($w->current_amount,2) ?> </td>
                            <td><div class="btn-group">
                                    <a class="btn green-jungle btn-sm" href="javascript:;" data-toggle="dropdown">
                                        <i class="fa fa-cogs"></i> Action <i class="fa fa-angle-down"></i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li>
                                            <?php echo anchor($this->page_level.$this->page_level2.'topup/'.$w->id*date('Y'),'  <i class="fa fa-upload"></i> Topup') ?>
                                        </li>
                                        <li>
                                            <?php echo anchor($this->page_level.$this->page_level2.'user_statement/'.$w->id*date('Y'),'  <i class="icon-list"></i> User Statements') ?>
                                        </li>

                                    </ul>
                                </div></td>
                        </tr>
                        <?php $no++; endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->

    </div>
</div>
<!-- END PAGE BASE CONTENT -->

