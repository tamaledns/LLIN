<?php

//this is the default qry to be loaded
$this->db->select('id,first_name,last_name, city, country, phone,photo,  gender,dob,user_type,status')->from('users')
    ->where(array('id !=' => $this->session->userdata('id')));
if ($this->uri->segment(4) == 'blocked') {
    $this->db->where(array('status' => 2));
}

isset($user_role)?$this->db->where(array('user_type' => $user_role)):'';

isset($status)?($status==1?$this->db->where(array('status' => 0))->or_where(array('status' => 1)):$this->db->where(array('status' => $status))):'';

$t = $this->db->order_by('id', 'desc')->get()->result();
// this is the end of query loaded

?>



<!-- BEGIN PAGE CONTENT-->
<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />

<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">


                <div class="caption font-dark hidden-print">


                    <?php echo form_open('','class="form-inline" ')   ?>
                    <?php echo anchor($this->page_level.$this->page_level2.'new',' <i class="fa fa-plus"></i> New user','class="btn btn-sm green-jungle"'); ?>
                    <div class="form-group">
                        <div class="">

                            <div class="input-group">
                                <select class="form-control input-sm" name="user_role" >
                                    <option value="" <?php echo set_select('user_role','',true) ?>>User Role</option>
                                    <?php foreach( $this->db->select()->from('user_type')->get()->result() as $u): ?>
                                        <option value="<?php echo $u->id  ?>" <?php echo set_select('user_role',$u->id) ?>><?php echo ucwords($u->title) ?></option>

                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="input-group">
                                <select class="form-control input-sm" name="status" >
                                    <option value="" <?php echo set_select('status','',true) ?>>Select Status</option>
                                    <option value="1" <?php echo set_select('status','1') ?>>Active</option>
                                    <option value="2" <?php echo set_select('status','2') ?>>Blocked</option>

                                </select>
                            </div>
                            <button class="btn green btn-sm" type="submit"><i class="fa fa-sliders"></i> Apply</button></div>
                    </div>

                    <?php echo form_close(); ?>

                </div>

                <div class="tools <?php echo $this->custom_library->role_exist('Export User List')?'':'hidden' ?>">

                    <?php echo form_open(); ?>

                    <select name="export"  class="btn red btn-sm  btn-outline dropdown-toggle"  onchange="this.form.submit()">
                        <option value="">Export</option>
                        <option value="export_pdf">PDF</option>
                        <option value="excel">Excel</option>

                    </select>
                    <?php echo form_close() ?>
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_2">

                    <thead>
                    <tr>
                        <th width="2">#</th>
<!--                        <th>Image</th>-->

                        <th> Name </th>

                        <th> Country </th>
<!--                        <th> City </th>-->

                        <th> Phone </th>

                        <th> Role </th>
                        <th style="width: 8px;">Status</th>
                        <th><span class="hidden-print">Action</span></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php


                    $no=1;
                    foreach($t as $user): ?>
                    <tr>
                        <td><?php echo $no; ?></td>
<!--                        <td>-->
<!--                            --><?php //echo img(array('src'=>$user->photo,'height'=>'32')); ?>
<!--                        </td>-->

                        <td>

                            <?php echo anchor($this->page_level.$this->page_level2.'edit/'.$user->id*date('Y'),strlen($user->first_name)>0?ucwords($user->first_name.' '.$user->last_name):'<span style="color: red;">Registration not Completed</span>') ?>
                        </td>
                        <?php  $ct= $this->db->select('country as title')->from('country')->where('a2_iso',$user->country)->get()->row(); ?>
                        <td title="<?php echo isset($ct->title)?$ct->title:''; ?>"> <?php

                            echo isset($ct->title)?character_limiter($ct->title,10):''; ?> </td>
<!--                        <td> --><?php
//                            $st= $this->db->select('title')->from('state')->where('state',$user->city)->get()->row();
//                            echo isset($st->title)?$st->title:'N/A' ?><!-- </td>-->

                        <td> <?php echo $user->phone ?> </td>

                        <td> <?php
                           $ut= $this->db->select('title')->from('user_type')->where('id',$user->user_type)->get()->row();
                            echo $ut->title ?> </td>
                        <td>
                            <?php $status=$user->status ?>

                            <div class="btn btn-sm btn-<?php echo $status=='2'?'danger':'success';  ?>"><?php echo $status=='2'?'Blocked':'Active';  ?></div>
                        </td>
                        <td class="hidden-print" style="width: 80px;">
                            <div class="btn-group hidden-print">
                                <a class="btn green-jungle btn-sm" href="javascript:;" data-toggle="dropdown">
                                    <i class="fa fa-cogs"></i> Action <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <?php echo anchor($this->page_level.$this->page_level2.'edit/'.$user->id*date('Y'),'  <i class="fa fa-eye"></i> View') ?>
                                    </li>
                                    <li>
                                        <?php echo anchor($this->page_level.$this->page_level2.'edit/'.$user->id*date('Y'),'  <i class="fa fa-pencil"></i> Edit') ?>
                                    </li>
                                    <?php if($this->session->userdata('user_type')!='2'){ ?>
                                    <li >

                                        <?php echo anchor($this->page_level.$this->page_level2.'delete/'.$user->id*date('Y'),'  <i class="fa fa-trash-o"></i> Delete','onclick="return confirm(\'Are you sure you want to delete ?\')"') ?>
                                    </li>
                                    <li>

                                        <?php echo $user->status==2? anchor($this->page_level.$this->page_level2.'unblock/'.$user->id*date('Y'),'  <i class="fa fa-check"></i> Unblock'): anchor($this->page_level.$this->page_level2.'ban/'.$user->id*date('Y'),'  <i class="fa fa-ban"></i> Block' ,'onclick="return confirm(\'You are about to ban User from accessing the System \')"') ?>
                                    </li>
                                    <li class="divider">
                                    </li>

                                    <?php } ?>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    <?php $no++; endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->

    </div>
</div>
<!-- END PAGE BASE CONTENT -->
<script src="<?php echo base_url() ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script>
    $('#sample_2').DataTable( {
        buttons: [
            {
                extend: 'copy',
                text: 'Copy to clipboard'
            },
            'excel',
            'pdf'
        ]
    } );
</script>

