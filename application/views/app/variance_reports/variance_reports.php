
<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />


<div class="row">
    <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light portlet-fit portlet-datatable ">
            <div class="portlet-title hidden " style="    padding: 0px 20px 0px;">


                <div class="caption font-dark" style="    padding: 19px 0;">
                    <i class="icon-home font-dark"></i>
                    <span class="caption-subject bold uppercase"><?php echo humanize($subtitle) ?></span>
                    <?php //echo anchor($this->page_level.$this->page_level2.'new',' <i class="fa fa-plus"></i> Add new','class="btn btn-sm green-jungle"'); ?>
                </div>


                <div class="tools hidden">
                    <div class="dt-button buttons-print btn dark btn-outline"><i class="fa fa-export"></i>Print</div>
                    <div class="dt-button buttons-print btn green btn-outline"><i class="fa fa-export"></i>PDF</div>
                    <div class="dt-button buttons-print btn purple btn-outline"><i class="fa fa-export"></i>CSV</div>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                    <div class="table-actions-wrapper  <?php echo $this->custom_library->role_exist('Export Variance Report')?'':'hidden' ?>">

                        <?php echo form_open() ?>

                        <select name="export"  class="btn red btn-sm  btn-outline dropdown-toggle"  onchange="this.form.submit()">
                            <option value="">Export</option>
                            <option value="export_pdf">PDF</option>
                            <option value="excel">Excel</option>

                        </select>
                        <?php echo form_close() ?>

                    </div>
                    <table class="table table-striped table-bordered table-hover table-checkable" id="<?php echo $title ?>">
                        <thead>
                        <tr role="row" class="heading">
                            <!--                            <th width="1%">-->
                            <!--                                <input type="checkbox" class="group-checkable"> </th>-->
                            <th width="1%"> # </th>
                            <th width="5%"> Form# </th>
                            <th width="20%"> Village </th>

                            <th width="5%"> Variations </th>
                            <th width="5">Resolved</th>
                            <th width="20%"> Entrant </th>
                            <th width="20%"> Validator </th>
                            <th width="1%"> Actions </th>
                        </tr>

                        </thead>
                        <tbody> </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
</div>

<?php
$this->load->view('ajax/'.$title);
?>
