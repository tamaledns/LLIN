<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />

<!-- BEGIN PAGE LEVEL PLUGINS -->


<div class="row">

    <div class="col-md-12 ">

        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title hidden">
                <div class="caption font-red-sunglo">
                    <span class="caption-subject bold uppercase">New User</span>
                </div>
                <div class="actions hidden">

                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body form">

                <?php echo form_open('') ?>
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-12">


                            <div class="row">


                                <div class="col-md-3">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input required type="text" class="form-control" name="first_name" value="<?php echo set_value('first_name') ?>">
                                        <label for="form_control_1">First Name <?php echo form_error('first_name','<span style=" color:red;">','</span>') ?></label>
                                        <span class="help-block">Add VHT First Name</span>

                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input required type="text" class="form-control" name="last_name" value="<?php echo set_value('last_name') ?>">
                                        <label for="form_control_1">Last Name <?php echo form_error('last_name','<span style=" color:red;">','</span>') ?></label>
                                        <span class="help-block">Add VHT Last Name</span>

                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input required type="text" class="form-control phone" name="phone" value="<?php echo set_value('phone') ?>" id="form_control_1">
                                        <label for="for m_control_1">Phone  <?php echo form_error('phone','<span style=" color:red;">','</span>') ?></label>
                                        <span class="help-block">Add VHT phone</span>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class=" form-group form-md-line-input form-md-floating-label has-success">
                                        <select required name="code" class="form-control form-filter input-sm">
                                            <option value=""></option>
                                            <option value="1" <?php echo set_select('code',1) ?>>1</option>
                                            <option value="2" <?php echo set_select('code',2) ?>>2</option>


                                        </select>
                                        <label for="form_control_1">Select Code</label>
                                    </div>
                                </div>

                                </div>

                            <div class="row">
                                <div class="col-md-2">

                                    <?php $path=$this->input->post('village')?$this->locations->get_path($this->input->post('village')):array();

                                    //print_r($path);
                                    ?>

                                    <div class="form-group form-md-line-input has-success">
                                        <select  required  name="regions" class="regions form-control form-filter input-sm">
                                            <option value="">Regions...</option>
                                            <?php foreach ($this->locations->get_children(1) as $s){ ?>
                                                <option value="<?php echo $s->id ?>" <?php echo set_select('regions',$s->id) ?>><?php echo $s->name ?></option>
                                            <?php } ?>

                                        </select>
                                        <label for="form_control_1">Select Region</label>
                                    </div>
                                </div>
                                <div class="col-md-2">
<!--                                    form-group form-md-line-input form-md-floating-label has-success-->
                                    <div class="form-group form-md-line-input has-success">
                                        <select  required  name="district" class="district form-control form-filter input-sm" >
                                            <?php echo $this->input->post('district')?' <option value="'.$this->input->post('district').'">'.$path[1]['name'].'</option>':''; ?>
                                            <option value="">District...</option>
                                        </select>
                                        <label for="form_control_1">District</label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group form-md-line-input has-success">
                                        <select  required  name="sub_county" class="sub_county form-control form-filter input-sm">
                                            <?php echo $this->input->post('sub_county')?' <option value="'.$this->input->post('sub_county').'">'.$path[2]['name'].'</option>':''; ?>
                                            <option value="">Sub county...</option>

                                        </select>
                                        <label for="form_control_1">Sub County</label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-md-line-input has-success">
                                        <select  required  name="parish" class="parish form-control form-filter input-sm" >
                                            <?php echo $this->input->post('parish')?' <option value="'.$this->input->post('parish').'">'.$path[3]['name'].'</option>':''; ?>
                                            <option value="">Parish...</option>

                                        </select>
                                        <label for="form_control_1">Parish</label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-md-line-input has-success">
                                        <select  required  name="village" class="village form-control form-filter input-sm" >
                                            <?php echo $this->input->post('village')?' <option value="'.$this->input->post('village').'">'.$path[4]['name'].'</option>':''; ?>
                                            <option value="">Village...</option>


                                        </select>
                                        <label for="form_control_1">Village</label>
                                    </div>
                                </div>
                            </div>



                        </div>




                    </div>



                </div>
                <div class="form-actions">
                    <button type="submit"  class="btn blue"><i class="icon-plus"></i> Add User</button>

                    <button type="reset" class="btn default">Cancel</button>
                </div>
                <?php echo form_close() ?>
            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->

    </div>
</div>



<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo base_url() ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>

<script>


    $('.filter-submit').click(function(){
        var r = $('.sub_county').val();

        if(r==''){
            $('.filter_alert').html('At least select District and Sub county');
        }else{
            $('.filter_alert').html('')
        }
    });

    //getting districts in regions
    $('.regions').change(function () {
        var r = $('.regions').val();
        if (r != '') {
            $.ajax({
                type: 'GET',

                url: '<?php echo base_url("index.php/ajax_api/get_children_options")?>/' + r,
                beforeSend: function () {
                    $(".district").html('<option value="">Loading...</option>');
                },
                success: function (d) {

                    $(".district").html(d);


                }


            });
        }
    });

    //getting subcounty in districts

    $('.district').change(function () {
        var r = $('.district').val();
        if (r != '') {
            $.ajax({
                type: 'GET',

                url: '<?php echo base_url("index.php/ajax_api/get_children_options")?>/' + r,
                beforeSend: function () {
                    $(".sub_county").html('<option value="">Loading...</option>');
                },
                success: function (d) {

                    $(".sub_county").html(d);


                }


            });
        }else{
            $(".sub_county").html('<option value="">Sub county...</option>');
        }
    });


    //getting parish in subcounty


    $('.sub_county').change(function () {
        var r = $('.sub_county').val();
        if (r != '') {
            $.ajax({
                type: 'GET',

                url: '<?php echo base_url("index.php/ajax_api/get_children_options")?>/' + r,
                beforeSend: function () {
                    $(".parish").html('<option value="">Loading...</option>');
                },
                success: function (d) {

                    $(".parish").html(d);


                }


            });
        }else{
            $(".parish").html('<option value="">Parish...</option>');
        }
    });


    //getting Village in Parish


    $('.parish').change(function () {
        var r = $('.parish').val();
        if (r != '') {
            $.ajax({
                type: 'GET',

                url: '<?php echo base_url("index.php/ajax_api/get_children_options")?>/' + r,
                beforeSend: function () {
                    $(".village").html('<option value="">Loading...</option>');
                },
                success: function (d) {

                    $(".village").html(d);


                }


            });
        }else{
            $(".village").html('<option value="">Village...</option>');
        }
    });



    $('.phone').keyup(function(){

        var first_no=$(this).val();
        var variable2 = first_no.substring(0, 2);

        if(variable2.length>1 && variable2 != '07'){


            $( this ).css( "color", "red" );
            $( this ).css( "border-color", "red" );
            alert('Invalid Phone Number entered');
        }else{
            $( this ).css( "color", "#555" );
            $( this ).css( "border-color", "#c2cad8" );
        }
        //alert(variable2);


    });


</script>

