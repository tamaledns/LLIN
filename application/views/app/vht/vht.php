<?php //$this->load->view($this->page_level.$this->page_level2.'tree_diagram'); ?>

<?php //print_r($this->input->post()) ?>
<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />


<div class="row">
    <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light portlet-fit portlet-datatable ">
            <div class="portlet-title hidden" style="    padding: 0px 20px 0px;">


                <div class="caption font-dark" style="    padding: 19px 0;">
                    <i class="icon-home font-dark"></i>
                    <span class="caption-subject bold uppercase"><?php echo humanize($title) ?></span>
                    <?php //echo anchor($this->page_level.$this->page_level2.'new',' <i class="fa fa-plus"></i> Add new','class="btn btn-sm green-jungle"'); ?>
                </div>


                <div class="tools hidden">
                    <div class="dt-button buttons-print btn dark btn-outline"><i class="fa fa-export"></i>Print</div>
                    <div class="dt-button buttons-print btn green btn-outline"><i class="fa fa-export"></i>PDF</div>
                    <div class="dt-button buttons-print btn purple btn-outline"><i class="fa fa-export"></i>CSV</div>
                </div>
            </div>
            <div class="portlet-body  table-scrollable">
                <?php echo form_open(); ?>
                <div class="table-container">
                    <div class="table-actions-wrapper">

                        <div class="form-inline">



                            <?php echo anchor($this->page_level.$this->page_level2.'new',' <i class="fa fa-plus"></i> New user','class="btn btn-sm green-jungle"'); ?>
                            <div class="form-group  <?php echo $this->custom_library->role_exist('Export VHT')?'':'hidde' ?>">

                                <select name="export"  class="btn red btn-sm  btn-outline dropdown-toggle"  onchange="this.form.submit()">
                                    <option value="">Export</option>
                                    <option value="export_pdf">PDF</option>
                                    <option value="excel">Excel</option>

                                </select>

                            </div>




                        </div>
                    </div>
                    <div class="table-scrollabl">
                    <table class="table table-striped  table-bordered table-hover table-checkable" id="vht">
                        <thead>
                        <tr role="row" class="filter  <?php  echo  $this->custom_library->role_exist('Filter Form list')?'':'hidden'; ?> ">

                            <td colspan="8">
                              <?php $this->load->view('ajax/locations_filter') ?>
                            </td>


                        </tr>
                        <tr role="row" class="heading">
                            <th width="1%">
                                <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                    <input type="checkbox" name="i[]" class="group-checkable" data-set="#sample_2 .checkboxes" />
                                    <span></span>
                                </label>
                            </th>


                            <th> Name </th>

                            <th> Phone </th>
                            <th> Code </th>


                            <th> District </th>
                            <th> Parish </th>

                            <th> Village </th>
                            <th width="60">Action</th>

                        </tr>



                        </thead>
                        <tbody> </tbody>
                    </table>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
</div>

<?php
$this->load->view('ajax/vht');
?>
