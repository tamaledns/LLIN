<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <title><?php echo ($this->site_options->title('site_name')) ?> | User Login</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description"/>
    <meta content="Dennis Tamale(tamaledns@gmail.com)" name="author"/>

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/login/login.css" rel="stylesheet" type="text/css" />

    <link rel="shortcut icon" href="<?php echo base_url($this->site_options->title('site_logo')) ?>" />
</head>
<!-- END HEAD -->
<div class="login">

    <a href="<?php echo base_url(); ?>">   <img class="login-logo"  src="<?php echo base_url($this->site_options->title('site_logo')) ?>" /> </a>
    <h1><?php echo $this->site_options->title('site_name') ?> Login <i class="icon-login"></i></h1>

        <?php if(isset($message)){ ?>
            <?php echo ' <p style="color: red;"><i class="icon-ban"></i> '. $message.'</p>'; ?>
        <?php } ?>

    <form method="post">
        <label style="color: red;"><?php echo form_error('username','<span class="require">','</span>') ?></label>
        <input type="text" name="username" placeholder="Username" required="required" />
        <label><?php echo form_error('password','<span class="require">','</span>') ?></label>
        <input type="password" name="password" placeholder="Password" required="required" />
        <button type="submit" class="btn btn-primary btn-block btn-large">Let me in. <i class="icon-key"></i></button>
    </form>
</div>


</body>

</html>
