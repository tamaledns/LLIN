<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered" id="form_wizard_1">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-layers font-red"></i>
                                        <span class="caption-subject font-red bold uppercase"> Payment Process -
                                            <span class="step-title"> Step 1 of 3 </span>
                                        </span>
                </div>
                <div class="actions">
                    <button class="btn btn-circle  btn-default">
                   <span class="font-blue"> Current Amount :</span>    <?php echo number_format($wallet->current_amount,2) ?>
                    </button>

                </div>
            </div>
            <div class="portlet-body form">



                <form action="#" class="form-horizontal" id="submit_form" method="POST">
                    <div class="form-wizard">
                        <div class="form-body">
                            <ul class="nav nav-pills nav-justified steps">
                                <li>
                                    <a href="#tab1" data-toggle="tab" class="step">
                                        <span class="number"> 1 </span>
                                                            <span class="desc">
                                                                <i class="fa fa-check"></i> Payment Method <?php //echo $wallet->user_id; ?></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#tab2" data-toggle="tab" class="step">
                                        <span class="number"> 2 </span>
                                                            <span class="desc">
                                                                <i class="fa fa-check"></i> Confirmation </span>
                                    </a>
                                </li>

                                <li>
                                    <a href="#tab3" data-toggle="tab" class="step">
                                        <span class="number"> 3 </span>
                                                            <span class="desc">
                                                                <i class="fa fa-check"></i> Finish </span>
                                    </a>
                                </li>
                            </ul>
                            <div id="bar" class="progress progress-striped" role="progressbar">
                                <div class="progress-bar progress-bar-success"></div>
                            </div>
                            <div class="tab-content">
                                <div class="alert alert-danger display-none">
                                    <button class="close" data-dismiss="alert"></button>
                                    You have some form errors. Please check below.
                                </div>
                                <div class="aler alert-success display-none">
                                    <button class="close" data-dismiss="alert"></button>
<!--                                    Your form validation is successful!-->
                                </div>
                                <div class="tab-pane active" id="tab1">
                                    <h3 class="block">Please Choose the Topup Method</h3>

                                    <div class="form-group">

                                        <label class="control-label col-md-3">Select Method</label>
                                        <div class="col-md-6 tooltips">

                                            <label> <input type="radio" checked name="method" value="mobile"> <span
                                                    class="btn btn-sm btn light " style="font-size: x-large"><i
                                                        class="fa fa-mobile"> Mobile</i></span></label>

                                            <label> <input type="radio" name="method" value="cash"> <span
                                                    class="btn btn-sm btn light " style="font-size: x-large"><i
                                                        class="fa fa-money"> Cash</i></span></label>


                                        </div>

                                    </div>




                                    <span id="mobile">
                                        <div class="row">

                                    <div class="form-group" style="display: block;">
                                        <label class="control-label col-md-3 tooltips"
                                               data-original-title="Select the type of network you are going to use"></label>
                                        <div class="col-md-6 tooltips">
                                            <h4 class="block bold" style="border-bottom: solid thin silver;"> <i class="fa fa-mobile"></i>   Mobile</h4>
                                            <label> <input type="radio" name="network" value="micropay" id="mp"
                                                           onclick="number()"> <span
                                                    class="btn btn-sm btn light "><?php echo img(array('src' => 'assets/payement_logos/micropay.png', 'height' => 35)) ?></span></label>
                                            <label> <input type="radio" name="network" value="mtnmoney" id="mp"
                                                           onclick="number()"> <span
                                                    class="btn btn-sm btn light "><?php echo img(array('src' => 'assets/payement_logos/mtnmoney.jpg', 'height' => 35)) ?></span></label>
                                            <label> <input type="radio" name="network" value="airtelmoney" id="mp"
                                                           onclick="number()"> <span
                                                    class="btn btn-sm btn light "><?php echo img(array('src' => 'assets/payement_logos/airtelmoney.jpg', 'height' => 35)) ?></span></label>
                                            <label> <input type="radio" name="network" value="pesapal" id="mp"
                                                           onclick="number()"> <span
                                                    class="btn btn-sm btn light "><?php echo img(array('src' => 'assets/payement_logos/pesapal.jpg', 'height' => 35)) ?></span></label>
                                        </div>


                                    </div>

                <div class="form-group" style="display: none;" id="phone">
                    <?php $number = $this->db->select('phone')->from('users')->where('id', $wallet->user)->get()->result(); ?>
                    <label class="col-md-3 control-label">    Phone Number</label>
                    <div class="col-md-2">
                        <?php echo form_error('phone', '<label style="color:red;">', '</label>'); ?>
                        <input type="text" name="phone" id="PhoneNo" value="<?php echo $number[0]->phone; ?>"
                               class="form-control tooltips" data-original-title="Subscriber's Phone Number"
                               placeholder="Phone Number">
                        <input type="hidden" name="invoice_id" id="invoice_id" value="<?php echo $user_id ?>">
                    </div>
                    <label class="control-label col-md-2">Paid in Amount</label>
                    <div class="form-group col-md-3">

                        <div class="input-group">
                                                                        <span class="input-group-addon">
                                                                            <i class="fa fa-money"></i>
                                                                        </span>
                            <input type="number" name="amount_to_paid_mobile" class="form-control"  value="" placeholder="Amount to be Paid"> </div>
                        <span id="cash_alert" class="font-red"></span>
                    </div>
                </div>

        </div>

</span>

                                    <span id="cash" class="hidden">
                                        <div class="row">
                                            <label class="col-md-3 control-label"></label>

                                            <div class="col-md-6">
                                                <h4 class="block bold" style="border-bottom: solid thin silver;">   <i class="fa fa-money"></i> Cash</h4>
                                         <!-- BEGIN FORM-->

                                         <div class="form-actions top" hidden>
                                             <button type="submit" class="btn green">Submit</button>
                                             <button type="button" class="btn default">Cancel</button>
                                         </div>
                    <div id="form-body" class="form-body">

                        <div class="form-group col-md-4">
                            <label class="control-label">Current Amount</label>
                            <div class="input-group">
                                                                        <span class="input-group-addon">
                                                                            <i class="fa fa-money"></i>
                                                                        </span>
                                <input type="text" class="form-control" name="amount" readonly value="<?php echo number_format($wallet->current_amount,2) ?>" placeholder="Amount"> </div>
                        </div>

                        <div class="form-group col-md-8">
                            <label class="control-label">Paid in Amount</label>
                            <div class="input-group">
                                                                        <span class="input-group-addon">
                                                                            <i class="fa fa-money"></i>
                                                                        </span>
                                <input type="number" name="amount_to_paid" class="form-control"  value="" placeholder="Amount to be Paid"> </div>
                            <span id="cash_alert" class="font-red"></span>
                        </div>



                        <div class="form-group last" style="margin-bottom: 0;margin-top: 0;">
                            <label class="control-label"></label>

                        </div>
                    </div>
                    <div class="form-actions" id="buttons">
                        <button type="submit" name="method" id="cash_submit" class="btn green"><i class="fa fa-refresh"></i> Process Payments</button>
                        <button type="button" class="btn default">Cancel</button>
                    </div>
                                         <?php echo form_open() ?>
                                                </div>
                                         <!-- END FORM-->
                                         </div>
                                    </span>



                                </div>
                                <div class="tab-pane" id="tab2">
                                    <h3 class="block">Please confirm the payment details</h3>
                                    <div class="form-group" id="payement_confirm">

                                        <!--                                        This is part is processes payments   -->


                                    </div>
                                </div>
                                <div class="tab-pane" id="tab3">
                                    <h3 class="block">Completing Transaction</h3>

                                    <div class="form-group" id="paid">

                                        <!--                                        This is part is processes payments   -->


                                    </div>

                                </div>

                            </div>
                        </div>
                        <div class="form-actions" id="nextButtons">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <a href="javascript:;" class="btn default button-previous">
                                        <i class="fa fa-angle-left"></i> Back </a>
                                    <a href="javascript:;" class="btn btn-outline green button-next"> Continue
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                    <a href="javascript:;" class="btn green button-submit"> Submit
                                        <i class="fa fa-check"></i>
                                    </a>
                                </div>
                            </div>
                        </div>


                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
<!-- END PAGE BASE CONTENT -->


</div>
</div>
<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->

<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner">   <?php echo date('Y') ?> &copy; Newspaper Management System.

    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<!-- END FOOTER -->
<!--[if lt IE 9]>
<script src="<?php echo base_url() ?>assets/global/plugins/respond.min.js"></script>
<script src="<?php echo base_url() ?>assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo base_url() ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/global/plugins/bootstrap/js/bootstrap.min.js"
        type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"
        type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
        type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/global/plugins/uniform/jquery.uniform.min.js"
        type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
        type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url() ?>assets/global/plugins/select2/js/select2.full.min.js"
        type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/global/plugins/jquery-validation/js/jquery.validate.min.js"
        type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/global/plugins/jquery-validation/js/additional-methods.min.js"
        type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js"
        type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?php echo base_url() ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<?php $this->load->view('payments/form_wizard'); ?>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="<?php echo base_url() ?>assets/layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>




<!-- END THEME LAYOUT SCRIPTS -->






<?php $this->load->view('payments/ajaxTopup') ?>
</body>

</html>