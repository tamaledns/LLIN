<div class="page-sidebar-wrapper">
    <!-- END SIDEBAR -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-compact <?php //echo $title=='house_hold_reg'?'page-sidebar-menu-closed':'' ?> " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" data-height="312">


            <li class=" nav-item <?php echo $this->custom_library->role_exist('dashboard','group')?'':'hidden'; ?>   <?php echo $title=='dashboard'?'active':''; ?>"><?php echo anchor($this->page_level.'dashboard','<i class="icon-home"></i> <span class="title">Dashboard</span> <span class="'.($title=='dashboard'?'selected':'').'"></span>') ?></li>





            <li class="nav-item <?php echo $this->session->user_type!=5 && $this->custom_library->role_exist('form management','group')?'':'hidden'; ?>  <?php echo $title=='house_hold_reg'?'active open':'' ?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-doc"></i>
                    <span class="title">Forms</span>
                    <span class=" <?php echo $title=='form_reg'?'selected':'' ?>"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">


                    <li class="nav-item  <?php echo $subtitle=='add_form_num'?'active':''; ?>">

                        <?php echo anchor($this->page_level.'house_hold_reg/add_form_num','<i class="fa fa-plus" style="font-size: smaller;"></i> <span class="title">New</span>','class="nav-link "') ?>
                    </li>
<!--                    <li class="nav-item  --><?php //echo $subtitle=='add_form_num'?'active':''; ?><!--">-->
<!---->
<!--                        --><?php //echo anchor('pdf_export/users','<i class="fa fa-plus" style="font-size: smaller;"></i> <span class="title">PDF</span>','target="_blank" class="nav-link "') ?>
<!--                    </li>-->

                    <li class="nav-item  <?php echo $subtitle=='village_view'?'active':''; ?> ">
                        <?php echo anchor($this->page_level.'house_hold_reg/list_forms','<i class="fa fa-list" style="font-size: smaller;"></i> <span class="title">List Forms</span>','class="nav-link "') ?>
                    </li>


                    <li class="nav-item  <?php echo $subtitle=='stored'?'active':''; ?> ">
                        <?php echo anchor($this->page_level.'house_hold_reg/in_store','<i class="fa fa-list" style="font-size: smaller;"></i> <span class="title">In Store</span>','class="nav-link "') ?>
                    </li>

                    <li class="nav-item  <?php echo $subtitle=='infield'?'active':''; ?> ">
                        <?php echo anchor($this->page_level.'house_hold_reg/in_field','<i class="fa fa-list" style="font-size: smaller;"></i> <span class="title">In field</span>','class="nav-link "') ?>
                    </li>


                    <li class="nav-item  <?php echo $subtitle=='released'?'active':''; ?> ">
                        <?php echo anchor($this->page_level.'house_hold_reg/released','<i class="fa fa-list" style="font-size: smaller;"></i> <span class="title">Released</span>','class="nav-link "') ?>
                    </li>



                    <li class="nav-item  <?php echo $subtitle=='captured'?'active':''; ?> ">
                        <?php echo anchor($this->page_level.'house_hold_reg/captured','<i class="fa fa-list" style="font-size: smaller;"></i> <span class="title">Captured</span>','class="nav-link "') ?>
                    </li>

                </ul>
            </li>


            <li class="nav-item <?php  echo $this->custom_library->role_exist('Variance Report','group')?'':'hidden'; ?>  <?php echo $title=='variance_reports'?'active open':'' ?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-equalizer"></i>
                    <span class="title">Variance Reports</span>
                    <span class=" <?php echo $title=='variance_reports'?'selected':'' ?>"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item <?php  echo $this->custom_library->role_exist('View Collection Variance')?'':'hidden'; ?>   <?php echo $subtitle=='collection_variance'?'active':''; ?> ">

                        <?php echo anchor($this->page_level.'variance_reports/collection_variance','<i class="fa fa-file" style="font-size: smaller;"></i> <span class="title">Collection Variance</span>','class="nav-link "') ?>
                    </li>
                    <li class="nav-item  <?php  echo $this->custom_library->role_exist('View Entry Variance')?'':'hidden'; ?>    <?php echo $subtitle=='entry_variance'?'active':''; ?> ">

                        <?php echo anchor($this->page_level.'variance_reports/entry_variance','<i class="fa fa-file" style="font-size: smaller;"></i> <span class="title">Entry Variance</span>','class="nav-link "') ?>
                    </li>
                </ul>
            </li>



            <li class="nav-item <?php  echo $this->custom_library->role_exist('vht','group')?'':'hidden'; ?>  <?php echo $title=='vht'?'active open':'' ?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-tag"></i>
                    <span class="title">VHT</span>
                    <span class=" <?php echo $title=='vht'?'selected':'' ?>"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item <?php  echo $this->custom_library->role_exist('new VHT')?'':'hidden'; ?>   <?php echo $subtitle=='new'?'active':''; ?> ">

                        <?php echo anchor($this->page_level.'vht/new','<i class="fa fa-file" style="font-size: smaller;"></i> <span class="title">New</span>','class="nav-link "') ?>
                    </li>
                    <li class="nav-item  <?php  echo $this->custom_library->role_exist('list VHT')?'':'hidden'; ?>    <?php echo $subtitle==''?'active':''; ?> ">

                        <?php echo anchor($this->page_level.'vht','<i class="fa fa-list" style="font-size: smaller;"></i> <span class="title">List VHT</span>','class="nav-link "') ?>
                    </li>
                </ul>
            </li>


            <li class="nav-item <?php  echo $this->custom_library->role_exist('coverage','group')?'':'hidden'; ?>  <?php echo $title=='coverage'?'active open':'' ?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-globe"></i>
                    <span class="title">Coverage</span>
                    <span class=" <?php echo $title=='coverage'?'selected':'' ?>"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item <?php  echo $this->custom_library->role_exist('Registration')?'':'hidden'; ?>   <?php echo $subtitle=='registration'?'active':''; ?> ">

                        <?php echo anchor($this->page_level.'coverage/registration','<i class="fa fa-file" style="font-size: smaller;"></i> <span class="title">Registration</span>','class="nav-link "') ?>
                    </li>
                    <li class="nav-item  <?php  echo $this->custom_library->role_exist('Allocation')?'':'hidden'; ?>    <?php echo $subtitle=='allocation'?'active':''; ?> ">

                        <?php echo anchor($this->page_level.'coverage/allocation','<i class="fa fa-list" style="font-size: smaller;"></i> <span class="title">Allocation</span>','class="nav-link "') ?>
                    </li>
                    <li class="nav-item  <?php  echo $this->custom_library->role_exist('Distribution')?'':'hidden'; ?>    <?php echo $subtitle=='distribution_gap'?'active':''; ?> ">

                        <?php echo anchor($this->page_level.'coverage/distribution_gap','<i class="fa fa-list" style="font-size: smaller;"></i> <span class="title">Distribution Gap</span>','class="nav-link "') ?>
                    </li>

                    <li class="nav-item  <?php  echo $this->custom_library->role_exist('Distribution')?'':'hidden'; ?>    <?php echo $subtitle=='registration_map'?'active':''; ?> ">

                        <?php echo anchor($this->page_level.'coverage/registration_map','<i class="fa fa-map" style="font-size: smaller;"></i> <span class="title">Registration Map</span>','class="nav-link "') ?>
                    </li>

                    <li class="nav-item  <?php  echo $this->custom_library->role_exist('Distribution')?'':'hidden'; ?>    <?php echo $subtitle=='distribution_map'?'active':''; ?> ">

                        <?php echo anchor($this->page_level.'coverage/distribution_map','<i class="fa fa-map" style="font-size: smaller;"></i> <span class="title">Distribution Map </span>','class="nav-link "') ?>
                    </li>

                    <li class="nav-item  <?php  echo $this->custom_library->role_exist('Distribution')?'':'hidden'; ?>    <?php echo $subtitle=='map_view'?'active':''; ?> ">

                        <?php echo anchor($this->page_level.'coverage/map_view','<i class="fa fa-map" style="font-size: smaller;"></i> <span class="title">Population Map</span>','class="nav-link "') ?>
                    </li>

                    <li class="nav-item <?php  echo $this->custom_library->role_exist('Distribution')?'':'hidden'; ?>    <?php echo $subtitle=='maps'?'active':''; ?>">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="fa fa-map"></i> Maps
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">

                            <?php $sub_sub_title=$this->uri->segment(4); ?>
                            <li class="nav-item  <?php  echo $this->custom_library->role_exist('Distribution')?'':'hidden'; ?>    <?php echo $sub_sub_title=='registration_map'?'active':''; ?> ">

                                <?php echo anchor($this->page_level.'coverage/maps/registration_map','<i class="icon-pointer" style="font-size: smaller;"></i> <span class="title">Registration Map</span>','class="nav-link "') ?>
                            </li>

                            <li class="nav-item  <?php  echo $this->custom_library->role_exist('Distribution')?'':'hidden'; ?>    <?php echo $sub_sub_title=='distribution_map'?'active':''; ?> ">

                                <?php echo anchor($this->page_level.'coverage/maps/distribution_map','<i class="icon-pointer" style="font-size: smaller;"></i> <span class="title">Distribution Map </span>','class="nav-link "') ?>
                            </li>

                            <li class="nav-item  <?php  echo $this->custom_library->role_exist('Distribution')?'':'hidden'; ?>    <?php echo $sub_sub_title=='map_view'?'active':''; ?> ">

                                <?php echo anchor($this->page_level.'coverage/maps/map_view','<i class="icon-pointer" style="font-size: smaller;"></i> <span class="title">Population Map</span>','class="nav-link "') ?>
                            </li>
                        </ul>
                    </li>

                </ul>
            </li>





            <li class="nav-item <?php echo  $this->custom_library->role_exist('allocation plan','group')?'':'hidden'; ?> <?php echo $title=='distribution_reports'?'active open':'' ?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-share"></i>
                    <span class="title">Allocation plan</span>
                    <span class=" <?php echo $title=='distribution_reports'?'selected':'' ?>"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">



                    <li class="nav-item  <?php //echo  $this->custom_library->role_exist('View Household Plan')?'':'hidden'; ?>  <?php echo $subtitle=='households'?'active':''; ?> ">

                        <?php echo anchor($this->page_level.'distribution_reports/households','<i class="fa fa-file" style="font-size: smaller;"></i> <span class="title">HouseHold</span>','class="nav-link "') ?>
                    </li>

                    <li class="nav-item <?php echo  $this->custom_library->role_exist('View Village Plan')?'':'hidden'; ?>    <?php echo $subtitle=='village'?'active':''; ?> ">

                        <?php echo anchor($this->page_level.'distribution_reports/village','<i class="fa fa-file" style="font-size: smaller;"></i> <span class="title">Village</span>','class="nav-link "') ?>
                    </li>

                    <li class="nav-item <?php echo  $this->custom_library->role_exist('View Parish Plan')?'':'hidden'; ?>    <?php echo $subtitle=='parish_report'?'active':''; ?> ">

                        <?php echo anchor($this->page_level.'distribution_reports/parish_report','<i class="fa fa-file" style="font-size: smaller;"></i> <span class="title">Parish</span>','class="nav-link "') ?>
                    </li>

                    <li class="nav-item <?php echo  $this->custom_library->role_exist('View Distribution Point Plan')?'':'hidden'; ?>   <?php echo $subtitle=='distribution_point'?'active':''; ?> ">
                        <?php echo anchor($this->page_level.'distribution_reports/distribution_point','<i class="fa fa-file" style="font-size: smaller;"></i> <span class="title">Distribution Point</span>','class="nav-link "') ?>
                    </li>


                    <li class="nav-item <?php echo  $this->custom_library->role_exist('View Subcounty Plan')?'':'hidden'; ?>   <?php echo $subtitle=='sub_county_report'?'active':''; ?> ">
                        <?php echo anchor($this->page_level.'distribution_reports/sub_county_report','<i class="fa fa-file" style="font-size: smaller;"></i> <span class="title">Subcounty</span>','class="nav-link "') ?>
                    </li>

                    <li class="nav-item <?php echo  $this->custom_library->role_exist('View District Plan')?'':'hidden'; ?>   <?php echo $subtitle=='district_report'?'active':''; ?> ">
                        <?php echo anchor($this->page_level.'distribution_reports/district_report','<i class="fa fa-file" style="font-size: smaller;"></i> <span class="title">District</span>','class="nav-link "') ?>
                    </li>


                </ul>
            </li>




            <li class="nav-item  <?php echo  $this->custom_library->role_exist('inventory management','group')?'':'hidden'; ?> <?php echo $title=='inventory'?'active open':'' ?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-layers"></i>
                    <span class="title">Inventory</span>
                    <span class=" <?php echo $title=='inventory'?'selected':'' ?>"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">

                    <li class="nav-item <?php echo  $this->custom_library->role_exist('View Inventory List')?'':'hidden'; ?>  <?php echo $subtitle=='new'?'active':''; ?> ">

                        <?php echo anchor($this->page_level.'inventory/new','<i class="fa fa-list" style="font-size: smaller;"></i> <span class="title">List Inventory</span>','class="nav-link "') ?>
                    </li>



                    <li class="nav-item  <?php echo $subtitle==''?'active':''; ?> ">

                        <?php echo anchor($this->page_level.'inventory','<i class="fa fa-file" style="font-size: smaller;"></i> <span class="title">All Requests</span>','class="nav-link "') ?>
                    </li>


                    <li class="nav-item  <?php echo $subtitle=='pending'?'active':''; ?> ">

                        <?php echo anchor($this->page_level.'inventory/pending','<i class="fa fa-refresh" style="font-size: smaller;"></i> <span class="title">Pending Requests</span>','class="nav-link "') ?>
                    </li>


                    <li class="nav-item  <?php echo $subtitle=='store_requests'?'active':''; ?> ">

                        <?php echo anchor($this->page_level.'inventory/store','<i class="fa fa-file" style="font-size: smaller;"></i> <span class="title">Store Requests</span>','class="nav-link "') ?>
                    </li>


                    <li class="nav-item  <?php echo $subtitle=='release_requests'?'active':''; ?> ">

                        <?php echo anchor($this->page_level.'inventory/release','<i class="fa fa-file" style="font-size: smaller;"></i> <span class="title">Release Requests</span>','class="nav-link "') ?>
                    </li>


                    <li class="nav-item  <?php echo $subtitle=='approved'?'active':''; ?> ">

                        <?php echo anchor($this->page_level.'inventory/approved','<i class="fa fa-check" style="font-size: smaller;"></i> <span class="title">Approved</span>','class="nav-link "') ?>
                    </li>

                </ul>
            </li>






            <li class="nav-item <?php echo  $this->custom_library->role_exist('messaging','group')?'hidden':'hidden'; ?> <?php echo $title=='messaging'?'active open':'' ?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-paper-plane"></i>
                    <span class="title">Messaging</span>
                    <span class=" <?php echo $title=='messaging'?'selected':'' ?>"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  <?php echo $subtitle=='add_products'?'active':''; ?> ">

                        <?php echo anchor($this->page_level.'messaging','<i class="fa fa-envelope" style="font-size: smaller;"></i> <span class="title">Messages</span>','class="nav-link "') ?>
                    </li>
                </ul>
            </li>



            <li class="nav-item <?php echo  $this->custom_library->role_exist('user management','group')?'':'hidden'; ?>  <?php echo $title=='users'||$title=='permissions'||$title=='profile'?'active open':'' ?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-users"></i>
                    <span class="title">User Management</span>
                    <span class=" <?php echo $title=='users'?'selected':'' ?>"></span>
                    <span class="arrow"></span>
                </a>

                <ul class="sub-menu">
                    <li class="nav-item  <?php echo  $this->custom_library->role_exist('new user')?'':'hidden'; ?>   <?php echo $subtitle=='new'?'active':''; ?> ">

                        <?php echo anchor($this->page_level.'users/new','<i class="fa fa-plus" style="font-size: smaller;"></i> <span class="title">Add User</span>','class="nav-link "') ?>
                    </li>
                    <li class="nav-item  <?php echo  $this->custom_library->role_exist('view user list')?'':'hidden'; ?>   <?php echo $subtitle==''||$subtitle=='view_users'?'active':''; ?> ">

                        <?php echo anchor($this->page_level.'users','<i class="fa fa-list" style="font-size: smaller;"></i> <span class="title">List Users</span>','class="nav-link "') ?>
                    </li>
                    <li class="nav-item <?php echo  $this->custom_library->role_exist('view user list')?'':'hidden'; ?>   <?php echo $subtitle==''||$subtitle=='view_users'?'active':''; ?> ">

                        <?php echo anchor($this->page_level.'users/filter/blocked','<i class="fa fa-list" style="font-size: smaller;"></i> <span class="title">Blocked Users</span>','class="nav-link "') ?>
                    </li>

                    <li class="nav-item <?php echo  $this->custom_library->role_exist('view own user profile')?'':'hidden'; ?>   <?php echo $title=='profile'?'active':''; ?> ">

                        <?php echo anchor($this->page_level.'profile','<i class="fa fa-user" style="font-size: smaller;"></i> <span class="title">My Profile</span>','class="nav-link "') ?>
                    </li>




                    <!--                    this is the menu for the permissions and roles-->


                    <li class="  <?php echo  $this->custom_library->role_exist('view user list')&& $this->session->user_type=='1'?'':'hidden'; ?>  <?php echo $title=='permissions'&&$subtitle==''?'active':''; ?>"><?php echo anchor($this->page_level.'permissions','<i class="fa fa-angle-double-right"></i> <span class="title">Permissions</span>') ?></li>

                    <li class="  <?php echo  $this->custom_library->role_exist('view user list')&& $this->session->user_type=='1'?'':'hidden'; ?>  <?php echo $title=='permissions'&&$subtitle=='roles'||$subtitle=='remove_permission'?'active':''; ?>"><?php echo anchor($this->page_level.'permissions/roles','<i class="fa fa-angle-double-right"></i> <span class="title">Roles</span>') ?></li>



                </ul>
            </li>

            <li class="nav-item <?php  echo  $this->custom_library->role_exist('audit logs','group')?'':'hidden'; ?> <?php echo $title=='logs'?'active':''; ?>"><?php echo anchor($this->page_level.'logs','<i class="icon-list"></i> <span class="title">Audit Trail</span>  <span class="'.($title=='logs'?'selected':'').'"></span>') ?></li>

            <li class="nav-item <?php  echo  $this->custom_library->role_exist('audit logs','group')?'':'hidden'; ?>  <?php echo $title=='settings'||$title=='site_options'?'active open':'' ?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-settings"></i>
                    <span class="title">Settings</span>
                    <span class=" <?php echo $title=='settings'?'selected':'' ?>"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">

                    <li class="<?php echo $subtitle=='countries'||$subtitle=='new_country'||$subtitle=='ban_country'||$subtitle=='delete_country'||$subtitle=='unblock_country'?'active':''; ?>"><?php echo anchor($this->page_level.'settings/countries','<i class="icon-globe"></i> <span class="title">Countries</span>') ?></li>
                    <li class="<?php echo $subtitle=='regions'?'active':''; ?>"><?php echo anchor($this->page_level.'settings/regions','<i class="icon-globe"></i> <span class="title">Regions</span>') ?></li>
                    <li class=" <?php echo $title=='site_options'?'active':''; ?>"><?php echo anchor($this->page_level.'site_options','<i class="icon-settings"></i> <span class="title">Site Options</span>') ?></li>


                </ul>
            </li>



        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>


<?php //var_dump($this->custom_library->role_exist('vht','group')) ?>